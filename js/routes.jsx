'use strict';

import React from 'react';
import { Router, Route, IndexRoute} from 'react-router'

import authStore from './stores/AuthStore';
import profileStore from './stores/ProfileStore';

import BridgeApp from './components/BridgeApp';
import Panels    from './components/Panels';
import Profile   from './components/panel4/Profile';
import BridgeDetail  from './components/panel4/BridgeDetail';
import Invites       from './components/bridge/Invites';
import Search       from './components/panel4/Search';
import ExploreBridge from './components/pages/ExploreBridge';

const NotFound = require('./components/common/Common').NotFound;
import Login from './components/pages/Login';
import SignUp from './components/pages/SignUp';
import Welcome from './components/pages/Welcome';


const Index = React.createClass({

	render: function () {

		return (
			<div>
				<h1>You should not see this.</h1>
				{this.props.children}
			</div>
		);
	}
});

const EBAuth = React.createClass({

	render: function () {
		return (
			<div>
				<h1>How did we get on?</h1>
			</div>
		);
	}
});

const MUAuth = React.createClass({

	render: function () {
		return (
			<div>
				<h1>Hi Meetup?</h1>
			</div>
		);
	}
});

function checkEBAuth(nextState, replaceState) {

	let match;
	if (match = /access_token=[A-Z0-9]*$/.exec(location.hash)) {

		const profile = profileStore.getProfile();
		const hash = match[0].split('&');
		const EventBriteCode = hash[0].replace("access_token=", "");

		profile.setEventbriteAuthId(
			EventBriteCode,
			function(){
				window.close();
			}
		);
	}else{
		window.close();
	}
}

function checkMUAuth(nextState, replaceState) {

	let match;
	if (match = /access_token=[^&]*/.exec(location.hash)) {
		const profile = profileStore.getProfile();
		let authid = authStore.getAuthData().auth.uid,
		meetupCode = match[0].replace("access_token=", "");

		profile.setMeetupAccessToken( meetupCode, function(){
			window.close();
		});
	}else{
		window.close();
	}
}

function requireAuth( nextState, replaceState ) {

	if (!authStore.isLoggedIn()) {
		replaceState(
			{nextPathName: nextState.location.pathname },
			'/login/' + (nextState.location.hash || "")
		);
	}

}

function checkRedirect( nextState, replaceState ){
	const redirectPaths = [
		'/login',
		'/login/',
		'/signup',
		'/signup/',
		'/welcome',
		'/welcome/',
		'/'
	];
	const profile = profileStore.getProfile();
	if( profile ){ //Then we are logged in
		const locationState = nextState.location.state
		? nextState.location.state
		: {};

		if( !profile.id ){
			locationState.profile = profile;
			if( nextState.location.pathname === '/welcome'){ return void 0;}
			locationState.nextPathName = nextState.location.pathname;
			replaceState(
				locationState,
				'/welcome'
			);
		} else if (
			redirectPaths.indexOf( nextState.location.pathname ) > -1
		) {
			return replaceState(
				locationState,
				'/bridge/'+ profile.username + '/home'
			);
		}
	}
}

function redirectToHome(nextState, replaceState) {
	const profile = profileStore.getProfile();
	if( profile ){
		return replaceState(null, '/bridge/'+ profile.username + '/home');
	} else {
		replaceState(null, '/login');
	}
}

// declare our routes and their hierarchy
const routes = (
	<Route path="/"
		component={BridgeApp}
		onEnter={checkRedirect}
		>
		<IndexRoute
			component={Index}
			onEnter={redirectToHome}
		/>

		<Route path="login" component={ Login }/>
		<Route path="signup" component={ SignUp }/>
		<Route path="welcome"
			component={ Welcome }
			onEnter={ requireAuth }
		/>
		<Route path="bridge/:username/:bname"
			component={ Panels }
			onEnter={ requireAuth }
			>
			<Route path="user/(:uid)" component={Profile}/>
			<Route path="invite" component={Invites}/>
			<Route path="detail" component={BridgeDetail}/>
			<Route path="search/:searchstring" component={Search}/>
		</Route>
		<Route path="bsearch(/)" component={ExploreBridge} />
		<Route path="ebauth" component={EBAuth} onEnter={checkEBAuth}/>
		<Route path="muauth" component={MUAuth} onEnter={checkMUAuth}/>
		<Route path="silverbridge" component={NotFound} />
		<Route path="*" component={NotFound} />
	</Route>
);

module.exports = routes;
