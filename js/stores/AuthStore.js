// AuthStore.js
// Store for authdata
// Listens to login and logout actions
"use strict";

import Reflux from 'reflux';

import actions from '../actions/AuthActions';

module.exports = Reflux.createStore({
	listenables: actions,

	_authData:  {},

	getInitialState: function() {

		let eventName;
		if(this.isLoggedIn()){
			eventName = 'login';
		}else{
			eventName = 'logout';
		}

		return {'eventName': eventName, 'data': this._authData};

	},

	/*SETTERS*/
	_setAuthData: function ( authData ) {

		this._authData = authData;

	},

	login: function ( authData ) {

		return this._setAuthData( authData );

	},

	logout: function () {

		return this._setAuthData( {} );

	},

	onLogin: function(authData){

		this.login( authData );
		return this.trigger( 'login', authData );

	},

	onLogout: function(){

		this.logout();
		return this.trigger( 'logout' );

	},

	getAuthData: function(){

		return this._authData;

	},

	isLoggedIn: function(){

		return !!Object.keys(this._authData).length;

	}

});
