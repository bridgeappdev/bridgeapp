/* BridgeStore.js
*  Creates a DataStore that listens to ReplyStore
*/
/*jshint esnext:true*/
'use strict';

import  Reflux from 'reflux';
import actions from '../actions/ReplyActions';

const ReplyStore = Reflux.createStore({
	listenables: actions,
	_repliesIndex:{},
	_threadList:{},
	_replyThread:[],

	triggerChange: function(event, data) {

		this.trigger(event, data);
	},

	/*GETTERS*/
	getIndex: function(){
		return this._repliesIndex;
	},
	getList: function(){
		return this._threadList;
	},
	getThread: function(){
		return this._replyThread;
	},
	allRepliesLoaded: function(){
		return this._allRepliesLoaded;
	},

	/*SETTERS*/
	_setIndex: function( indexData ){
		return this._repliesIndex = indexData;
	},
	_removeIndex: function( indexKey ){
		delete this._repliesIndex[ indexKey ];
		return delete this._threadList[ indexKey ];
	},
	_setList: function( threadList ){
		return this._threadList = threadList;
	},
	_setThread: function( replyThread ){
		return this._replyThread = replyThread;
	},
	_addNewThreadMessage: function( message ){
		return this._replyThread.push( message );
	},
	_updateThreadMessage: function( message ){
		let messageUpdated = false;
		for ( let i = this._replyThread.length - 1; i >= 0; i-- ) {
			if (this._replyThread[i].id === message.id) {
				this._replyThread[i] = message;
				messageUpdated = true;
				break;
			}
		}
		return messageUpdated;
	},

	_removeThreadMessage: function( messageId ){
		let messageDeleted = false;
		for ( let i = this._replyThread.length - 1; i >= 0; i-- ) {
			if ( this._replyThread[i].id === messageId ) {
				this._replyThread.splice(i, 1);
				messageDeleted = true;
				break;
			}
		}
		return messageDeleted;
	},

	_addPreviousRepies: function ( previousReplies ){

		let newRepliesList = previousReplies.concat(this._replyThread);
		return this._setThread( newRepliesList );
	},

	_clearReplyStore: function(){
		this._repliesIndex = {};
		this._threadList = {};
		return this._replyThread = [];
	},
	_clearThread: function(){
		return this._replyThread = [];
	},

	updateIndex: function( index ){
		this._repliesIndex = indexData;
	},
	updateList: function( threadList ){
		return this._threadList = threadList;
	},
	updateThread: function( replyThread ){
		return this._replyThread = replyThread;
	},

	onLoadIndex: function( indexData ){
		this._setIndex( indexData );
		return this.triggerChange( "loadIndex", this._repliesIndex );
	},
	onUpdateIndex: function( messageId, replyIndex ){
		this._repliesIndex[messageId] = replyIndex;
		return this.triggerChange( "updateIndex", this._repliesIndex );
	},
	onRemoveIndex: function( messageId ){
		this._removeIndex( messageId );
		return this.triggerChange( "removeIndex", this._repliesIndex );
	},

	onLoadList: function( listData ){
		//this._setList( listData );
		return this.triggerChange( "loadList", this._threadList );
	},
	onNewReplyThread: function( newThread ){
		this._threadList[ newThread.id ] = newThread;
		return this.triggerChange( "updateList", this._threadList );
	},

	onReceiveThreadMessage: function( message ){
		this._addNewThreadMessage( message );
		return this.triggerChange( "updateThread", this._replyThread );
	},

	onUpdateThreadMessage: function( message ){
		const updated = this._updateThreadMessage( message );
		if( updated ){
			return this.triggerChange(
				"updateThread",
				this._replyThread
			);
		}
	},

	onRemoveThreadMessage: function( messageId ){
		const deleted = this._removeThreadMessage( messageId );
		if( deleted ){
			return this.triggerChange(
				"updateThread",
				this._replyThread
			);
		}
	},

	onAllRepliesLoaded: function(){
		return this._allRepliesLoaded = true;
	},

	onGetPreviousReplies: function( previousReplies ) {

		this._addPreviousRepies( previousReplies );
		this.triggerChange('getPreviousReplies', this._replyThread);

	},

	onCloseBridge: function( ){
		this._clearReplyStore();
		return this.triggerChange( "closeBridge" );
	},
	onCloseThread: function( ){
		this._clearThread();
		return this.triggerChange( "updateThread", this._replyThread );
	}

});

export default ReplyStore;
