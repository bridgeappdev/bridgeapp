// LoadStore.js
// Store for aggregating listeners to submit an event when the application
// data has been loaded into the stores
//
// Listens to bridge and profile actions
/*jshint node:true*/

var Reflux = require('reflux'),
		bridgeActions = require('../actions/BridgesActions'),
		profileActions = require('../actions/ProfileActions');

module.exports = Reflux.createStore({

	init: function() {
		'use strict';
//		this.joinLeading(bridgeActions.getMyBridges, profileActions.getProfile, this.triggerAsync);
	}

});

