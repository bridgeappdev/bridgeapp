/*
* stores/NotificationStore.js
* Flux store for the user notification data
*
* */
'use strict';

import Reflux from 'reflux';

// Local modules
import Actions from '../actions/NotificationActions';
import FireAPI from '../lib/FireAPI';

module.exports = Reflux.createStore({
	listenables: [
		Actions
	],

	notifications: [],

	getInitialState: function() {

		return this.notifications;
	},

	_setNotificationData: function(notifications){
		return this.notifications = notifications;
	},

	triggerChange: function(event, data) {

		this.trigger(event, data);
	},

	_addNotification: function(notification){
		return this.notifications.push(notification);
	},

	_updateNotificationList: function _updateNotificationList(notification){

		let change = false;
		for (let i = this.notifications.length - 1; i >= 0; i--){
			if(this.notifications[i].id === notification.id){
				this.notifications[i] = notification;
				change = true;
				break;
			}
		}

		return change;

	},

	onUpdateNotification: function(notificationData){
		if(this._updateNotificationList(notificationData)){
			this.triggerChange( 'update', this.notifications );
		}else{
			//Something odd is going on
			console.log('NOTIFICATIONSSTORE::onUpdateNotification ===> something is not quite right');
		}
	},

	onNewNotification: function(notificationData){
		this._addNotification(notificationData);
		this.triggerChange( 'new', this.notifications );
	}

	});
