/* stores/main.js
 *
 * Set up entry point for flux stores
 *
 * */
/*jshint node:true*/

module.exports = {

	authStore: require('./AuthStore'),

	bridgeStore:require('./BridgeStore'),

	eventBriteStore:require('./EventBriteStore'),

	profileStore:require('./ProfileStore'),

	meetupStore:require('./MeetupStore'),

	messageStore:require('./MessageStore'),

	profileStore:require('./ProfileStore'),

	replyStore:require('./ReplyStore')

};
