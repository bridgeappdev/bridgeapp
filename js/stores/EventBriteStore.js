// Node modules
var Reflux = require('reflux');
var _ = require('lodash');

// Local modules
var Actions = require('../actions/EventBriteActions');

module.exports = Reflux.createStore({
	listenables: [
		Actions
	],

	_events: [],

	getInitialState: function() {
		'use strict';

		return {'ebevents': this._events};
	},

	_setEvents: function(events){
		'use strict';

		this._events = events;
	},

	_triggerChange: function(event, data) {
		this.trigger(event, data);
	},

	onGetUserEvents: function(events){
		'use strict'

		this._setEvents( events );
		this._triggerChange('getUserEvents', this._events);
	},
	
	onUpdateEventsList: function(){
		'use strict'

		this._triggerChange('updateUserEvents', this._events);
	}

});
