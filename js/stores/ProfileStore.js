/*
* stores/ProfileStore.js
* Flux store for the user profile data
*
* */
'use strict';

import Reflux from 'reflux';

// Local modules
import profileActions from '../actions/ProfileActions';

module.exports = Reflux.createStore({
	listenables: [
		profileActions
	],

	profile: null,

	getInitialState: function() {

		return this._profile;
	},

	/*SETTERS*/
	_setProfileData: function( eventName, newProfile ){

		switch ( eventName ) {
			case 'profileUpdated':
				return this._profile.merge( newProfile );
				break;
			default:
				return this._profile = newProfile;
		}

	},

	triggerChange: function(event, data) {

		this.trigger(event, data);
	},

	saveProfile: function( Profile ) {

		this.triggerChange('change', this._profile);
	},

	/*GETTERS*/
	getProfile: function(){
		return this._profile;
	},

	/*ACTION HANDLERS*/
	onUpdateProfile: function( updatedProfile ){

		const eventName = this._profile
			? 'profileUpdated'
			: 'profileLoaded'

		this._setProfileData( eventName, updatedProfile );
		this.triggerChange(eventName, this._profile);

	},

	onUpdateBridgeIndex: function(){
		return this.triggerChange('updateBridgeIndex', this._profile);
	},

	onUpdateContactIndex: function(){
		return this.triggerChange('updateContactIndex', this._profile);
	},

	onUpdateEventIntegration: function(){
		return this.triggerChange('updateEventIntegration', this._profile);
	}

});
