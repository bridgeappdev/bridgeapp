/* BridgeStore.js
*  Creates a DataStore that listens to ProfileStore
*/
'use strict';

import Reflux from 'reflux';
import bridgeActions from '../actions/BridgesActions';

const BridgeStore = Reflux.createStore({

	listenables: bridgeActions,

	_bridges: {},

	_currentBridge: {},

	getInitialState: function() {

		return {
			'eventName': 'initBridgeStore',
			'data': {
				'bridges': this._bridges,
				'currentBridge': this._currentBridge
			}
		};

	},

	emitEvent: function( eventName, data ){

		return this.trigger( eventName, data );

	},
	/*SETTERS */
	_loadBridges: function ( bridgeList ) {

		return this._bridges = bridgeList;

	},

	_addBridge: function ( bridge ){
		const isUpdate = !!this._bridges[bridge.id];

		this._bridges[bridge.id] = bridge;
		if( isUpdate ){
			this.onUpdateBridge( bridge );
		}

		return isUpdate;
	},

	_setCurrentBridge: function (bridge){

		this._currentBridge = bridge;

	},

	/*GETTERS*/

	getCurrentBridge: function() {

		if(Object.keys(this._currentBridge).length > 0){
			return this._currentBridge;
		}else{
			return null;
		}
	},

	getBridgeFromPath: function( bridgePath ) {

		const bridges = this._bridges;

		for (let bridgeId in bridges) {
			if (bridges.hasOwnProperty( bridgeId )) {
				if( bridges[bridgeId].path === bridgePath ) {
					return bridges[bridgeId]
				}
			}
		}

		return void 0;
	},

	/*ACTION HANDLERS*/

	onGetMyBridges: function( bridgeData ){

		this._loadBridges( bridgeData );
		this.emitEvent('bridgesLoaded', this._bridges);
	},

	onBridgesLoaded: function( ){

		this.emitEvent('bridgesLoaded', this._bridges);
	},

	onReceiveNewBridge: function( bridge ){

		this._addBridge( bridge );
		return this.emitEvent( 'receivedBridge', this._bridges );
	},

	onOpenBridge: function( bridge ){

		this._setCurrentBridge( bridge );
		this.emitEvent( 'openBridge', this._currentBridge );
	},

	onUpdateBridge: function ( bridge ){

		if( this._currentBridge.id === bridge.id ){
			this._setCurrentBridge( bridge );
			this.emitEvent( 'updateBridge', this._currentBridge );
		}

	}

});

module.exports = BridgeStore;
