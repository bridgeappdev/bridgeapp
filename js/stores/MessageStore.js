/*
 *  stores/MessageStore.js
 *
 *  Creates 'flux' store using Reflux for the messages
 *  Holds and manages a message cache.
 */
'use strict';

const Reflux = require('reflux'),
			bridgeStore = require('./BridgeStore'),
			actions = require('../actions/MessageActions');

const MessageStore = Reflux.createStore({

  listenables: actions,

	_messageCache: {},

	_messageList: [],

	init: function() {

		this.listenTo(
			bridgeStore,
			this.onChangeOpenBridge,
			this.initBridgeStoreListener
		);

	},

 /* GETTERS */
	getInitialState: function() {

		return {'data': this._messageCache};

	},

	allMessagesLoaded: function(){
		return this._allMessagesLoaded;
	},

	emitEvent: function( eventName, data ){

		return this.trigger( eventName, data );

	},

  initBridgeStoreListener: function( data ){

		this._currentBridge = data.data.currentBridge;
	},

	_addMessage: function _addMessage( message ){

		let messageAdded = false;
		let checkRepeatLastMessage = false;
		const noOfMessages = this._messageList.length;
		if(noOfMessages){
			checkRepeatLastMessage =
				this._messageList[noOfMessages - 1].id === message.id;
		}
	 	if(!checkRepeatLastMessage){
			this._messageList.push( message );
			messageAdded = true;
		}

		return messageAdded;
	},

	_addPreviousMessages: function _addPreviousMessages(previousMessages){

		let newMessageList = previousMessages.concat(this._messageList);
		return this._setMessageList( newMessageList );
	},

	_updateMessage: function _updateMessage( message ) {
			let messageUpdated = false;
			for ( let i = this._messageList.length - 1; i >= 0; i-- ) {
				if (this._messageList[i].id === message.id) {
					this._messageList[i] = message;
					messageUpdated = true;
					break;
				}
			}
			return messageUpdated;
	},

	_removeMessage: function _removeMessage( message ){

		var messageRemoved = false;
		for (let i = this._messageList.length - 1; i >= 0; i--){
	 		if(this._messageList[i].id === message.id){
				this._messageList.splice(i, 1);
				messageRemoved = true;
				break;
			}
		}
	},

	_updateCache: function(bridgeName, messageListToCache){

		this._messageCache[bridgeName] = messageListToCache;

	},

	_setMessageList: function(messages){

		this._messageList = messages;
	},

	onChangeOpenBridge: function(eventName, data){

		if(eventName === 'openBridge'){
			this._currentBridge = data;
			this._allMessagesLoaded = false;
		}
	},

	onReceiveNewMessage: function( newMessage ) {

		if( newMessage.bridgeId === this._currentBridge.id ){
			if( this._addMessage( newMessage ) ){
				this.emitEvent( 'newMessage', this._messageList );
			} else {
				console.log(
					'MESSAGESTORE::onReceiveNewMessage ===> Message is already in the list: ',
					 newMessage
				 );
			}
		}else{
		//Something odd is going on
			console.log(
				'MESSAGESTORE::onReceiveNewMessage ===> message is from another bridge',
				 newMessage
			 );
		}
	},

	onUpdateMessage: function ( updatedMessage ) {

		this._updateMessage( updatedMessage );
		this.emitEvent( 'updateMessage', this._messageList );
	},

	onRemoveMessage: function(removedMessage) {

		this._removeMessage(removedMessage);
		this.emitEvent('removeMessage', this._messageList);
	},

	onCacheBridgeContent: function(bridge, messages) {

		this._updateCache(bridge.id, messages);
		this.emitEvent('cachedContent', this._messageCache);

	},

	onGetMessageList: function(messages) {

		this._setMessageList(messages);
		this.emitEvent('newMessageList', this._messageList);

	},

	onAllMessagesLoaded: function(){
		return this._allMessagesLoaded = true;
	},

	onGetPreviousMessages: function( previousMessages ) {

		this._addPreviousMessages(previousMessages);
		this.emitEvent('getPreviousMessages', this._messageList);

	}

});

module.exports = MessageStore;
