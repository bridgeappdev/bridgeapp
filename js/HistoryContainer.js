// HistoryContainer.js

import createHistory from 'history/lib/createBrowserHistory'

module.exports = createHistory( { queryKey: false } );
