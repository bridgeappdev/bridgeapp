//BridgeActions.js
//
// Set up bridge actions using reflux
/*jshint node:true*/

var Reflux = require('reflux');

module.exports = Reflux.createActions([
		"getMyBridges",
		"bridgesLoaded",
		"openBridge",
		"receiveNewBridge",
		"updateBridge"
]);
