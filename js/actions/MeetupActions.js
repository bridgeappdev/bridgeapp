// Modules
var Reflux = require('reflux');

module.exports = Reflux.createActions([
  'getUserEvents',
  'getUserGroups',
  'updateEventsList'
]);
