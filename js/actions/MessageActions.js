/* actions/MessageActions.js
*
* Set up message actions using reflux
*/
const Reflux = require('reflux');

module.exports = Reflux.createActions([
	"cacheBridgeContent",
	"getMessageList",
	"allMessagesLoaded",
	"getPreviousMessages",
	"receiveNewMessage",
	"removeMessage",
	"updateMessage"
	]);
