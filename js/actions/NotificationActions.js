// Modules
import Reflux from'reflux';

module.exports = Reflux.createActions([
		'newNotification',
		'updateNotification'
	]);
