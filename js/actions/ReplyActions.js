/*jshint esnext:true*/
'use strict';

import Reflux from 'reflux';

let replyActions = Reflux.createActions([
		"updateList",
		"loadList",
		"allRepliesLoaded",
		"newReplyThread",
		"closeThread",
		"loadIndex",
		"updateIndex",
		"removeIndex",
		"closeBridge",
		"receiveThreadMessage",
		"updateThreadMessage",
		"removeThreadMessage",
		"getPreviousReplies"
]);

export default replyActions;
