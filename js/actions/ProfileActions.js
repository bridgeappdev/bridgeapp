/* actions/ProfileActions.js
*
* reflux actions definitions for profile store
* */

import Reflux from 'reflux';
module.exports = Reflux.createActions([
	'updateProfile',
	'updateBridgeIndex',
	'updateContactIndex',
	'updateEventIntegration',
	'saveProfile',
	'setDefaultProfile'
]);
