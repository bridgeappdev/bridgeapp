/* Messages.js
 * React components for messages.
 * Props:
 * {
 *		bridge: current bridge,
 *		isMember: boolean set for if the current user is a member of the open bridge
 *		currUser: profile object for user
 *		messages: array of bridge content objects
 * }
 */
"use strict";

import React      from 'react';
import ReactDOM   from 'react-dom';
import Reflux     from 'reflux';

import Linkify    from 'react-linkify';
import dateFormat from 'dateformat';
import moment     from 'moment';

import history from '../../HistoryContainer';

import BAPI       from '../../lib/BridgesAPI';
import MessageAPI from '../../lib/MessageAPI';

import ChatMessage from '../../models/messages/ChatMessage';

import AppBar from 'material-ui/AppBar';
import RaisedButton     from 'material-ui/RaisedButton';


import {List, ListItem} from 'material-ui/List';

import Colors           from 'material-ui/styles/colors';

import Snackbar from 'material-ui/Snackbar';

import Post  from './Post';
import Input from '../common/Input';
import DeleteMessageDialog from './DeleteMessageDialog';

module.exports = React.createClass ({

	displayName: "BridgeContent",

	_hasPreviousHistory: true,

	_hasPastHistory: false,

	_scrollToDivId: false,

	_messagesContentJQueryElem: false,

	getInitialState: function(){

		return {
			openDeleteDialog: false,
			openSnackBar: false,
			loadMessagesError:""
		};
	},

	_infiniteScrollTop: function(el, bridgeContentContext){

		if(!this.props.content.allMessagesLoaded){
			console.log("BRIDGECONTENT::_infiniteScrollTop ==> "
				            + "Let´s get some more messages");
			this._messagesContentJQueryElem = el.mcs.content;
			this.setState(
				{'loadingPrevious': true},
				this._loadPrevMessages
			);
		}
	},

	_updateScrollToDiv: function(){
		if(this._scrollToDivId.id){
			this._scrollToDivId.scrollNow = true;
		}
	},

	_loadPrevMessages: function(){
			let bridgeId = this.props.bridge.id;
			let lastMessageTimestamp = this.props.content[0].timestamp;
			let firstTimestamp = this.props.bridge.firstmessage;
			let noOfMessages = 100;
			let noOfDays = 6;

			//Record where the scroll needs to be.
			this._scrollToDivId = {
				'id': this.props.content[0].id,
				'pos': $(this._messagesContentJQueryElem).height()
			};

			MessageAPI.getPreviousMessages(
				bridgeId,
				lastMessageTimestamp,
				noOfDays, //Number of previous days to load
				noOfMessages,
				firstTimestamp,
				(err) => {
					let errMessage = "";
					let openSnackBar = false;
					if(err){
						//Error Message in snackbar
						errMessage = 'There was a problem loading previous messages.';
						openSnackBar = true;
					}
					this.setState(
						{
							'loadingPrevious':false,
							'loadMessagesError': errMessage,
							'openSnackBar': openSnackBar
						},
						this._updateScrollToDiv
					);
				}
			);
	},

	_infiniteScrollBottom: function(el, bridgeContentContext){

		if(this._hasPastHistory){
			console.log("BRIDGECONTENT::_infiniteScrollBottom ==> Let´s get some more messages");
		}

	},

	componentDidMount: function(){
		let _self = this;
		$("#message-content").mCustomScrollbar({
			autoHideScrollbar:true,
			theme:"dark",
			callbacks: {
				onTotalScrollBack: function(){
					_self._infiniteScrollTop(this, _self);
				},
				onTotalScroll: function(){
					_self._infiniteScrollBottom(this, _self);
				}
			}
		});
		//TODO Run the scrollTo after the messages are rendered
		window.setTimeout(()=>{
			$("#message-content").mCustomScrollbar("scrollTo","bottom");
		}, 2000);

		$(document).on('mouseenter', '.panel2-message', function () {
				$(this).find('.date').show();
				$(this).find('.options').show();
		}).on('mouseleave', '.panel2-message', function () {
				$(this).find(".date").hide();
				$(this).find('.options').hide();
		});
	},

	componentDidUpdate: function(){

		if(this._scrollToDivId.pos && this._scrollToDivId.scrollNow){
			let scrollTo = $('#mCSB_8_container').height() - (this._scrollToDivId.pos + 50);
			$("#message-content").mCustomScrollbar(
				"scrollTo",
				scrollTo,
				{scrollInertia:0,scrollEasing:"linear"}
			);
			window.setTimeout(function(){
				$("#message-content").mCustomScrollbar(
					"scrollTo",
					scrollTo - 200,
					{scrollInertia:3000}
				);
			},1000);
			this._scrollToDivId = false;
		}else{
			$("#message-content").mCustomScrollbar("scrollTo","bottom");
		}
	},

	_handleSnackbarClose: function(){
		this.setState({
			openSnackBar: false,
		});
	},

	render: function(){
		"use strict";

		var thisBridge = this.props.bridge,
				isBridgeMember = false,
				currUser = this.props.currUser,
				messagesList = this.props.content?this.props.content:[];

		if(thisBridge) {
			isBridgeMember = (
				thisBridge.name === 'home'
				|| thisBridge.isMember(currUser.id)
			);
		}

		let lastMessageUserId;
		let currentDate,messageDate;
		let messages=[];
		let containerDays = [];
		let currUserId = this.props.currUser.id;
		let openDialog = this.openDeleteDialog;
		let _self = this;
		messagesList.map(function(message){
			let isSameUser = (lastMessageUserId === message.uId);
			let isOwner = (currUserId === message.uId);
			lastMessageUserId = message.uId;
			messageDate = dateFormat(
				new Date(message.timestamp?message.timestamp:message.createdAt),
				'yyyy-mm-ddthh:mm:ss'
			);
			if(!currentDate){
				currentDate = messageDate;
			}
			if(currentDate!=messageDate){
				var containerDay = (
					<div key={currentDate} className="day_container">
						<h2><span>{moment(currentDate).calendar(null, {
								nextDay: '[Tomorrow]',
								lastDay: '[Yesterday]',
								lastWeek: '[Last] dddd',
								sameElse: 'dddd Do MMMM'
							})}</span></h2>
						<div>{messages}</div>
					</div>
				);
				currentDate= messageDate;
				messages =[];
				containerDays.push(containerDay);
				isSameUser = false;
			}

			messages.push(<Post
				key={message.id}
				sameUser={isSameUser}
				isOwner={isOwner}
				data={message}
				openDeleteDialog={openDialog}
				newReply={_self.props.newReply}
			/>);
		});
		if(messages.length>0){
			var containerDay = (
				<div key={currentDate} className="day_container">
					<h2><span>
						{moment(currentDate).calendar(null, {
							sameDay: '[Today]',
							nextDay: '[Tomorrow]',
							lastDay: '[Yesterday]',
							lastWeek: '[Last] dddd',
							sameElse: 'dddd Do MMMM'
						})}</span></h2>
					<div>{messages}</div>
				</div>
			);
			containerDays.push(containerDay);
		}

		var joinButton = <RaisedButton
			label="Join Bridge"
			primary={true}
			fullWidth={true}
			onClick={this.handleJoinBridge}
		/>;

		return (
			<div id="bridge-content" >
				<div
				  ref="message_content"
				  id="message-content"
				>
					<List>
						{this.state.loadingPrevious?<div>Loading history...</div>:null}
						{containerDays}
					</List>
				</div>

				<Toolbar  className="panel-2-footerbar" >
					{isBridgeMember ? <Input
						currUser={this.props.currUser}
						bridge={this.props.bridge}
						handleKeyDown={this._handleKeyDown}/> : joinButton}
				</Toolbar>

				<DeleteMessageDialog
					message={this.state.openDeleteDialog}
					closeDeleteDialog={this.closeDeleteDialog}
				/>

			</div>
		);
	}
});
