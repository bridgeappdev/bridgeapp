/*
* /components/panel3/Header.jsx
*
* React component for the panel three header

*/
'use strict';

import React from 'react';

import SearchInput from './SearchInput';

export default function Panel3Header ( props ) {

	return(

		<div  className='panel-3-header' >
			<div className={'panel-3-header-search-input'}>
				
				<SearchInput
					history={ props.history }
				/>
				</div>

		</div>

	)
}
