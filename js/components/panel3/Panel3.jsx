//jshint esnext:true
'use strict';

import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import replyStore from '../../stores/ReplyStore';
import replyActions from '../../actions/ReplyActions';
//TODO WTF??
import RepliesAPI from '../../models/replies/Replies';
const seeReplyAPI = RepliesAPI;

import ReplyList from './replies/ReplyList';
import ReplyThread from './replies/ReplyThread';

import Panel3Header from './Header';

import Paper from 'material-ui/Paper';

export default class PanelThree extends React.Component{
	constructor( props ){

		super( props );

		this.state = {
			repliesIndex: {}, //The reply index from the user profile
			threadList: {}, //Object of message objects
			replyThread: [], //Array of reply message objects.
		};

		this._handleClickReply = this._handleClickReply.bind( this );
		this._handleClickClose = this._handleClickClose.bind( this );
		this._addMessageToReplyIndexIfNew =
			this._addMessageToReplyIndexIfNew.bind( this );
		this._checkLastViewed = this._checkLastViewed.bind( this );

	}

	_onChangeFromReplyStore( eventName, data ){
		switch ( eventName ) {
			case "loadIndex":
			case "updateIndex":
				this.setState(
					{
						'repliesIndex': data,
						'event': eventName,
					}
				);

				break;
			case "removeIndex":
				this.setState(
					{
						'repliesIndex': data,
						'event': eventName,
					}
				);
				if(
					this.props.messageToReply
					&& !this.state.repliesIndex.hasOwnProperty(
						this.props.messageToReply.id
					)
				){
					this._closeReplyThread(
						this.props.messageToReply
					);
					this.props.handleClickClose();
				}

				break;
			case "updateList":
					this.setState(
						{
							'threadList': data,
							'event': eventName,
						}
					);

				break;
			case "updateThread":
			case "getPreviousReplies":
					this.setState(
						{
							'replyThread': data,
							'event': eventName,
						}
					);

				break;
			case "loadList":

				this.setState(
					{
						'threadList': data,
						'event': eventName
					}
				);

				break;
			case "loadThread":

				this.setState(
					{
						'replyThread': data,
						'event': eventName,
					}
				);

				break;
			case "closeBridge":

				this.setState(
					{
						repliesIndex: {},
						threadList: [],
						replyThread: []
					}
				);
				break;
			default:

		}
	}

	getRepliesForBridge( bridge ){
		const _self = this;
		//RepliesApi.getRepliesForBridge(
		seeReplyAPI.getRepliesForBridge(
			bridge.id,
			this.props.userProfile.userRef,
			( err ) => {
				if( err ) {
					/* Use the ticker to tell the user*/
					return _self.setState( { "threadList": {} } );
				}
			}
		)
	}

	closeBridgeReplies( bridge ){
		const _self = this;
		//RepliesApi.closeBridgeReplies(
		seeReplyAPI.closeBridgeReplies(
			bridge.id,
			this.props.userProfile.userRef,
			( err ) => {
				if( err ) {
					/* Use the ticker to tell the user*/
					return _self.setState( { "threadList": {} } );
				}
			}
		)
	}

	_openReplyThread( message ){
		seeReplyAPI.getThread(
			this.props.bridge.id,
			this.props.userProfile.userRef,
			message.id,
			( err ) => {
				if( err ) {
					/* Use the ticker to tell the user*/
					return _self.setState( { "replyThread": [] } );
				}
			}
		)
	}

	_replyThreadIsNew(){
		return ! this.state.repliesIndex.hasOwnProperty(
			this.props.messageToReply.id
		);
	}

	_checkLastViewed( messageId ){
		if(
			this.state.repliesIndex.hasOwnProperty(
				messageId
			)
		){
			return this.state.threadList[ messageId ].replyMeta.lastReplyDate
				> this.state.repliesIndex[ messageId ].lastViewed;
		} else {
			return false;
		}
	}

	_addMessageToReplyIndexIfNew(){
		if(this._replyThreadIsNew()){
			this.props.userProfile.addNewReplyToIndex(
				this.props.bridge.id,
				this.props.messageToReply.id,
				( error ) => {
					console.log( "Add New Thread didn\'t work.");
				}
			);
		}
	}

	_closeReplyThread( message ){
		seeReplyAPI.closeThread(
			this.props.bridge.id,
			this.props.userProfile.userRef,
			message.id,
			( error ) => {
				if( error ){
					console.log( "closeThread didn\'t work. ==> ", error);
				}
			}
		);

		return replyActions.closeThread();
	}

	componentWillMount(){

		this.listenTo(
			replyStore,
			this._onChangeFromReplyStore
		);

		if(this.props.bridge){
			this.getRepliesForBridge(this.props.bridge, ( err ) => {
				if( err ){/*Report back to the user*/}
			});
		}

	}

	componentWillReceiveProps( nextProps ) {
		if( !nextProps.bridge ) { return void 0 }

		const changeBridge = (
			!this.props.bridge
			|| (this.props.bridge.id !== nextProps.bridge.id)
		);

		const newMessageToReply = (
			( !!nextProps.messageToReply
			&& (
				!this.props.messageToReply
				||(this.props.messageToReply.id !== nextProps.messageToReply.id)
				)
			)
			|| ( !nextProps.messageToReply && !!this.props.messageToReply )
		);

		switch (true) {
			case changeBridge:

				if( this.props.messageToReply ){
					this._closeReplyThread( this.props.messageToReply );
				}

				if( this.props.bridge ){
					this.closeBridgeReplies( this.props.bridge );
				}

				return this.getRepliesForBridge(
					nextProps.bridge,
					( err ) => {
						if( err ){/*Report back to the user*/}
					}
				);
				break;
			case newMessageToReply:

				if( this.props.messageToReply ){
					this._closeReplyThread( this.props.messageToReply );
				}

				if( nextProps.messageToReply ){
					return this._openReplyThread( nextProps.messageToReply );
				}
			default:

		}

	}

	_handleClickReply( selectedReply ){

		return this.props.onClickReply( selectedReply )

	}

	_handleClickClose( ){

		return this.props.handleClickClose( )

	}

	// shouldComponentUpdate(){
	// 	return ( this._renderList || this._renderThread );
	// }

	render(){
		if( !this.props.bridge ) { return null; }

		let content,header;
		header = <Panel3Header history={ this.props.history } />
		switch (true) {
			case !!this.props.messageToReply:
				content = <ReplyThread
					userProfile={this.props.userProfile}
					bridge={ this.props.bridge }
					isBridgeMember={ this.props.isBridgeMember }
					message={ this.props.messageToReply }
					content={ this.state.replyThread }
					handleClickClose={ this.props.handleClickClose }
					selectProfile={ this.props.selectProfile }
					addMessageToReplyIndexIfNew={
						this._addMessageToReplyIndexIfNew
					}
				/>;
			break;

			default:
				content = <ReplyList
					userProfile={ this.props.userProfile }
					threadList={ this.state.threadList }
					repliesIndex={ this.state.repliesIndex }
					bridge={ this.props.bridge }
					checkLastViewed={ this._checkLastViewed }
					handleClick={ this._handleClickReply }
				/>;


				break;
		}

		return (
			<div id="panel-3" className="mdl-color--white">
				{header}
				{ content }
			</div>
		);
	}
}
reactMixin(PanelThree.prototype, Reflux.ListenerMixin);
