import React from 'react';

import MessageInput from '../../common/MessageInput';

import ChatMessage from '../../../models/messages/ChatMessage';
import URLMessage from '../../../models/messages/URLMessage';

export default class FooterReplyThread extends React.Component{
	constructor(props){

		super(props);

		this._handleSend = this._handleSend.bind(this);

	}

	_handleSend( reply ) {

		const _self = this;
		reply.isReply = true;
		reply.replyMessageId = this.props.message.id;
		reply.send(
			this.props.bridge.id,
			( error ) => {
				if( error ){
					console.log('Reply was not sent', error);
				}else{
					console.log('Reply was sent');
					_self.props.addMessageToReplyIndexIfNew();
					_self.props.message.updateReplyCount( 'add' );
					_self.messageInput.clearText();
				}
			}
		);
	}

	render(){
		return (
			<div className="new-panel-2-footer">
				<MessageInput
					ref={ (ref) => this.messageInput = ref }
					userProfile={this.props.userProfile}
					bridge={ this.props.bridge }
					handleSend={ this._handleSend }
				/>
			</div>
		);
	}

}
