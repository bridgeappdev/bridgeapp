import React from 'react';
import Avatar from 'material-ui/Avatar';

export default ( props )=>{

	const item = props.item;
	const notification = props.hasNewMessages ? (<i className="fa fa-bell" aria-hidden="true"></i>) : null;
	const itemClassName = props.hasNewMessages ? 'thread-list-item thli-success clearfix' : 'thread-list-item clearfix';
	return (

		<div
			key={ item.id }
			className={itemClassName}
			onClick={ props.onClick }
		>
			<div className={ 'thread-list-item-avatar' } >
				<Avatar src={ item.avatar } />
			</div>
			<div className={ 'thread-list-item-content' } >
				<div className={ 'thread-list-item-content-user' } >
					{ item.username }
				</div>

				<div className={ 'thread-list-item-content-message' } dangerouslySetInnerHTML={{__html: item.meta.messageHTML}} />
			</div>
			<div className={ 'thread-list-item-content-notify' }>
				{ notification }
			</div>
		</div>

	);
}
