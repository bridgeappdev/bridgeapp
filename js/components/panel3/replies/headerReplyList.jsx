import React from 'react';

import { IconButton } from 'react-mdl';

export default function HeaderReplyList ( props ) {

		return (
			<header className="panel-header">
				<div className="mdl-layout__header-row">
					<span className="mdl-layout-title">Reply threads for <strong>{ props.bridgeName }</strong></span>
					<div className="mdl-layout-spacer"></div>
			</div>
			</header>
		);
}
