//jshint esnext:true
import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import Messages from '../../panel2/messages';

import HeaderReplyThread from './headerReplyThread';
import Footer from './footerReplyThread';

import replyStore from '../../../stores/ReplyStore';
import RepliesAPI from '../../../models/replies/Replies';

import moment from 'moment';

export default class ReplyThread extends React.Component {

	constructor(props) {

		super(props);

	}

	_loadPrevReplies( next ){
		const oldestMessageId = this.props.content[0].id;
		const noOfMessages = 100;

		RepliesAPI.getPreviousReplies(
			this.props.bridge.id,
			this.props.message.id,
			oldestMessageId,
			noOfMessages,
			( err ) => {
				next( err );
			}
		);
	}

	render(){

		let message = (
			<div
				className="panel-3-message-content mdl-color--grey-50"
				dangerouslySetInnerHTML={
					{__html: this.props.message.meta.messageHTML}
				}
			/>

		);

		const messageList = (
			this.props.content)
			?	this.props.content
			:	[];

		return (
			<div className="ReplyThread">

				<HeaderReplyThread
					message = { this.props.message }
					bridgeId={ this.props.bridge.id }
					onClickClose={ this.props.handleClickClose }
					userProfile={ this.props.userProfile }
				/>

				{ message }
				<Messages
					messages={messageList}
					allMessagesLoaded={ replyStore.allRepliesLoaded() }
					loadPrevMessages={ this._loadPrevReplies.bind( this ) }
					isBridgeMember={ this.props.isBridgeMember }
					userProfile={this.props.userProfile}
					selectProfile={ this.props.selectProfile }
					isReplyThread={ true }
				/>


				<Footer
					userProfile={ this.props.userProfile }
					bridge={ this.props.bridge }
					message={ this.props.message }
					addMessageToReplyIndexIfNew={
						this.props.addMessageToReplyIndexIfNew
					}
				/>

			</div>
		);
	}
}

reactMixin(ReplyThread.prototype,  Reflux.ListenerMixin);
