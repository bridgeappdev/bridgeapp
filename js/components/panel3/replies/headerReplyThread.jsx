'use strict';

import React from 'react';

import Avatar from 'material-ui/Avatar';
import { Menu, IconButton, MenuItem } from 'react-mdl';

export default class HeaderReplyThread extends React.Component {

	constructor( props ) {

		super( props );

		this._removeThread = this._removeThread.bind( this );
	}

	_removeThread(){
		this.props.userProfile.removeReplyFromIndex(
			this.props.bridgeId,
			this.props.message.id,
			( err ) => {
				if( err ){
					//Tell the user
				} else {
					this.props.message.removeListeners();
					this.props.onClickClose();
				}
			}
		)
	}

	render(){

		return (
			<header className="panel-header mdl-color--grey-100">

				<div className="mdl-layout__header-row">
					<IconButton
						name="keyboard_arrow_left"
						onClick={ this.props.onClickClose }
					/>
					<Avatar src={ this.props.message.avatar } />
					<span
						className="mdl-layout-title"
					>
						&nbsp;&nbsp;{ this.props.message._username }
					</span>
					<div className="mdl-layout-spacer"></div>
					<IconButton name="keyboard_arrow_down" id="reply-thread-menu" />
					<Menu target="reply-thread-menu" align="right">
						<MenuItem onClick={ this._removeThread }>
							<i className="fa fa-trash-o" aria-hidden="true"></i> Remove Thread
						</MenuItem>
					</Menu>
				</div>
			</header>
		);
	}
}
