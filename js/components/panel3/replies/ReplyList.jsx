//jshint esnext:true
import React from 'react';

import Avatar from 'material-ui/Avatar';

import {List, ListItem} from 'material-ui/List';
import { IconButton } from 'react-mdl';

import ReplyThreadItem from './ReplyThreadItem';
import HeaderReplyList from './headerReplyList';

export default class ReplyList extends React.Component {

	constructor(props) {

		super(props);

		this._checkLastViewed = this._checkLastViewed.bind( this );

	}

	_openReplyThread( message ){
		return this.props.handleClick( message )
	}

	_checkLastViewed( messageId ){
		return this.props.checkLastViewed( messageId );
	}

	render(){

		const replyThreadListItems = [];
		const threadList = this.props.threadList;
		const _self = this;

		if( !Object.keys(threadList).length ){
			replyThreadListItems.push(
				<div className='thread-list-empty'>
					<i className="material-icons">reply_all</i>
					<div className='thread-list-empty-message'>
						You are not following any reply threads
						on { this.props.bridge.name }
					</div>
				</div>
			)
		} else {

			let item;
			const threadListKeysSorted =
				Object.keys( threadList ).sort(
					function( a, b ){
						return threadList[b].replyMeta.lastReplyDate
							- threadList[a].replyMeta.lastReplyDate
					}
				);

			threadListKeysSorted.forEach( function( key ){
				item = threadList[key];
				replyThreadListItems.push(
					<ReplyThreadItem
						hasNewMessages={ _self._checkLastViewed( item.id ) }
						item={ item }
						onClick={_self._openReplyThread.bind( _self , item ) }
					/>
				);
			});
		}

		return (

			<div>
				<HeaderReplyList bridgeName={ this.props.bridge.name }/>
				<div className={'padder-md'}>
					<div className={ 'thread-list' }>
						{replyThreadListItems}
					</div>
				</div>
			</div>
		);

	}
}
