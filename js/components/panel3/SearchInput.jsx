'use strict';

import React from 'react';

import { Textfield } from 'react-mdl';

var delayTimer;
//TODO: Use this for the tag search.
const _handleTextChange = function _handleTextChange (e){
	if(delayTimer){
		clearTimeout(delayTimer);
	}
	delayTimer = setTimeout((function() {
		console.log("P3Header::_handleTextChange => ", this);
	}).bind(e.target.value), 1000);
}

const openSearchModal = function(searchString, history){

	var parts = location.pathname.split('/');
	parts = parts.slice(0, 4);
	let searchPath = parts.join('/') + '/search/' + escape(searchString);
	history.pushState({}, searchPath);
};

const _handleKeyUp = function(e){
	if ( e.which === 13 ) {
		e.preventDefault();

	 	let searchString = e.target.value;
		if( searchString === "") {
			return void 0;
		}

		return openSearchModal(searchString, this.history);
	}
};

const _getValueFromURL = function(){
	let parts = location.pathname.split('search/');
	if (parts.length === 2){
		return unescape(parts[1]);
	}else{
		return '';
	}
}

export default function SearchInput (props){

	return (
		<Textfield
			onKeyUp={_handleKeyUp.bind(props)}
			defaultValue={_getValueFromURL()}
			label="Expandable Input"
			expandable
			expandableIcon="search"
			/>
	)
}
