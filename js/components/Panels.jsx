/* Panels.js
* React components for bridge app.
* Entry point for the Panel UI
*/
"use strict";

import React  from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import profileStore from '../stores/ProfileStore';
import bridgeStore from '../stores/BridgeStore';

import Bridges from '../models/bridges/Bridges';

import LoadingPage  from './pages/Loading';

import PanelOne   from './panel1/Panel1';
import PanelTwo   from './panel2/Panel2';
import PanelThree from './panel3/Panel3';
import PanelFour  from './panel4/Panel4';

import BModal    from './common/BModal';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';


export default class Panels extends React.Component{

	constructor( props ){

		super( props );

		this.state = {
			renderPanels: false,
			bmodalContent: false,
			bridge: null,
			messageToReply: null
		};

		this._handleClickReply =
			this._handleClickReply.bind( this );
		this._handleCloseReplyThread =
			this._handleCloseReplyThread.bind( this );
		this.setBModalVisibility =
			this.setBModalVisibility.bind( this );
		this._selectProfile = this._selectProfile.bind( this );

	}

	onChangeFromBridgeStore( eventName, data ){

		if(eventName && data){

			switch (eventName){

				case 'bridgesLoaded':
					this.openBridge( this.props.params );
					this.setState({
						'event': eventName
					});
					break;

				case 'openBridge':
					const bridge = data;

					if(this.state && this.state.bridge){
						this.closeBridge(
							this.state.bridge
						);
					}

					this.setState({
						'bridge': bridge,
						'event': eventName,
						'renderPanels': true,
						'messageToReply': null
					});

					break;

				case 'updateBridge':

					this.setState({
						'bridge': data,
						'event': eventName
					});

					break;
			}
		}

	}

	initProfileStoreListener( data ){

		this.setState( { 'profile': data } );
	}

	onChangeFromProfileStore( eventName, data ){

		if(eventName && data){

			switch (eventName){
				default:
				this.setState({ 'profile': data, 'event': eventName });
				break;
			}
		}
	}

	componentWillMount(){

		this.listenTo(
			profileStore,
			this.onChangeFromProfileStore,
			this.initProfileStoreListener
		);
		this.listenTo(
			bridgeStore,
			this.onChangeFromBridgeStore
		);

		const openBridge = bridgeStore.getCurrentBridge()
		if( openBridge ){
			return this.openBridge( this.props.params );
		}

		if(
			this.props.location.state
			&& this.props.location.state.open
		){
			return this.openBridge( this.props.params );
		}

		return void 0;

	}

	componentWillReceiveProps( nextProps ){

		const bname = this.props.params.bname;
		const username = this.props.params.username;

		const nextbname = nextProps.params.bname;
		const nextusername = nextProps.params.username;

		if( bname !== nextbname || username !== nextusername ){
			this.openBridge( nextProps.params );
		}
	}

	openBridge( bridgeParams ){

		const _self = this;
		const pathFromParams = bridgeParams.username + '/' + bridgeParams.bname;
		const profile = this.state.profile
			? this.state.profile
			: profileStore.getProfile();

		const bridgeFromStore = bridgeStore.getBridgeFromPath(
			pathFromParams
		);

		if( bridgeFromStore ){
			return Bridges.openBridge(
				bridgeFromStore,
				profile
			);
		}

		Bridges.getBridgeByPath(
			bridgeParams,
			( error, bridge ) => {
				if( error ){
					return _self.props.history.replaceState(
						{},
						'/silverbridge'
					);
				}else{
					return Bridges.openBridge(
						bridge,
						profile
					);
				}
			}
		);

	}

	closeBridge( bridge ){

		return Bridges.closeBridge(
			bridge.id,
			this.state.profile,
			( err ) => {
				if(err){
					/*Don't really care, I think...*/
				}else{
					return void 0;
				}
			}
		);

	}

	_handleClickReply( message ){

		this.setState({
			messageToReply: message
		});

	}

	_handleCloseReplyThread( ){

		this.setState({
			messageToReply: null
		});

	}

	setBModalVisibility( modalContent ){

		return this.setState({bmodalContent: modalContent});
	}

	_isBridgeMember(){
		if(
			this.state.bridge
			&& this.state.profile
		){
			return this.state.bridge.isMember(
				this.state.profile.id
			);
		} else {
			return false;
		}
	}

	_selectProfile( uId ){

		let parts = location.pathname.split('/');
		let pathEnd = parts[4];
		if(pathEnd === 'user'){
			pathEnd = '/';
		}else{
			pathEnd = '/user/' + ( uId ? uId : '');
		}
		parts = parts.slice(0, 4);
		let userPath = parts.join('/') + pathEnd;
		this.props.history.pushState({}, userPath);
	}

	render() {

		if( !this.state.renderPanels ){
			return < LoadingPage profile={ this.state.profile } / >
		}

		return (
			<MuiThemeProvider >
			<main>
				<PanelOne
					userProfile={this.state.profile}
					history={ this.props.history }
					openBModal={this.setBModalVisibility}
					selectProfile={ this._selectProfile }
				/>
				<PanelTwo
					bridge={ this.state.bridge }
					isBridgeMember={ this._isBridgeMember() }
					history={ this.props.history }
					bridgeParams={ this.props.params }
					userProfile={ this.state.profile}
					openBModal={this.setBModalVisibility}
					onClickReply={ this._handleClickReply }
					selectProfile={ this._selectProfile }
				/>
				<PanelThree
					bridge={ this.state.bridge }
					isBridgeMember={ this._isBridgeMember() }
					history={ this.props.history }
					userProfile={ this.state.profile }
					onClickReply={ this._handleClickReply }
					messageToReply={ this.state.messageToReply }
					handleClickClose={ this._handleCloseReplyThread }
					selectProfile={ this._selectProfile }
				/>
				<PanelFour
					userProfile={this.state.profile}
					currBridge={this.state.bridge}
					isBridgeMember={ this._isBridgeMember() }
					content={this.props.children}
					openBModal={this.setBModalVisibility}
				/>
				<BModal
					content={this.state.bmodalContent}
					closeBModal={this.setBModalVisibility}
					userProfile={this.state.profile}
				/>
			</main>
			</MuiThemeProvider>
			);
	}
}

reactMixin(Panels.prototype, Reflux.ListenerMixin);
