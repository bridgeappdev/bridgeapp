/*
* /components/panel4/BridgeDetail/ChatBridgeDetails
*
* Component for viewing a chat bridge's details
*/

import React  from 'react';
import moment from 'moment';
moment().format();

module.exports = React.createClass({
	displayName: "Chat Bridge Detail",

	render (){

		let coverImgURL = this.props.bridge.img_url ? this.props.bridge.img_url : "/assets/img/default-image.png";

		return(
			<div>
				<div className='panel4-bridge-item'>
					<div className='bridge-det-title'>
						{this.props.bridge.title}
					</div>
					<div className='bridge-det-name'>
						Name: {this.props.bridge.name}
					</div>
					<div className='bridge-det-built'>
						Built: {moment(this.props.bridge.createdAt).calendar()}
					</div>
				</div>

				<div style={{width:'100%'}}>
					<img src={coverImgURL} className="img-centered" />
				</div>

				<div className='panel4-bridge-item'>
					<div className='desc-bridge-label'>
						Reason:
					</div>
					<div className='bridge-det-text'>
						{this.props.bridge.desc}
					</div>
				</div>
			</div>

		)
	},

});
