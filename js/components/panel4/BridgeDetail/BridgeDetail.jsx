/*
* /components/panel4/BridgeDetail.jsx
*
* Component for viewing a bridge's details
*/

import React  from 'react';
import Reflux from 'reflux';

import Header from '../../common/Header';
import ChatBridgeDetail from './ChatBridgeDetails';
import EventBridgeDetail from './EventBridgeDetail';

import FontIcon  from 'material-ui/FontIcon';

import BridgeSettings from '../../bridge/Settings';
import PanelBuildBridgeSteps from '../../panel1/BuildBridge/buildBridgeSteps';

export default class BridgeDetail extends React.Component{
	constructor(props){
		super(props);
	}
	_canEdit(){
		'use strict';

		return this.props.currBridge.isOwner(
			this.props.userProfile.username
		);

	}

	closePanel(){
		this.path = /\/$/.test(location.pathname) ? location.pathname : location.pathname + "/";
		return this.props.history.pushState({},this.path.split(/detail/)[0] );
	}

	_handleEditClick(){
		'use strict';

		/*return this.props.openBModal(
			<BridgeSettings
				initialData={this.props.currBridge}
				profile={this.props.currUser}
				/>
		);*/
		this.refs.PanelBuildBridgeSteps._open(this.props.currBridge.type,this.props.currBridge,true);
	}

	render() {
		'use strict';
		if(this.props.currBridge.id) {


			let content;
			switch (this.props.currBridge.type) {
				case 'chat':
				case 'CHAT':
				content = <ChatBridgeDetail bridge={this.props.currBridge} />
				break;
				case 'event':
				case 'EVENT':
				content = <EventBridgeDetail bridge={this.props.currBridge}/>
				break;
				default:

			}

			return (
				<div id="bridge-detail">
					<Header
						titleText="Bridge Detail"
						requestClose={this.closePanel.bind(this)}
						/>

					{this._canEdit()?
						<div className='panel4-bridge-edit'>
							<i className="material-icons" onClick={this._handleEditClick.bind(this)}>mode_edit</i>
						</div>:null
					}

					<div className="panel4--bridge-content">
						{content}
					</div>
					<PanelBuildBridgeSteps ref="PanelBuildBridgeSteps" profile = {this.props.userProfile}/>

				</div>
			);
		} else {
			return (
				<div className="center-loading-block">
					<i className="loading fa fa-cog fa-spin"></i>
				</div>
			);
		}
	}
}
