/*
* /components/panel4/profile.jsx
*
* Component for viewing a user profile by ID
* If the ID is not preent in the URL, show the current users profile.
*
*/
'use strict';

import React  from 'react';
import Reflux from 'reflux';

import FollowNotification from '../../models/notifications/FollowNotification';

import Header from '../common/Header';
import FollowButton from '../common/FollowButton';
import ProfileSettings from '../profile/Settings';

import FontIcon  from 'material-ui/FontIcon';
import { Button } from 'react-mdl';

import profileAPI from '../../models/profiles/Profiles';

module.exports = React.createClass({
	displayName: "Profile",

	getInitialState: function(){

		return {
			profile: {},
			canEdit: false,
			errMessage: null
		};
	},

	_setProfileState: function(uId){

		const isCurrentUser = (
			!uId
			|| uId === this.props.userProfile.username
		);
			const errMessage = "There was a problem loading the profile."
			+ " Maybe give it another go?";

			if(!isCurrentUser){
				profileAPI.getProfileById( uId, ( err, profile )=>{
					if(err){
						return this.setState(
							{
								'profile':null,
								'errMessage': errMessage
							}
						);
					}else {
						return this.setState(
							{
								'profile': profile,
								'canEdit': false,
								'errMessage': null
							}
						);
					}
				});
			}else{ //It is current user profile.
				this.setState(
					{'profile': this.props.userProfile,
						'canEdit': true
					}
				);
			}

		},

		componentWillMount: function(){

			const uId = this.props.params.uid;
			return this._setProfileState(uId);

		},

		componentWillReceiveProps: function(nextProps){

			const uId = nextProps.params.uid;
			if(uId !== this.props.params.uid){
				return this._setProfileState(uId);
			}
		},

		closePanel: function(){

			this.path = /\/$/.test(location.pathname)
				? location.pathname
				: location.pathname + "/";

			return this.props.history.pushState(
				{},
				this.path.split(/user/)[0]
			);
		},

		_handleEditClick: function(){

			return this.props.openBModal(
				<ProfileSettings
					initialProfile={this.props.userProfile}
					/>
			);
		},

		_sendFollowNotification: function( followType ) {
			if( !( followType === 0 || followType === 1 ) ){
				return void 0;
			}

			const me = this.props.userProfile;
			const notificationDetails = {
				whoId: me.id,
				who: me.username,
				avatar: me.avatar,
				mutual: followType === 0 ? false : true
			};

			const Notification = new FollowNotification( notificationDetails );
			Notification.send( this.state.profile.id, ( err ) => {
				if( err ){
					console.log(
						'Error sending follow notification:: ',
						notificationDetails
					);
				}
			});

		},

		_handleFollowClick: function( followType ){

			const profileID = this.state.profile.id;
			profileAPI.updateContact(followType, profileID, (err) =>{
				if(err){

				}else {
					this._sendFollowNotification( followType );
					return this.forceUpdate();
					//MAYBE this._setProfileState(this.props.params.uid);
				}
			});
		},

		_renderTopDiv: function(){

			if(this.state.canEdit){
				return (
					<div className='panel4-profile-edit'>
						<Button
							type='button'
							className="mdl-button mdl-js-button mdl-button--raised"
							onClick={ this._handleEditClick }
						>
							Edit Profile
						</ Button>
						<div style={{clear:'both'}}/>
					</div>
				)
			}else{
				const contacts = this.props.userProfile.contacts;
				const profileId = this.state.profile.id;
				return (
					<div className='panel4-profile-follow'>
						<FollowButton
							contactList={contacts}
							profileId={profileId}
							onClick={this._handleFollowClick}
							/>
						<div style={{clear:'both'}}/>
					</div>
				)
			}
		},

		render: function() {

			if(this.state.profile
				&& this.state.profile
				&& this.state.profile.username) {

					let avatarURL = this.state.profile.avatar ? this.state.profile.avatar : "/assets/img/profile-placeholder.png";

					let wrapImgCropStyle = {
						width: '180px',
						height: '180px',
						overflow: 'hidden',
						display: 'flex',
						margin: '1.6em auto'
					};
					let imgCropStyle = {
						display: 'flex'
					}

					return (


						<div id="profile-settings">

							<Header
								titleText="Profile"
								requestClose={this.closePanel}
								/>

							<div style={wrapImgCropStyle} className="img-rounded">
								<img src={avatarURL} height={180} style={imgCropStyle}/>
							</div>


							<div className="panel4--profile-content">

								<div className='profile-item-name'>
									{this.state.profile.name}
								</div>

								{this._renderTopDiv()}

								<div className='panel4-profile-item'>
									<div className="desc-bridge-label">
										<strong>Website:</strong><br/>
									</div>
									<div className='bridge-det-text' >
										<a title={this.state.profile.website} href="#">{this.state.profile.website}</a>
									</div>
								</div>

								<div className='panel4-profile-item'>
									<div className="desc-bridge-label">
										<strong>Bio:</strong><br/>
									</div>
									<div className='bridge-det-text'>
										{this.state.profile.bio}
									</div>
								</div>
							</div>
						</div>
					);
				} else {
					return (
						<div className="center-loading-block">
							<i className="loading fa fa-cog fa-spin"></i>
						</div>
					);
				}
			}
		});
