import React from 'react';
import Chat from './Chat';

const SearchMessage = (props) => (
	<div className='search-message-result'>
		<div className='search-message-bridge-name'>
			{props.bridgeName}
		</div>
		<div className='search-message-content'>
			<Chat
				message={props.message}
				selectProfile={props.selectProfile}
			/>
		</div>
	</div>
);

SearchMessage.propTypes = { message: React.PropTypes.object };

export default SearchMessage;
