import React from 'react';
import SearchMessage from './SearchMessage';

export default class SearchList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {messages: props.messageList};
		this.selectProfile = this.selectProfile.bind(this);
	}

	selectProfile (e) {
		console.log("Boom! ", e);
	}

	componentDidMount() {
		$(".search-list").mCustomScrollbar({
			autoHideScrollbar:true,
			theme:"dark"
		});
	}

	render() {
		const messages = [];
		Object.keys(this.props.messageList).forEach((function(messageId){
			let currMessage = this.props.messageList[messageId];
			let currMessageBridge = this.props.bridgeList[currMessage.bridgeid];
			let name = currMessageBridge?currMessageBridge.name:'bridge';
			messages.push(<SearchMessage
				message={currMessage}
				bridgeName={name}
				selectProfile={this.selectProfile}
				key={messageId}
			/>)
		}).bind(this))
		return (
			<div className='search-list'>
				<div>
					{messages}
				</div>
			</div>
		);
	}
}
//SearchList.propTypes = { messageList: React.PropTypes.object };
