import React from 'react';
var ReactDOMServer = require('react-dom/server');

import dateFormat  from 'dateformat';
import ReactEmoji  from 'react-emoji';
import Linkify     from 'react-linkify';

import Avatar       from 'material-ui/Avatar';

//We can't use dangerouslySetInnerHTML with ReactEmoji so we need to
// render any emoticon element as a string first.
const returnHTML = function(text) {
	const parsedMessage = ReactEmoji.emojify(text);
	let result = '';
	parsedMessage.forEach(function(e){
		if(typeof e === 'object'){
			result += ReactDOMServer.renderToString(e);
		}else{
			result += e;
		}
	});
	return {__html: result};
};
const Chat = (props) => (
		<div className='search-message-content-chat' >
			<div id={props.message.id} className="search-message withAvatar">
				<div className="search-detail-avatar">
					<div
						className="avatar-icon"
						style={{cursor:'pointer'}}
						onClick={props.selectProfile}
					>
						<Avatar src={props.message.avatarurl} />
					</div>
				</div>

				<div className="search-detail-message">
					<div className="user">
						{props.message.user} <span className="user-date">{
							dateFormat(new Date(props.message.timestamp),'h:MM')
						}
					</span>
					</div>
						<Linkify
							properties={{target: '_blank'}}
						>
								<p dangerouslySetInnerHTML={
										returnHTML(props.message.highlight)
								} />
						</Linkify>
				</div>

			</div>
		</div>
);

Chat.propTypes = { message: React.PropTypes.object };

export default Chat;
