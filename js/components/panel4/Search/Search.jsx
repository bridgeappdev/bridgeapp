'use strict';

import React from 'react';

import Reflux from 'reflux';


import bridgeStore from '../../../stores/BridgeStore';

import SearchAPI from '../../../lib/SearchAPI';


import Header from '../../common/Header';

import { Tabs, Tab } from 'react-mdl/lib/Tabs';

import SearchList from './SearchList';

const Search = React.createClass({
	displayName: 'Search',

	mixins: [Reflux.ListenerMixin],

	getInitialState(){
		return {
			'err': null,
			'activeTab': 0,
			'bridges': null
		}
	},

	onChangeFromBridgeStore: function ( eventName, data ){
		if( eventName === 'receivedBridge' ) {
			this.setState( {bridges: data} );
		}
	},

	initBridgeStoreListener: function (data){

		this.setState({bridges: data.data.bridges});

	},

	_runSearch(sString){
		const queryString =
			sString ? sString : this.props.routeParams.searchstring;
		const bridgeObj = this.props.userProfile.bridges;
		const bridgeList = Object.keys(bridgeObj);

		return SearchAPI.messages(
			queryString,
			bridgeList,
			this._searchCallback
		);
	},

	_searchCallback(err, result){
		if(err){
			return this.setState({'err': err});
		}

		return this.setState({
			'err': null,
			'result': result.data
		})

	},

	componentWillMount(){

		this.listenTo(
			bridgeStore,
			this.onChangeFromBridgeStore,
			this.initBridgeStoreListener
		);
		this._runSearch();
	},

	componentWillReceiveProps(nextProps){
		const sString = nextProps.params.searchstring;
		if( this.props.params.searchstring !== sString ){
			this._runSearch(sString);
		}
	},

	closePanel(){
		this.path = /\/$/.test(location.pathname) ? location.pathname : location.pathname + "/";
		return this.props.history.pushState({},this.path.split(/search/)[0] );
	},

	_renderError(){
		return (<p>There was a problema: {this.state.err}</p>)
	},

	getTabContent(){
		switch (this.state.activeTab){
			case 0:
				return <SearchList
					messageList={this.state.result.messages}
					bridgeList={this.state.bridges}
				/>;
			break;
			case 1:
			return (<div>Files Tab, Coming soon</div>);
			break;
		}
	},

	render(){
		let content;

		if(this.state.err){
			content = this._renderError();
		}else{
			if(this.state.result){
				content = this.getTabContent();
			}else{
				content = (<div className='no-search-results'>
					<p>No results for that search :(</p>
					</div>);
			}
		}
		return (
			<div className='panel4-search-results'>

				<Header
					titleText="Search"
					requestClose={this.closePanel}
				/>

				<div className='search-results'>
						<Tabs
						  activeTab={this.state.activeTab}
						  onChange={(tabId) => this.setState({ activeTab: tabId })}
						  ripple
						>
							<Tab>Chat</Tab>
							<Tab>Files</Tab>
						</Tabs>
						<section>
						<div className="search-results-content">
							{content}
						</div>
						</section>
					</div>
			</div>
		);
	}
});

export default Search;
