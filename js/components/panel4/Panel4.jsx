import React from 'react';
import Drawer from 'material-ui/Drawer';

export default class Panel4 extends React.Component {
	constructor(props) {
		super(props);
	}

	renderChildren() {
		//console.log(this.props.content);
		return React.Children.map(this.props.content, function (child) {
			return React.cloneElement(child, this.props);

		}.bind(this));
	}

	render(){

		let open = !!this.props.content;

		return (
			<Drawer containerClassName="panel4-wrapper" width={450} openSecondary={true} open={open}>
				<div className='panel4-content'>
					{this.renderChildren()}
					<div style={{clear:'both'}}/>
				</div>
			</Drawer>
		);
	}
}
