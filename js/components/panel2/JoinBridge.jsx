import React from 'react';

import { Button } from 'react-mdl';

export default class JoinBridge extends React.Component {
	constructor( props ){
		super( props );

		this._handleClickJoin = this._handleClickJoin.bind( this );
	}

	_handleClickJoin(){
		const profile = this.props.userProfile;
		const bridge = this.props.bridge;

		bridge.addMemberToBridge(
			profile.id,
			( err ) => {
				if( err ){
					//Stop right now!
					return console.log( 'Error joining bridge: ', err );
				} else {
					profile.joinBridge(
						bridge.id,
						( err ) => {
							if( err ){
								//Stop right now!
								return console.log( 'Error joining bridge: ', err );
							}
						}
					);
				}
			}
		);
	}

	render(){
		switch (true) {
			case ( location.search.indexOf('from=invite') > 0 ):
			return (
				<div className='join-bridge-container'>
					<div className='join-bridge-text' >
						You have been invited to { this.props.bridge.name }
					</div>
					<div className='join-bridge-button-container'>
						<Button
							type='button'
							raised
							onClick={ this._handleClickJoin }
						>
							Accept Invite <span></span>
						</ Button>
					</div>
				</div>
			);
			break;
		default:
		return (
			<div className='join-bridge-container'>
				<div className='join-bridge-text' >
					You are taking a peek at { this.props.bridge.name }
				</div>
				<div className='join-bridge-button-container'>
					<Button
						type='button'
						raised
						onClick={ this._handleClickJoin }
					>
						Join Bridge <span></span>
					</ Button>
				</div>
			</div>
		);
	}
	}

}
