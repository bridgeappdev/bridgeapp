import React from 'react';

import MessageInput from '../common/MessageInput';
import JoinBridge from './JoinBridge';

import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

export default class PanelTwoFooter extends React.Component{
	constructor(props){

		super(props);

		this._handleSend = this._handleSend.bind(this);

	}

	_handleSend( message ) {

		const _self = this;
		message.send( this.props.bridge.id, ( error ) => {
			if( error ){
				console.log('message was not sent', error);
			}else{
				console.log('message was sent');
				_self.props.bridge.updateLastMessageDate();
				_self.messageInput.clearText();
			}
		});
	}

	render(){

		if( this.props.isBridgeMember ){
			return (
				<div className="new-panel-2-footer">
					<MessageInput
						ref={ (ref) => this.messageInput = ref }
						userProfile={ this.props.userProfile }
						bridge={ this.props.bridge }
						handleSend={ this._handleSend }
						dropzone = {this.props.dropzone}
					/>
				</div>
			);
		}else {
			return (
				<div className="new-panel-2-footer">
					<JoinBridge
						userProfile={ this.props.userProfile }
						bridge={ this.props.bridge }
					/>
				</div>
			);
		}
	}

}
