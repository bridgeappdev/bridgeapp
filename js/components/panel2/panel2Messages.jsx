'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import MessagesAPI from '../../models/messages/Messages';
import messageStore from '../../stores/MessageStore';

import Messages from './messages';
import ContainerDay from './messages/container-messages';
import MessageItem from './messages/messageItem';
import FirstMessage from './messages/firstmessage';

import dateFormat from 'dateformat';
import moment     from 'moment';

export default class PanelTwoMessages extends React.Component{
	constructor(props){
		super(props);

	}

	_loadPrevMessages( next ){
			const bridgeId = this.props.bridge.id;
			const oldestMessageId = this.props.messages[0].id;
			const noOfMessages = 100;

			MessagesAPI.getPreviousMessages(
				bridgeId,
				oldestMessageId,
				noOfMessages,
				( err ) => {
					next( err );
				}
			);
	}

	render(){

		let firstMessage = null;
		const allMessagesLoaded = messageStore.allMessagesLoaded();
		if( allMessagesLoaded ){
			const currUsername = this.props.userProfile.username;
			const isOwner = this.props.bridge.isOwner( currUsername );
			firstMessage = (
				<FirstMessage
					bridge={ this.props.bridge }
					profile={ this.props.userProfile }
					openBModal={ this.props.openBModal }
					history={ this.props.history }
					owner={ isOwner }
				/>
			);
		}

		return(
			<Messages
				loadPrevMessages={ this._loadPrevMessages.bind( this ) }
				firstMessage={ firstMessage }
				messages={this.props.messages}
				allMessagesLoaded={ allMessagesLoaded }
				bridge={ this.props.bridge }
				userProfile={this.props.userProfile}
				isBridgeMember={ this.props.isBridgeMember }
				onClickReply={ this.props.onClickReply }
				onClickDelete={ this._onClickDelete }
				selectProfile={ this.props.selectProfile }
			/>
		);
	}

}
