import React from 'react';

import { Menu,IconButton,MenuItem} from 'react-mdl';

function _openDetailModal(  history ){

	let parts = location.pathname.split('/');
	parts = parts.slice(0, 4);
	const invitePath = parts.join('/') + '/detail';
	history.pushState({}, invitePath);
}

function _openInviteModal( history ){

	let parts = location.pathname.split('/');
	parts = parts.slice(0, 4);
	const invitePath = parts.join('/') + '/invite';
	history.pushState({}, invitePath);
};

export default class PanelTwoHeader extends React.Component{
	constructor(props){
		super(props);

		this.onClickStar = this.onClickStar.bind( this );
	}

	onClickStar(){
		return this.props.profile.toggleBridgeStar(
			this.props.bridge.id
		);
	}

	render(){
		const bridge = this.props.bridge;
		const isOwner = bridge.isOwner(
			this.props.profile.username
		);

		let starIconName = 'star_border';
		if(
			this.props.profile.bridges[
				bridge.id
			] &&
			this.props.profile.bridges[
				bridge.id
			].isFavorite
		){
			starIconName = 'star';
		}

		return (
			<header className="panel-header mdl-color--cyan-500 mdl-color-text--white">
				<div className="mdl-layout__header-row">
					<i className="material-icons panel-header-icon">chat</i>

					<span
						className="mdl-layout-title"
					>
						{ " Welcome to " + bridge.name }
					</span>
					<div className="mdl-layout-spacer"></div>

					<IconButton
						name={ starIconName }
						onClick={ this.onClickStar }
					/>

					<IconButton name="keyboard_arrow_down" id="demo-menu-lower-right" />
					<Menu target="demo-menu-lower-right" align="right">
						<MenuItem
							onClick={_openDetailModal.bind( null,this.props.history)}
						>
							<i className="fa fa-info" aria-hidden="true"></i> View Details
						</MenuItem>
						{( isOwner && !bridge.isHome )?
							<MenuItem
								onClick={_openInviteModal.bind( null, this.props.history )}
							>
								<i className="fa fa-users" aria-hidden="true"></i> Invite People
							</MenuItem>
							: null
						}

					</Menu>
				</div>
			</header>
		);

	}

}
