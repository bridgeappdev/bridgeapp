'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import bridgeStore from '../../stores/BridgeStore';
import messageStore from '../../stores/MessageStore';

import MessagesAPI from '../../models/messages/Messages';

import Header from './header';
import Messages from './panel2Messages';
import Footer from './footer';
import Upload from './upload';

import Paper from 'material-ui/Paper';

export default class PanelTwo extends React.Component{

	constructor( props ){

		super( props );

		this.state = {
			messages: [],
		}
	}

	componentWillMount(){

		this.listenTo(
			messageStore,
			this.onChangeFromMessageStore
		);

		if(this.props.bridge){
			this.getBridgeContent(this.props.bridge, ( err ) => {
				if( err ){/*Report back to the user*/}
			});
		}

	}

	componentWillReceiveProps( nextProps ){
		if( !nextProps.bridge ) { return void 0 }
		if(
			!this.props.bridge ||
			(this.props.bridge.id !== nextProps.bridge.id)
		){
			this.getBridgeContent(nextProps.bridge, ( err ) => {
				if( err ){/*Report back to the user*/}
			});
		}
	}

	onChangeFromMessageStore( eventName, data ){

		if( eventName && data ){

			switch (eventName){
				case 'newMessage':
				case 'removeMessage':
				case 'updateMessage':
				this.forceUpdate();
				break;
				case 'newMessageList':
				case 'getPreviousMessages':
				this.setState({ 'messages': data, 'event': eventName });
				break;
			}
		}
	}

	getBridgeContent( bridge ){

		MessagesAPI.getBridgeMessages(
			bridge,
			this.props.userProfile.id,
			(error) => {
				if(error){
					/*Use a ticker component to report error*/
					return this.setState({ messages:  [] });
				}
			}
		);
	}

	_getLastMessage(){
		return this.state.messages[
			this.state.messages.length -1
		]
	}

	render(){

		if( !this.props.bridge ) { return null; }

		return (

			<div
				id="panel-2"
				className="new-panel-2"
				onDragOver={ ()=>{ this.refs.uploadPanel._onDragOver(); } }
			>
				<Upload
					ref="uploadPanel"
					bridge={this.props.bridge}
					userProfile={this.props.userProfile}
					history={this.props.history}
				/>
				<Header
					bridge={this.props.bridge}
					profile={ this.props.userProfile }
					history={this.props.history}
				/>

				<Messages
					messages={this.state.messages}
					bridge={ this.props.bridge }
					userProfile={this.props.userProfile}
					isBridgeMember={ this.props.isBridgeMember }
					openBModal={ this.props.openBModal }
					history={ this.props.history }
					onClickReply={ this.props.onClickReply }
					onClickDelete={ this._onClickDelete }
					selectProfile={ this.props.selectProfile }
				/>

				<Footer
					userProfile={this.props.userProfile}
					bridge={this.props.bridge}
					isBridgeMember={ this.props.isBridgeMember }
					dropzone = { ()=>{this.refs.uploadPanel.open();}}
				/>

			</div>


		);
	}

}
reactMixin(PanelTwo.prototype, Reflux.ListenerMixin);
