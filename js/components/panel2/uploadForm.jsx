import { Dialog,DialogTitle,DialogContent,DialogActions,Button ,Textfield,ProgressBar } from 'react-mdl';
import React from 'react';
import S3 from '../../lib/s3';

export default class UploadForm extends React.Component{
	constructor(props){

		super(props);

		this.state = {
			open: false,
			progress:0,
			file:props.file,
			title:'',
			isImage:false
		};

		this._upload = this._upload.bind(this);
	}

	_handleCloseDialog(){

		this.setState({open:false});
	}

	_handleOpenDialog(){

		this.setState({open:true});
	}

	componentWillReceiveProps( nextProps ){
		let images = "*.jpg,*.jpeg,*.png,*.gif";
		let isImage=false;
		if(nextProps.file){
			var extension = nextProps.file.name.split('.').pop().toLowerCase();
			if(images.indexOf(extension)>=0){
				isImage = true;
			}

			this.setState(
				{
					file:nextProps.file,
					title:nextProps.file.name,
					isImage:isImage,
					progress:0
				});
		}


	}

	_upload(){
		let _self = this;
		S3.getS3UrlUpload(
			this.state.file,
			this.state.isImage,
			this.props.username,
			( error, data ) => {
				const s3URL = data.s3url;
				const fileKey = data.key;
				S3.sendFile(
					this.state.file,
					s3URL,
					( loaded, total ) => {
						let progress = Math.round(loaded*100/total);
						_self.setState({progress:progress});
					},( error, data ) => {
						const fileMeta = {};
						fileMeta.url = s3URL.split('?')[0];
						fileMeta.key = fileKey;
						fileMeta.title = _self.state.title;
						fileMeta.name = _self.state.file.name;
						//TODO: We should not trust the file extension
						//http://stackoverflow.com/questions/18299806/how-to-check-file-mime-type-with-javascript-before-upload
						//fileMeta.type = _self.state.file.type;
						fileMeta.type = _self.state.file.name.split('.').pop().toLowerCase();
						_self.props.handleSaveMessage( fileMeta );

						return _self.setState({open:false});
					}
				);
			}
		);
	}

	render(){
		let open = this.state.open || false;
		return (
		<Dialog open={open} className="dialog-medium">
		 	<DialogTitle> Upload File </DialogTitle>
			<DialogContent style={{textAlign:'left'}}>
				<div className="mdl-grid">
				<div className="mdl-cell mdl-cell--12-col">
				{
					(this.state.isImage)?
					<img src={this.state.file.preview} height="100" style={{margin:'auto'}}/>
					:
					null
				}
				</div>
				<div className="mdl-cell mdl-cell--12-col">
  				<Textfield
  					onChange={(e) => {
							this.setState({title:e.target.value});
						}}
  					label="Title"
						value = {this.state.title}
  					floatingLabel
  				/>
					<span>
					The title will be used to find this file in the search.
					</span>
					{
						(this.state.progress>0)?
						<ProgressBar progress={this.state.progress} />
						:
						null
					}

			  </div>
			</div>
			</DialogContent>
			<DialogActions>
				<Button
					id="btnCopyUrl"
					type='button'
					onClick={this._upload}
					disabled={(this.state.progress>0)}
					>
					Upload
				</Button>
				<Button
					type='button'
					onClick={this._handleCloseDialog.bind(this)}
					disabled={(this.state.progress>0)}
				>
					Close
				</Button>
			</DialogActions>
		</Dialog>
		);
	}
}
