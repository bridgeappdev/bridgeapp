import React from 'react';
import Dropzone from 'react-dropzone';
import ReactDOM from 'react-dom';

import S3 from '../../lib/s3';
import FileMessage from '../../models/messages/FileMessage';

import UploadForm from './uploadForm';

export default class Upload extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			file:null
		};

		this._handleSaveMessage = this._handleSaveMessage.bind( this );
		this.open = this.open.bind(this);
	}

	_onDrop( files ){
		this.refs.uploadForm._handleOpenDialog();

		$(ReactDOM.findDOMNode(this.refs.dropzone)).css('z-index',0);
		this.setState( { file: files[0] } );
	}

	_onDragOver(){
		console.log("dentro...");
		$(ReactDOM.findDOMNode(this.refs.dropzone)).css('z-index',1000);
	}
	_onDragLeave (){
		console.log("fuera....");
		$(ReactDOM.findDOMNode(this.refs.dropzone)).css('z-index',0);
	}
	open() {
      this.refs.dropzone.open();
    }

	_handleSaveMessage( fileMeta ){

		const messageData = {
			'meta': fileMeta
		};
		const profile = this.props.userProfile;
		messageData.uId = profile.id;
		messageData.username = profile.username;

		if( profile.avatar ){
			messageData.avatar = profile.avatar;
		} else {
			messageData.avatar = "/assets/img/avatar/ph-avatar.png";
		}

		const message = new FileMessage( messageData );
		const _self = this;
		message.send( this.props.bridge.id, ( error ) => {
			if( error ){
				console.log('File message was not saved', error);
			} else {
				console.log('File message was sent');
				_self.props.bridge.updateLastMessageDate();
				_self.setState({ file: null });
			}
		});
	}

	render(){
		let style  = {
		      width:'100%',
		      border:'none',
		      flex:'2',
		      display: 'flex',
		      position:'absolute',
		      flexDirection:'column',
		      justifyContent: 'center',
		      alignItems: 'center',
		      background:'transparent ',
		      top:'0',
		      bottom:'0',
		      zIndex:0
		    };
		 let activeStyle = {
		    	backgroundColor:'#fff',
		      outline: 'black',
		      outlineStyle: 'dashed',
		      backgroundImage:'url(/assets/img/upload.png)',
		      backgroundRepeat:'no-repeat',
		      backgroundPosition:'center center',
		      zIndex:1000,
		      opacity:'0.9'
		 };

		return(
			<div>
				<Dropzone
					ref="dropzone"
					onDragLeave={this._onDragLeave.bind(this)}
					onDrop={this._onDrop.bind(this)}
					style={style}
					activeStyle={activeStyle}
					disableClick
				>
				</Dropzone>
				<UploadForm
					ref="uploadForm"
					file={ this.state.file }
					username={ this.props.userProfile.username }
					handleSaveMessage={ this._handleSaveMessage }
				/>
			</div>
		);
	}
}
