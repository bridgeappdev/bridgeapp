'use strict';

import React from 'react';
import ReactDOM from 'react-dom';

import ContainerDay from './messages/container-messages';
import MessageItem from './messages/messageItem';

import dateFormat from 'dateformat';
import moment     from 'moment';

export default class Messages extends React.Component{
	constructor(props){
		super(props);

		this.state = {
			loadingPrevious: false
		}

		this.noOfMessages = props.messages.length;
		this._scrollToDivId = false;

	}

	_infiniteScrollTop( el, bridgeContentContext ){

		if(
			!this.props.allMessagesLoaded
			&& !this.state.loadingPrevious
		){

			console.log(
				"BRIDGECONTENT::_infiniteScrollTop ==> "
					+ "Let´s get some more messages"
			);

			this._messagesContentJQueryElem = el.mcs.content;

			return this.setState(
				{'loadingPrevious': true},
				this._loadPrevMessages
			);
		} else {
			console.log(
				"BRIDGECONTENT::_infiniteScrollTop ==> "
					+ "Not loading more messages"
			);
		}
	}

	_updateScrollToDiv(){
		if(this._scrollToDivId.id){
			this._scrollToDivId.scrollNow = true;
		}
	}

	_loadPrevMessages(){
		const _self = this;

		return this.props.loadPrevMessages( ( err )=>{
			let errMessage = "";
			let openSnackBar = false;
			if( err ){
				//Error Message in snackbar
				errMessage = 'There was a problem loading previous messages.';
				openSnackBar = true;
			} else {

				//Record where the scroll needs to be.
				const oldestMessageId = _self.props.messages[0].id;
				_self._scrollToDivId = {
					'id': oldestMessageId,
					'pos': $(this._messagesContentJQueryElem).height()
				};
			}
			_self.setState(
				{
					'loadingPrevious':false,
					'loadMessagesError': errMessage,
					'openSnackBar': openSnackBar
				},
				_self._updateScrollToDiv
			);
		});
	}

	_setScrollFalse(){
		this._scrollToDivId = false
	}


	_infiniteScrollBottom( el, bridgeContentContext ){

		if( this._hasMoreRecentMessages ){
			console.log("BRIDGECONTENT::_infiniteScrollBottom ==> Let´s get some more messages");
		}

	}

	componentDidMount() {
		const _self = this;

		$(ReactDOM.findDOMNode(this.refs.messages)).mCustomScrollbar({
			autoHideScrollbar: this.props.isReplyThread,
			alwaysShowScrollbar: 1,
			setHeight: "100%",
			theme:"dark",
			scrollInertia:0,
			mouseWheel:{
				scrollAmount: 50
			},
			callbacks: {
				onTotalScrollBack: function(){
					_self._infiniteScrollTop(this, _self);
				},
				onTotalScroll: function(){
					_self._infiniteScrollBottom(this, _self);
				}
			}
		});
		window.setTimeout(function(){
			$(ReactDOM.findDOMNode(this.refs.messages))
				.mCustomScrollbar(
					"scrollTo","bottom",
					{ scrollInertia:0 }
				)
			}.bind( this ), 1000);
	}

	componentDidUpdate( prevProps, prevState ) {
		const scrollToDivId = this._scrollToDivId;

		if(scrollToDivId && scrollToDivId.pos && scrollToDivId.scrollNow){
			//const scrollTo = $('#mCSB_8_container').height() - (scrollToDivId.pos + 50);
			const scrollTo = scrollToDivId.pos;
			$(ReactDOM.findDOMNode( this.refs.messages))
			.mCustomScrollbar(
				"scrollTo",
				scrollTo,
				{scrollInertia:0,scrollEasing:"linear"}
			);
			// window.setTimeout(function(){
			// 	$(ReactDOM.findDOMNode(this.refs.messages))
			// 	.mCustomScrollbar(
			// 		"scrollTo",
			// 		scrollTo - 200,
			// 		{scrollInertia: 3000}
			// 	);
			// }.bind( this ), 1000);
			this._setScrollFalse();
		}else{
			const noOfMessagesNow = this.props.messages.length;
			if( noOfMessagesNow !== this.noOfMessages ){
				this.noOfMessages = noOfMessagesNow;

				$(ReactDOM.findDOMNode(this.refs.messages))
					.mCustomScrollbar(
						"scrollTo","bottom",
						{ scrollInertia:0 }
					);
			}
		}
	}

	render(){
		let lastMessageUserId;
		let currentDate,messageDate;
		let messages=[];
		let containerDays = [];
		let currUserId = this.props.userProfile.id;
		this.props.messages.forEach((message,index)=>{
			let isSameUser = (lastMessageUserId === message.uId);
			let isOwner = (currUserId === message.uId);
			lastMessageUserId = message.uId;
			messageDate = dateFormat(
				new Date(message.createdAt),
				'yyyy-mm-dd'
			);
			if(!currentDate){
				currentDate = messageDate;
			}

			if(currentDate!=messageDate){
				let day = moment(currentDate).calendar(null, {
								sameDay: '[Today]',
								nextDay: '[Tomorrow]',
								lastDay: '[Yesterday]',
								lastWeek: '[Last] dddd',
								sameElse: 'dddd Do MMMM'
							});
				let containerDay = <ContainerDay
					key={ day + '-' + index }
					day={day}
				>
					{messages}
				</ContainerDay>;

				currentDate= messageDate;
				messages =[];
				containerDays.push(containerDay);
				isSameUser = false;
			}
			messages.push(
				<MessageItem
					key={"message-"+index}
					message={message}
					isBridgeMember={ this.props.isBridgeMember }
					owner={isOwner}
					showheader={!isSameUser}
					onClickReply={ this.props.onClickReply }
					onClickDelete={ this.props.onClickDelete }
					selectProfile={ this.props.selectProfile }
				/>
			);
		});

		if(messages.length>0){
			let day = moment(currentDate).calendar(null, {
								sameDay: '[Today]',
								nextDay: '[Tomorrow]',
								lastDay: '[Yesterday]',
								lastWeek: '[Last] dddd',
								sameElse: 'dddd Do MMMM'
							});
			let containerDay =
				<ContainerDay
					key={day}
					day={day}
				>
					{messages}
				</ContainerDay>;
			containerDays.push(containerDay);
		}

		return (
			<div
				ref="messages"
				className="new-panel-2-messages mCustomScrollbar"
			>
				{
					this.props.loadingPrevious
					? <div>loading...</div>
					: null
				}

				<div>
					{ this.props.firstMessage }
					{containerDays}
				</div>

			</div>
		)
	}

}
