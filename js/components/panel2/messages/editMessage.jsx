/* components/common/Input.js
* React components for messages.
* Props:
* {
* }
*/
"use strict";

import React from 'react';
import reactMixin from 'react-mixin';

import ChatMessage from '../../../models/messages/ChatMessage';
import URLMessage from '../../../models/messages/URLMessage';

import FontIcon   from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

import { Textfield, Button } from 'react-mdl';

import EmojiPicker from '../../common/EmojiPicker';

export default class MessageInput extends React.Component{
	constructor( props ){

		super( props );

		this.state = {
			inputValue: props.message.meta.message,
			showEmojiPicker: true
		};

		this.toggleEmojiPicker = this.toggleEmojiPicker.bind(this);
		this._handleKeyDown = this._handleKeyDown.bind(this);
		this._handleSubmit = this._handleSubmit.bind(this);
		this._onClickCancel = this._onClickCancel.bind(this);
		this._handleTextChange = this._handleTextChange.bind(this);
		this.clearText = this.clearText.bind(this);
		this.addIcon = this.addIcon.bind(this);
		this.renderEmoji = this.renderEmoji.bind(this);

	}

	componentDidMount() {

		document.onkeydown = this._handleKeyDown;
		return this.toggleEmojiPicker();
	}


	toggleEmojiPicker() {
		const value = !this.state.showEmojiPicker;
		this.setState( { showEmojiPicker: value } );
	}

	_parseText( messageText ){
		const linkify = require('linkifyjs');

		const messageMetaObj = { 'message': messageText };
		const messageUrls = linkify.find( messageText, 'url' );
		if( messageUrls.length ){
			messageMetaObj.url = messageUrls[0].href;
		}

		return messageMetaObj;

	}

	_handleKeyDown( e ) {

		if( e.keyCode === 27 ){
			return this.props.close();
		};

		if ( e.which === 13 ) {
			e.preventDefault();
			return this._handleSubmit();
		}

	}

	_handleSubmit(){

		const messageText = this.state.inputValue;
		if( messageText === "" ) {
			return console.log( "Do delete: ", this.props.message );;
		}

		const editedMessage = this.props.message;
		editedMessage.meta = this._parseText( messageText )

		switch (true) {
			case !!editedMessage.meta.url :
			editedMessage.type = 'URL';
			break;
			default:
			editedMessage.type = 'CHAT';
		}

		return editedMessage.update( ( err ) => {
			if( err ){
				//report to user
			}
			this.props.close();
		} );

	}

	_handleTextChange(e){

		return this.setState(
			{ inputValue: e.target.value }
		);

	}

	_onClickCancel(){
		return this.props.close();
	}

	clearText(){

		return this.setState(
			{ inputValue: '' }
		);

	}

	addIcon( icon ){
		this.setState({
			inputValue : this.state.inputValue + icon
		});
		this.message_input.focus();
		this.toggleEmojiPicker();
	}

	renderEmoji(){
		if(this.state.showEmojiPicker){
			return (
				<EmojiPicker
					open={this.state.showEmojiPicker}
					addIcon={this.addIcon}>
				</EmojiPicker>
			);
		}
	}

	render() {

		let iconEmoji = 'fa fa-smile-o';
		if(this.state.showEmojiPicker){
			iconEmoji = 'fa fa-chevron-down';
		}
		return (
			<div className='panel-2-edit-message'>
				<div>
					{this.renderEmoji()}
					<div className="new-panel-2-input-message">
						<div className="input">

							<input
								type="text"
								ref={(ref) => this.message_input = ref}
								onKeyDown={this._handleKeyDown}
								onChange={this._handleTextChange}
								placeholder="Say something"
								style={{width: '100%'}}
								value={ this.state.inputValue }
								/>
						</div>
						<IconButton
							className="emoticon"
							onClick={this.toggleEmojiPicker}
							>
							<FontIcon className={iconEmoji} />
						</IconButton>
					</div>

				</div>
				<div className='panel-2-edit-message-buttons'>
					<Button
						type='button'
						className='cancel'
						raised
						onClick={ this._onClickCancel }
					>
						Cancel
					</ Button>
					<Button
						type='button'
						className='save'
						raised
						accent
						onClick={ this._handleSubmit }
					>
						Save
					</ Button>
				</div>
			</div>
		);

	}

}
