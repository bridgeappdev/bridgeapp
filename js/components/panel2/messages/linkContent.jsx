import React from 'react';
export default class LinkContent extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			visible:true
		};
	}
	_toggleLink(e){
		e.preventDefault();
		this.setState({visible:!this.state.visible})
	}
	render(){
		return (
		<div className="new-panel-2-link-content">
			<a href={this.props.data.url||this.props.data.thumbnail_url} onClick={this._toggleLink.bind(this)}>{this.props.data.title}</a>
			{
				(this.state.visible)?
				<div className="content">
					<img src={this.props.data.thumbnail_url||this.props.data.url } width="100%" style={{maxWidth:'250px'}}/>
		       		<div className="description">{this.props.data.description}</div>
				</div>
				:null
			}
		       
	      </div>
		);	
	}

}