import React from 'react';
export default class EmbedlyComponent extends React.Component{
  constructor(props){
    super(props);
  }
  componentWillMount() {
    $.getJSON( 'http://api.embed.ly/1/oembed?' + $.param({
      url: this.props.url, 
      key:'dac524884eb249f7b958f553c8c12395',
      maxheight:300
    }),
      function( data ) {

        if (data) this.setState(data);
    }.bind(this));
  }

  render(){

    return !this.state ?
      <a href={this.props.url} target="_blank">{this.props.url}</a> :
      <div className="new-panel-2-embedly">
        <a href={this.props.url} target="_blank">{this.props.url}</a>
          <br/>
        {this.state.html && <div dangerouslySetInnerHTML={{__html: this.state.html}}/>}
        {(this.state.type == 'photo' || this.state.thumbnail_url) && !this.state.html && <img src={this.state.thumbnail_url || this.state.url} width="100%"/>}
        {(this.state.type == 'video') ?'':this.state.description}
      </div>;
  }
}