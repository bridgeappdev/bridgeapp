'use strict';

import React from 'react';

export default function replyCount( props ){
	return (
		<div
			className='message-reply-count'
			onClick={ props.handleClickReplyCount }
		>
			{props.count} replies
		</div>
	);
}
