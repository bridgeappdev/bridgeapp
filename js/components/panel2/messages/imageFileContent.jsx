'use strict';

import React from 'react';

export default (props) => {

	return (
		<div className="image-file-content">

			<div className="image-file-image">
				<img src={props.data.thumbnail_url || props.data.url} width="100%"/>
			</div>

			<div className="image-file-description">
				{props.data.title}
			</div>

		</div>
	);

}
