import moment from 'moment';
import React from 'react';

export default (props)=>{
	let dateName = moment(props.date)
	.calendar(null, {
		nextDay: '[Tomorrow]',
		lastDay: '[Yesterday]',
		lastWeek: '[Last] dddd',
		sameElse: 'dddd Do MMMM'
	});
	return (
		<div  className="new-panel-2-container">
			<h2>
			<span>{props.day}</span>
			</h2>
			<div>{props.children}</div>
		</div>
	);
}