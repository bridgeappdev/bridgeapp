'use strict';

import React from 'react';

import s3 from '../../../lib/s3';

import Message from './message';
import EditMessage from './editMessage';
import DeleteMessageDialog from './DeleteMessageDialog';

import history from '../../../HistoryContainer';

import EmbedlyContent from './embedlyContent';
import FileContent from './fileContent';
import ReplyCount from './replyCount';

import dateFormat from 'dateformat';

export default class MessageItem extends React.Component{
	constructor( props ){

		super( props );

		this.state = {
			edit:false,
			menuValign: null
		}

		this._reply = this._reply.bind( this );
		this._download = this._download.bind( this );
		this._edit = this._edit.bind( this );
		this._delete = this._delete.bind( this );
		this._closeEdit = this._closeEdit.bind( this );
		this._closeDelete = this._closeDelete.bind( this );
		this._selectProfile = this._selectProfile.bind( this );
		this.onClickMenu = this.onClickMenu.bind( this );

	}

	_getOptions(){
		let options = [];
		if(
			!this.props.message.isReply
			&& !this.props.isBridgeMember
		){
			return options;
		}

		if( !this.props.message.isReply ){
			options.push({
				label: 'Reply',
				icon: '',
				handleEvent: this._reply
			});
		}

		if(this.props.owner){
			if( this.props.message.type !== 'FILE' ){
				options.push({
					label: 'Edit',
					icon: '',
					handleEvent: this._edit
				});
			}
			options.push({
				label: 'Delete',
				icon: '',
				handleEvent: this._delete
			});
		}

		if( this.props.message.type === 'FILE' ){
			options.push({
				label: 'Download',
				icon: '',
				handleEvent: this._download
			});
		}

		return options;

	}

	_reply(){
		return this.props.onClickReply(
			this.props.message
		);
	}

	_download(){
		const fileKey = this.props.message.meta.key;
		const fileName = fileKey.substring(
			fileKey.lastIndexOf('/') + 1,
			fileKey.lastIndexOf('_')
		)	+ fileKey.substring( fileKey.lastIndexOf('.') );
		s3.getS3UrlDownload(
			fileKey,
			( err, url )=>{
				if(err){
				} else {
					const dl = document.createElement('a');
					dl.setAttribute( 'href', url );
					dl.setAttribute( 'download', fileName );
					dl.click();
				}
			}
		);
	}

	_edit(){
		return this.setState({
			edit: true
		});
	}

	_delete(){
		return this.setState({
			delete: true
		});
	}

	_getContent(type){
		let content;
		switch(type){
			case 'URL':
				content  = <EmbedlyContent data = {this.props.message.meta}/>;
				break;
			case 'FILE':
				content  = <FileContent data={ this.props.message.meta }/>;
				break;
			default:
				content = (
					<div
						dangerouslySetInnerHTML={
							{ __html: this.props.message.meta.messageHTML }
						}
					/>
				);
		}

		return content;

	}

	_closeEdit(){

		return this.setState(
			{ edit: false }
		);

	}

	_closeDelete( ){
		this.setState({
			delete: false
		});
	}

	_selectProfile(){
		const uId = this.props.owner
			? null
			: this.props.message.uId;

		return this.props.selectProfile( uId );
	}

	onClickMenu( e ){
		const messageViewPortLocation =
			e.target.getBoundingClientRect();
		const displayHeightMiddle = document.documentElement.clientHeight / 2;

		if( messageViewPortLocation.top > displayHeightMiddle ){
			this.setState( { 'menuValign': 'top' } );
		}else{
			this.setState( { 'menuValign': null } );
		}
	}

	render(){
		const time = dateFormat(
			new Date(this.props.message.createdAt),
			'hh:MM'
		);

		const deleteDialog = this.state.delete
			?
			(<DeleteMessageDialog
				closeDeleteDialog={ this._closeDelete }
				message={ this.props.message }
			/>)
			: null;

		return (
					(!this.state.edit)?
					<Message
						id={ this.props.message.id }
						avatar={ this.props.message.avatar }
						user={ this.props.message.username }
						time={ time }
						options={ this._getOptions() }
						owner={ this.props.owner }
						showheader={ this.props.showheader }
						selectProfile={ this._selectProfile }
						valign={ this.state.menuValign }
						onClickMenu={ this.onClickMenu }
					>

					{deleteDialog}
					{this._getContent(this.props.message.type)}
					{
						(this.props.message.replyMeta
							&& this.props.message.replyMeta.count > 0
						) ? <ReplyCount
									handleClickReplyCount={ this._reply }
									count={ this.props.message.replyMeta.count }
								/>
							:
							null
					}
					</Message>
					: <EditMessage
						message={ this.props.message }
						close={ this._closeEdit }
					/>
			);
	}

}
