import React from 'react';
import VideoContent from './videoContent';
import LinkContent from './linkContent';
import ProviderContent from './providerContent';

const PROVIDER = {
	GOOGLE_DRIVE_DOC:'https://docs.google.com',
	GOOGLE_DRIVE:'https://drive.google.com',
	DROPBOX:'https://www.dropbox.com',
	GITHUB:'https://github.com'
}
const TYPE = {
	LINK:'link',
	VIDEO:'video',
	PHOTO:'photo',
	RICH:'rich'
}
export default class EmbedlyContent extends React.Component{
	constructor(props){
		super(props);
	}
	_renderByProvider(provider){
		let content;
		switch(provider){
			case PROVIDER.GOOGLE_DRIVE_DOC:
			case PROVIDER.GOOGLE_DRIVE:
			case PROVIDER.DROPBOX:
			case PROVIDER.GITHUB:
				content = <ProviderContent data={this.props.data.urlData}/>;
				break;
			default:
				content = <LinkContent data={this.props.data.urlData}/>;
		}
		return content;
	}
	_renderByType(type){
		let content;
		switch(type){
			case TYPE.VIDEO:
				content =  <VideoContent data = {this.props.data.urlData}/>;
				break;
			case TYPE.RICH:
			case TYPE.LINK:
				content =  this._renderByProvider(this.props.data.urlData.provider_url);
				break;
			case TYPE.PHOTO:
				content = <LinkContent data={this.props.data.urlData}/>;
				break;
		}
		return content;
	}
	render(){
		return(
			<div>
				<div dangerouslySetInnerHTML={{__html:this.props.data.messageHTML}}></div>
				{(this.props.data.urlData)?this._renderByType(this.props.data.urlData.type):''}
			</div>
		);
	}
}