import React from 'react';
const PROVIDER = {
	GOOGLE_DRIVE:{
		provider:'https://drive.google.com',
		image:'/assets/img/googledrive.png'
	},
	GOOGLE_DRIVE_DOC:{
		provider:'https://drive.google.com',
		image:'/assets/img/googledrive.png'
	},
	DROPBOX:{
		provider:'https://www.dropbox.com',
		image:'/assets/img/dropbox.png'
	},
	GITHUB:{
		provider:'https://github.com',
		image:'/assets/img/github.png'
	}
}
let getImageProvider = (provider)=>{
	let image;
	switch(provider){
		case PROVIDER.GOOGLE_DRIVE.provider:
		case PROVIDER.GOOGLE_DRIVE_DOC.provider:
			image = PROVIDER.GOOGLE_DRIVE.image;
			break;
		case PROVIDER.DROPBOX.provider:
			image = PROVIDER.DROPBOX.image;
			break;
		case PROVIDER.GITHUB.provider:
			image = PROVIDER.GITHUB.image;
			break;
	}
	return image;

}
export default (props)=>{
	return (
		<div className="new-panel-2-provider-content">

			<div className="image">
				<img src={ getImageProvider(props.data.provider_url)} width="32px"/>
			</div>
			 <div className="description">       
			 	<strong>{props.data.title}</strong><br/>
			        {props.data.description}
			 </div>
	      	</div>
	);

}