'use strict';

import React from 'react';

import BridgeSettings from '../../../bridge/Settings';
import BuildBridgePanel from '../../../panel1/BuildBridge/buildBridgePanel';


export default class FirstMessageHome extends React.Component{
	constructor( props ){
		super( props );
	}

	_handleClickSearch( e ){

		e.preventDefault();
		let queryString;
		const tags = this.props.profile.tags;
		if( tags && tags.length ){
			queryString = '?' + tags.join(',');
		}
		return this.props.history.pushState( {}, '/bsearch' + queryString );
	}

	_handleClickBuild( e ){
		e.preventDefault();

		return this.refs.buildBridgePanel._open();

	}

	render(){
		const bridge = this.props.bridge;
		let bridgeOwnerDiv = null;

		if( this.props.owner ){
			bridgeOwnerDiv = (
				<div className='first-message-owner'>
					<a
						className='first-message-edit-bridge'
						href='#'
						onClick={ this._handleClickSearch.bind( this ) }
					>
						<i className='material-icons'>search</i>
						<span>Search for a bridge</span>
					</a>
					<a
						className='first-message-invite-bridge'
						href='#'
						onClick={ this._handleClickBuild.bind( this ) }
					>
						<i className='material-icons'>add</i>
						<span>Build a bridge</span>
					</a>
				</div>
			);
		}

		return (
			<div className='first-message'>

				<div className='first-message-title'>
					{ bridge.name }
				</div>

				<div className='first-message-text'>
					This is your home bridge. Description: <span className='description'> { bridge.desc } </span>
			</div>
			<br/>

			<div className='first-message-call-to-action'>
				Here you will see your personlised feed (COMING SOON!) and be able to keep files and link for later.
			</div>

				{ bridgeOwnerDiv }
				<BuildBridgePanel ref="buildBridgePanel" profile = {this.props.profile}/>
			</div>
		)
	}
}
