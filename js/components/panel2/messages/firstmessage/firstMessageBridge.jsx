'use strict';

import React from 'react';

import BridgeSettings from '../../../bridge/Settings';

export default class FirstMessageBridge extends React.Component{
	constructor( props ){
		super( props );
	}

	_handleClickEdit( e ){

		e.preventDefault();
		return this.props.openBModal(
			<BridgeSettings
				initialData={ this.props.bridge }
				profile={ this.props.currUser }
			/>
		);
	}

	_handleClickInvite( e ){
		e.preventDefault();

		let parts = location.pathname.split('/');
		parts = parts.slice(0, 4);
		const invitePath = parts.join('/') + '/invite';
		this.props.history.pushState({}, invitePath);
	}

	render(){
		const bridge = this.props.bridge;
		let bridgeOwnerDiv = null;

		if( this.props.owner ){
			bridgeOwnerDiv = (
				<div className='first-message-owner'>
					<a
						className='first-message-edit-bridge'
						href='#'
						onClick={ this._handleClickEdit.bind( this ) }
					>
						<i className='material-icons'>edit</i>
						<span>Edit this bridge</span>
					</a>
					<a
						className='first-message-invite-bridge'
						href='#'
						onClick={ this._handleClickInvite.bind( this ) }
					>
						<i className='material-icons'>people</i>
						<span>Invite people to this bridge</span>
					</a>
				</div>
			);
		}

		return (
			<div className='first-message'>

				<div className='first-message-title'>
					{ bridge.name }
				</div>

				<div className='first-message-text'>
					This is where it all begins. Description: <span className='description'> { bridge.desc } </span>
			</div>
			<br/>

			<div className='first-message-call-to-action'>
				Let´s get started! Post messages, links and files you want to share, reply to
				or follow posts you enjoy and make connections.
			</div>

				{ bridgeOwnerDiv }
			</div>
		)
	}
}
