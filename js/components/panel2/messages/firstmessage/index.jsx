'use strict';

import React from 'react';
import FirstMessageHome from './firstMessageHome';
import FirstMessageBridge from './firstMessageBridge';

export default ( props ) => {

	switch ( true ) {
		case ( props.bridge.isHome ) :
			return <FirstMessageHome
				bridge={ props.bridge }
				profile={ props.profile }
				openBModal={ props.openBModal }
				history={ props.history }
				owner={ props.owner }
			/>
			break;
		default:
		return <FirstMessageBridge
			bridge={ props.bridge }
			profile={ props.profile }
			openBModal={ props.openBModal }
			history={ props.history }
			owner={ props.owner }
		/>

	}
};
