import React from 'react';

import {
	Dialog,
	DialogTitle,
	DialogContent,
	DialogActions,
	Button
} from 'react-mdl';

export default class DeleteMessageDialog extends React.Component {
	constructor(props) {
		super(props);

		this._handleClose = this._handleClose.bind( this );
		this._handleClickDelete = this._handleClickDelete.bind( this );
	}

	_handleClose = () => {
		this.props.closeDeleteDialog();
	};

	_handleClickDelete = () => {
		const message = this.props.message;
		message.remove( (err) => {
			if( err ){
				console.log('Synchronization failed. ', err);
			} else {
				console.log('Synchronization succeeded');
			}

			return this._handleClose();
		});
	};

	render() {

		const open = !!this.props.message;
		const message = this.props.message;

		return (
			<div>
				<Dialog
					className="dialog-medium"
					modal={ false }
					open={ open }
				>
					<DialogTitle> Delete Message? </DialogTitle>

					<DialogContent style={{textAlign:'left'}}>
						<div className="mdl-grid">
							<div className="mdl-cell mdl-cell--12-col">
								{ message ? message.meta.message : message }
							</div>
						</div>
					</DialogContent>
					<DialogActions>
						<Button
							type='button'
							onClick={ this._handleClose }
						>
							Cancel
						</Button>
						<Button
							type='button'
							onClick={ this._handleClickDelete }
						>
							Delete
						</Button>
					</DialogActions>
				</Dialog>
			</div>
		);
	}
}
