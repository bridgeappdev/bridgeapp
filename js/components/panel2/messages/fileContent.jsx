import React from 'react';

import ImageFile from './imageFileContent';

const FILE_TYPE = {
	POWERPOINT:{
		type:'',
		icon:'/assets/img/fileicons/PPT.png'
	},
	WORD:{
		type:'',
		icon:'/assets/img/fileicons/WORD.png'
	},
	PDF:{
		type:'application/pdf',
		icon:'/assets/img/fileicons/PDF.png'
	},
	EXCEL:{
		type:'',
		icon:'/assets/img/fileicons/excel.png'
	},
	ZIP:{
		type:'',
		icon:'/assets/img/fileicons/zip.png'
	},
	DEFAULT:{
		icon:'/assets/img/fileicons/default.png'
	}
}

let getIconByType = ( type ) => {
	let icon;

	switch( type ){
		case FILE_TYPE.WORD.type:
			icon = FILE_TYPE.WORD.icon;
			break;
		case FILE_TYPE.EXCEL.type:
			icon = FILE_TYPE.EXCEL.icon;
			break;
		case FILE_TYPE.POWERPOINT.type:
			icon = FILE_TYPE.POWERPOINT.icon;
			break;
		case FILE_TYPE.ZIP.type:
			icon = FILE_TYPE.ZIP.icon;
			break;
		default:
			icon = FILE_TYPE.DEFAULT.icon;

	}

	return icon;

}
export default ( props ) => {

	if( "*.jpg,*.jpeg,*.png,*.gif".indexOf( props.data.type )>=0 ){
		return <ImageFile data={ props.data } />
	}else{

		return (
			<div className="new-panel-2-file-content">

				<div className="file-message-icon">
					<img src={ getIconByType(props.data.type)} width="32px"/>
				</div>
				<div className="file-message-description">
					<strong>{props.data.type}</strong><br/>
					{props.data.title}
				</div>
			</div>
		);

	}

}
