import React from 'react';
import Avatar from 'material-ui/Avatar';
import { IconButton, Menu, MenuItem } from 'react-mdl';

export default ( props )=>{
	const showheader = (
		props.hasOwnProperty('showheader'))
		? props.showheader
		:true;
	const id = 'message-'+props.id;
	const options = props.options.map((item,index)=>{
		return (
			<MenuItem
				key={'option-'+index}
				onClick={item.handleEvent.bind(props,item)}
			>
				{item.label}
			</MenuItem>);
	});
	const menuProps={
		target:id,
		align:'right'
	};
	if( props.valign ){
		menuProps.valign = 'top';
	}
	return (
		<div className="new-panel-2-message">
			<div
				className="new-panel-2-message-content-avatar"
			>
				{ (showheader)?
					<div
						className='message-content-avatar-onclick'
						onClick={ props.selectProfile }
					>
						<Avatar
							src={props.avatar}
							size={30}
							/>
					</div>
				:
					<span
						className="new-panel-2-message-content-date"
					>
						{props.time}
					</span>
				}
			</div>
			<div className="new-panel-2-message-content-message">
				{ (showheader)?
					<div className="new-panel-2-message-content-title">
						{props.user}
						<span
							className="new-panel-2-message-content-date"
						>
							{props.time}
						</span>
					</div>
					:
					null
				}
				<div className="new-panel-2-message-content-content">
				{props.children}
				</div>
			</div>
			<div className="new-panel-2-message-options">
				{options.length?
					<div>
						<IconButton name="more_vert" id={id} />
						<Menu
							{ ...menuProps }
						>
							{options}
						</Menu>
					</div>
					: null
				}
			</div>
		</div>
	);
}
