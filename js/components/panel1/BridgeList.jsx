/* BridgeList.js
* React components for bridge lists.
*/
'use strict';

const React = require('react'),
Reflux = require('reflux'),
bridgesUtil = require('../../lib/common/bridgesUtil');

import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';

import { List, ListItem, MakeSelectable } from 'material-ui/List';
import HistoryContainer from '../../HistoryContainer';
let SelectableList = MakeSelectable(List);
var BridgeList = {};

module.exports = React.createClass({

	displayName: "BridgeList",

	LIST_LENGTH: 2,

	extractAdditionalPath: function(path){

		var parts = location.pathname.split('/');
		parts = parts.slice(4, parts.length);
		return parts.join('/');
	},

	getInitialState:function(){
		return {
			selectedIndex:0
		};
	},

	selectBridge: function( route ){

		return HistoryContainer.pushState({},route);

	},

	onClickSeeMore( type ){

		switch (type) {
			case 'starred':
				this.setState({
					showAllStarredBridges: true
				});

				break;
				case 'event':
				this.setState({
					showAllEventBridges: true
				});

				break;
			case 'chat':
			this.setState({
				showAllChatBridges: true
			});

				break;
			default:

		}
	},
	onClickHide( type ){

		switch (type) {
			case 'starred':
				this.setState({
					showAllStarredBridges: false
				});

				break;
			case 'event':
				this.setState({
					showAllEventBridges: false
				});

				break;
			case 'chat':
			this.setState({
				showAllChatBridges: false
			});

				break;
			default:

		}
	},

	render: function (){

		const userBridges = this.props.userProfile.bridges;
		const userBridgesKeysSorted =
			Object.keys( userBridges ).sort(
				function( a, b ){
					return userBridges[b].visitCount - userBridges[a].visitCount
				}
			);
		const bridgeData = this.props.bridges,
		starredbList = [], chatbList = [], eventbList = [],
		_self = this;
		let currentPathEnd = '/' + this.extractAdditionalPath(location.pathname);

		// if (bridgeData[currentPathEnd] !== undefined) {
		// 	currentPathEnd = "";
		// }

		userBridgesKeysSorted.forEach( function( key ){

			if(bridgeData.hasOwnProperty( key )){
				let nextBridge = bridgeData[key];
				if( nextBridge.isMember( _self.props.userProfile.id ) ){
					const bridgeId = nextBridge.id;
					const bridgePath = nextBridge.path;
					const bridgeTitle = nextBridge.title;
					const isFavorite = userBridges[ key ].isFavorite;
					const route = "/bridge/" + bridgePath + currentPathEnd;

					let newIcon = false;
					if(
						_self.props.currentBridgeId !== nextBridge.id &&
						nextBridge.messagesMeta.lastMessageDate >
						userBridges[nextBridge.id].lastVisit
					){
						newIcon = true;
					};

					switch (true){
						case( isFavorite ):
						if(
							!_self.state.showAllStarredBridges &&
							starredbList.length > _self.LIST_LENGTH
						){
							break;
						}
						if(
							!_self.state.showAllStarredBridges &&
							starredbList.length === _self.LIST_LENGTH
						){
							starredbList.push(
								<li key='showall'>
									<div
										className='bridge-list-see-more'
										onClick={_self.onClickSeeMore.bind(_self, 'starred')}
									>
										see more ...
									</div>
								</li>
							);
							break;
						}
						starredbList.push(
							<li
								key={ bridgeId }
								onClick={ _self.selectBridge.bind( _self, route ) }
							>
								<a href="#">
									<div className="icon">
										<span
											className="material-icons"
											title='This bridge is public'
										>
											public
										</span>

									</div>
									<div className="title"> {bridgeTitle} </div>
									{
										(newIcon)?
											<div className="icon"><span className="badge">New</span></div>
										:
										null
									}
								</a>
							</li>
						);
						break;

						case (nextBridge.type === 'event'):
						case (nextBridge.type === 'EVENT'):
						if(
							!_self.state.showAllEventBridges &&
							eventbList.length > _self.LIST_LENGTH
						){
							break;
						}
						if(
							!_self.state.showAllEventBridges &&
							eventbList.length === _self.LIST_LENGTH
						){
							eventbList.push(
								<li key='showall'>
									<div
										className='bridge-list-see-more'
										onClick={_self.onClickSeeMore.bind(_self, 'event')}
									>
										see more ...
									</div>
								</li>
							);
							break;
						}
						eventbList.push(
							<li
								key={bridgeId}
								onClick={_self.selectBridge.bind(_self, route)}
							>
								<a href="#">
									<div className="icon">
										<span
											className="material-icons"
											title='This bridge is public'
										>
											public
										</span>

									</div>
									<div className="title"> {bridgeTitle} </div>
									{
										(newIcon)?
											<div className="icon"><span className="badge">New</span></div>
										:
										null
									}
								</a>
							</li>
						);
						break;

						case (nextBridge.visibility === 'public'):
						if(
							!_self.state.showAllChatBridges &&
							chatbList.length > _self.LIST_LENGTH
						){
							break;
						}
						if(
							!_self.state.showAllChatBridges &&
							chatbList.length === _self.LIST_LENGTH
						){
							chatbList.push(
								<li>
									<div
										className='bridge-list-see-more'
										onClick={_self.onClickSeeMore.bind(_self, 'chat')}
									>
										see more ...
									</div>
								</li>
							);
							break;
						}

						chatbList.push(
							<li key={bridgeId}  onClick={_self.selectBridge.bind(_self, route)}>
								<a href="#">
									<div className="icon">
										<span
											className="material-icons"
											title='This bridge is public'
										>
											public
										</span>

									</div>
									<div className="title"> {bridgeTitle} </div>
									{
										(newIcon)?
											<div className="icon"><span className="badge">New</span></div>
										:
										null
									}
								</a>
							</li>
						);
						break;
					}
				}
			}
		});

		return(
			<div className="bridge-groups">
				<div className="bridge-group">
					<div className="bridge-group-header">
						STARRED BRIDGES
						{ this.state.showAllStarredBridges
							? <i
									className="material-icons  bridge-group-hide"
									title="hide bridges"
									onClick={ this.onClickHide.bind( this, 'starred' ) }
								>
									visibility_off
								</i>
						: null
						}
					</div>
					<div className="bridge-group-content">
						<ul>
							{ starredbList }
						</ul>
					</div>
				</div>
				<div className="bridge-group">
					<div className="bridge-group-header">
						EVENT BRIDGES
						{ this.state.showAllEventBridges
							? <i
									className="material-icons  bridge-group-hide"
									title="hide bridges"
									onClick={ this.onClickHide.bind( this, 'event' ) }
								>
									visibility_off
								</i>
						: null
						}
					</div>
					<div className="bridge-group-content">
						<ul>
							{ eventbList }
						</ul>
					</div>
				</div>
				<div className="bridge-group">
					<div className="bridge-group-header">
						CHAT BRIDGES
						{ this.state.showAllChatBridges
							? <i
									className="material-icons bridge-group-hide"
									title="hide bridges"
									onClick={ this.onClickHide.bind( this, 'chat' ) }
								>
									visibility_off
								</i>
						: null
						}
					</div>
					<div className="bridge-group-content">
						<ul>
							{ chatbList }
						</ul>
					</div>
				</div>
			</div>
		);
	}
});
