'use strict';

import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import Actions from '../../../../actions/NotificationActions';

import { Badge, Icon } from 'react-mdl';

import notificationStore from '../../../../stores/NotificationStore';
import notificationsAPI from '../../../../models/notifications/Notifications';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
export default class Notifications extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			notifications : []
		};
		this.open = false;
	}

	componentWillMount(){
		this.unsubscribe = notificationStore.listen( () => {
			this.setState({
				notifications: notificationStore.notifications
			});
		});
	}

	componentDidMount(){
		notificationsAPI.addListener(
			this.props.profile.id
		);
	}

	componentWillUnmount() {
		this.unsubscribe();
		notificationsAPI.removeNotificationListener(
			this.props.profile.id
		)
	}

	_renderNotifications(){
		const nList = this.state.notifications;
		const renderedList = [];
		const empty = (
			<MenuItem>
				"Nobody Loves You!"
			</MenuItem>
		);

		/*nList.forEach( ( notification ) => {
			renderedList.push(notification.render());
		} );*/
		nList.forEach(function( notification ) {
			if(notification){
				renderedList.push(notification.render());
			}
		});

		return renderedList.length?renderedList:empty;

	}

	_handleClickNotificationButton( e ){
		this.open = !this.open;
		if(this.open){
			return void 0;
		}

		const nList = this.state.notifications;
		nList.forEach( ( notification ) => {
			//notification.setSeen();
		} );
		this.forceUpdate();
	}

	_getBadgeText(){

		let noOfUnseen = null;
		const nList = this.state.notifications;

		nList.forEach( ( notification ) => {
			if(notification && !notification.seen){
				noOfUnseen++;
			}
		} );

		return noOfUnseen;
	}

	render() {
		let iconNotification = <IconButton onClick={this._handleClickNotificationButton.bind(this)}><span className="material-icons mdl-color-text--white">notifications_none</span></IconButton> ;
		let totalNotifications = this._getBadgeText() ;

		if(totalNotifications>0){
			iconNotification = (<IconButton onClick={this._handleClickNotificationButton.bind(this)}>
											<Badge text={totalNotifications} overlap>
							    				<Icon name="notifications" className="mdl-color-text--white"/>
											</Badge>
										</IconButton>);
		}else{

		}

		return (
			<IconMenu

				  iconButtonElement={iconNotification }
				  anchorOrigin={{horizontal: 'right', vertical: 'top'}}
				  targetOrigin={{horizontal: 'right', vertical: 'top'}}
				>
				 {this._renderNotifications()}
			</IconMenu>
		);
	}

}
reactMixin(Notifications.prototype, Reflux.ListenerMixin);
