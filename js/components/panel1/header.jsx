import React from 'react';
import UserInfo from './UserInfo';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import IconButton from 'material-ui/IconButton/IconButton';
import Avatar from 'material-ui/Avatar';

import EventBriteList from '../events/EventBriteList';
import MeetupList     from '../events/MeetupList';
import Profile        from '../profile/Settings';

import Auth from '../../models/auth/Auth';

export default class Header extends React.Component{
	constructor(){
		super();

		this._openSearch = this._openSearch.bind( this );
	}

	handleMenuSelect( value ){

		switch(value){
			case 'signout' :
			this._handleClickSignOut();
			break;
			case 'events' :
			this.refs.eventBridgeList._open();
			break;
			case 'meetups' :
			this.props.openBModal(<MeetupList />);
			break;
			case 'explore' :
			this._openSearch();
			break;
			case 'profile' :
			this.props.openBModal(
				<Profile
					initialProfile={this.props.userProfile}
					/>
			);
			break;
		}
	}

	_openSearch(){
		let queryString;
		const tags = this.props.userProfile.tags;
		if( tags && tags.length ){
			queryString = '?' + tags.join(',');
		}
		return this.props.history.pushState( {}, '/bsearch' + queryString );
	}

	_handleClickSignOut() {

		Auth.logout();
	}

	render(){
		return(
			<div className="panel-1-header">
				<EventBriteList ref="eventBridgeList" userProfile={this.props.userProfile}/>
				<div className="logo">
					<div className="options">
						<IconMenu
							iconButtonElement={
								<IconButton>
									<span
										className="material-icons mdl-color-text--teal-200"
									>
										more_vert
									</span>
								</IconButton>
							}
							anchorOrigin={{horizontal: 'left', vertical: 'top'}}
							targetOrigin={{horizontal: 'right', vertical: 'top'}}
							>
							<MenuItem
								primaryText={this.props.userProfile.name}
								leftIcon={
									<Avatar
										src={this.props.userProfile.avatar}
										/>
								}
								/>
							<MenuItem
								primaryText="Edit Profile"
								leftIcon={<i className="material-icons">account_box</i>}
								onClick={this.handleMenuSelect.bind(this, 'profile')}
							/>
							<MenuItem
								primaryText="Import Events"
								leftIcon={<i className="material-icons">event</i>}
								onClick={this.handleMenuSelect.bind(this, 'events')}
							/>
							<MenuItem
								primaryText="Import Meetups"
								leftIcon={<i className="material-icons">event</i>}
								onClick={this.handleMenuSelect.bind(this, 'meetups')}
							/>
							<MenuItem
								primaryText="Explore Bridges"
								leftIcon={<i className="material-icons">search</i>}
								onClick={this.handleMenuSelect.bind(this, 'explore')}
							/>
							<MenuItem
								primaryText="Sign out"
								leftIcon={<i className="material-icons">settings_power</i>}
								onClick={this.handleMenuSelect.bind(this, 'signout')}
							/>
						</IconMenu>
					</div>
				</div>
				<UserInfo userProfile={ this.props.userProfile }/>
			</div>
		);
	}
}
