/* ContactList.js
 * React components for bridge lists.
 */

/*jshint node: true*/

var React = require('react');
import {List, ListItem} from 'material-ui/List';

var ContactList = {};

ContactList.Cnt = React.createClass ({
	render: function(){
		"use strict";

		return (
			<ListItem>
				<span className="fa fa-users"></span>	Your Contacts
			</ListItem>
		);
	}
});

module.exports = ContactList;
