'use strict';

/* components/panel1/Panel1.jsx
* React components declaration for the first UI panel
* */

import React from 'react';
import Reflux from 'reflux';

import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';
import MenuItem from 'material-ui/MenuItem';
import NotificationsIcon from 'material-ui/svg-icons/social/notifications';

import bridgeStore from '../../stores/BridgeStore';

import Header from './header';
import Notifications from './Header/Notifications/Notifications';
import BuildBridge from './BuildBridge';
import BridgeList from './BridgeList';
import ContactList from './ContactList';

const PanelOne = React.createClass ({

	displayName: "PanelOne",

	mixins: [Reflux.ListenerMixin],

	getInitialState(){
		'use strict';

		return {

			'bridges': null,
			'currentBridge': null

		};
	},

	onChangeFromBridgeStore: function (eventName, data){
		"use strict";

		if( eventName === 'receivedBridge' ) {
			this.setState({bridges: data});
		} else if (
			eventName === 'updatedBridge' || eventName === 'openBridge'
		){
			this.setState({currentBridge: data});
		}

	},

	initBridgeStoreListener: function (data){
		"use strict";

		this.setState({
			bridges: data.data.bridges,
			currentBridge: data.data.currentBridge
		});

	},

	componentWillMount: function(){
		'use strict';

		this.listenTo(
			bridgeStore,
			this.onChangeFromBridgeStore,
			this.initBridgeStoreListener
		);

	},

	componentDidMount: function(){
		$("#panel1-scroll").mCustomScrollbar({
			autoHideScrollbar:true,
			theme:"dark",
			scrollInertia:0
		});
	},

	render: function (){
		"use strict";
		let primary = {
			'color':'white'
		};

		return (

			<div
				id="panel-1"
				zDepth={2}
				className="panel-one"
				>
				<div className="panel-1-content">
					<Header
						userProfile={this.props.userProfile}
						history={ this.props.history }
						openBModal={this.props.openBModal}
						/>

					<Tabs inkBarStyle={{'backgroundColor':"#8BC34A"}}>
						<Tab
							icon={<FontIcon className="material-icons">message</FontIcon>}
							/>
						<Tab
							icon={<FontIcon className="material-icons">person</FontIcon>}
							/>

					</Tabs>
					<section id='panel1-scroll'>
						<BridgeList
							userProfile={this.props.userProfile}
							bridges={this.state.bridges}
							currentBridgeId={this.state.currentBridge.id}
							/>
					</section>
				</div>
			</div>
		);
	}
});

module.exports = PanelOne;
