'use strict';

import React from 'react';

import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import BuildBridge from './BuildBridge';
import Notifications from './Header/Notifications/Notifications';

export default class InfoUser extends React.Component{
	constructor(props){
		super(props);
	}

	render(){

		return(
			<div className="user-info  mdl-color-text--teal-200">

				<div className="info">{ this.props.userProfile.name }</div>
				<div className="icon">
					<Notifications
						profile={ this.props.userProfile }
					/>
				</div>
				<BuildBridge
					profile={ this.props.userProfile }
				/>
			</div>
		);
	}
}
