/* components/panel1/Avatar.jsx
*
* React component user avatar
*
*/
'use strict';

import React from 'react';

import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';
import MenuItem from 'material-ui/MenuItem';

import history from '../../HistoryContainer';

module.exports = React.createClass({

	displayName: "Avatar",

	_selectProfileSettings: function(){
		'use strict';

		let parts = location.pathname.split('/');
		let pathEnd = parts[4];
		if(pathEnd === 'user'){
			pathEnd = '/';
		}else{
			pathEnd = '/user/';
		}
		parts = parts.slice(0, 4);
		let userPath = parts.join('/') + pathEnd;
		history.pushState({}, userPath);
	},

	render: function(){

		return (
			<div
				onClick={this._selectProfileSettings}
				disabled={false}
			>
				<Avatar
					style={ {'marginRight': '5px'} }
					src={this.props.profile.avatar}
				/>
				@{this.props.profile.username}
			</div>
		);

	}

});
