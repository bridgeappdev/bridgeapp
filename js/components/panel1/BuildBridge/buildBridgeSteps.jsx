import React from 'react';
import Dialog from 'material-ui/Dialog';
import {IconButton} from 'react-mdl';
import {
	Step,
	Stepper,
	StepLabel
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import FormDetail from '../../bridge/ChatBridgeForm/ChatBridgeForm';
import FormTags from '../../bridge/ChatBridgeForm/TagsBridgeForm';
import FormEvent from '../../bridge/ChatBridgeForm/EventBridgeForm';
import UploadImage from '../../bridge/ChatBridgeForm/UploadImage';
import bridgeAPI from '../../../models/bridges/Bridges';

export default class BuildBridgeSteps extends React.Component{
	constructor(){
		super();
		this.state = {
			open:false,
			stepIndex :0,
			type:'discussion',
			data:{
				name:''
			}
		};
	}

	_open(type,initialData,isEdit){
		let data  = initialData || {name:'',title:''};
		let edit = isEdit || false;
		this.setState({open:true,type:type,stepIndex:0,data:data,isEdit:edit});
	}
	_close(){
		this.setState({open:false,data:{}});
	}

	handleNext(){
		const {stepIndex} = this.state;
		this.setState({
			stepIndex: stepIndex + 1,
			finished: stepIndex >= 2,
		});
	}

	handlePrev(){
		const {stepIndex} = this.state;
		if (stepIndex > 0) {
			this.setState({stepIndex: stepIndex - 1});
		}
	}


	_getSteps(type){
		let steps = [];
		steps.push(
			<Step key="step-0">
				<StepLabel>{(this.state.isEdit)?'Edit':'Add'} Bridge Details</StepLabel>
			</Step>
		);
		if(type=='EVENT')
		steps.push(
			<Step key="step-1">
				<StepLabel>{(this.state.isEdit)?'Edit':'Add'} event details</StepLabel>
			</Step>
		);
		steps.push(
			<Step key="step-2">
				<StepLabel>Tag your bridge</StepLabel>
			</Step>
		);
		steps.push(
			<Step key="step-3">
				<StepLabel>Upload an image</StepLabel>
			</Step>
		);

		return steps;

	}

	_getContent( type ){
		const content = [];
		content.push(
			<FormDetail
				ref="formDetail"
				changeProperty={ this._changeProperty.bind( this ) }
				profile={ this.props.profile }
				checkBridgeNameAvailable={ this.checkBridgeNameAvailable.bind( this ) }
				data={ this.state.data }
			/>
		);

		if( type === 'EVENT' ){
			content.push(
				<FormEvent
					data={ this.state.data }
					changeProperty={ this._changeProperty.bind( this ) }
				/>
			);
		}

		content.push(
			<FormTags
				ref="formTags"
				data={	this.state.data	}
				changeProperty={	this._changeProperty.bind(	this	)	}
			/>
		);

		content.push(
			<UploadImage
				ref="uploadImage"
				data={	this.state.data	}
				changeProperty={	this._changeProperty.bind(	this	)	}
			/>
		);

		return content;
	}


	checkBridgeNameAvailable(bridgeName, cb){

		return bridgeAPI.checkNameAvailable(this.props.profile.username, bridgeName, cb);

	}

	_onSubmit(){
		const self = this;
		const bridgeData = this.state.data;
		bridgeData.type = this.state.type;
		bridgeData.visibility = 'public';

		this.refs.uploadImage._upload((error,isUpload,data)=>{
			if(isUpload){
				bridgeData.img_url = data.url;
			}
			console.log(bridgeData);
			bridgeAPI.saveNewBridge(
				this.props.profile,
				bridgeData,
				function ( error ){
					if( error ){
						console.log('There was a problem saving the bridge', e);
					}else{
						self._close();
					}
					return false;
				}
			);
		});



	}

	_update(){

		const _self = this;
		const bridge = this.state.data;
		const myUsername = this.props.profile.username;

		this.refs.uploadImage._upload((error,isUpload,data)=>{
			if(isUpload){
				bridge.img_url = data.url;
			}
			bridge.update( function ( error ){
				if( error ){
					console.log('There was a problem saving the bridge: ', bridge.name );
				}else{
					return _self._close();
				}
			});

		});


	}
	_changeProperty(key,value){
		let form = this.state.data ;
		form[key] = value;
		this.setState({data:form});
	}
	render(){
		const {stepIndex,type} = this.state;
		let steps =[];
		let contents =[];
		if(this.state.open){
			steps = this._getSteps(type);
			contents = this._getContent(type);
		}

		let nextButton = (<RaisedButton  label="Next"   primary={true}  onTouchTap={this.handleNext.bind(this)}   />);
		if(stepIndex === steps.length-1 ){
			if(!this.state.isEdit){
				nextButton = (<RaisedButton  label="Public Bridge"   primary={true}  onTouchTap={this._onSubmit.bind(this)}   />);
			}else{
				nextButton = (<RaisedButton  label="Update Bridge"   primary={true}  onTouchTap={this._update.bind(this)}   />);
			}
		}
		const actions = [
			<FlatButton
				label="Back"
				disabled={stepIndex === 0}
				onTouchTap={this.handlePrev.bind(this)}
				style={{marginRight: 12}}
				/>,
			nextButton
		];
		return (
			<Dialog modal={true} open={this.state.open} actions  = {actions} >
				<div className="mdl-cell mdl-cell--12-col newBridge--form-head-form">
					<IconButton name="close" style={{float:'right'}} onClick={this._close.bind(this)}/>
				</div>
				<Stepper activeStep={this.state.stepIndex}>
					{steps}
				</Stepper>
				{contents[this.state.stepIndex]}
			</Dialog>
		);
	}
}
