/* components/panel1/BuildBridge/index.jsx
*
* React component for the build a new bridge modal content
* Three stages
* ONE: Choose bridge type
* TWO: Fill in details and share
* THREE: Invite users to your bridge
*
* This files is written using es6 syntax and currently compiled
* using babel
* */
'use strict';

import React from 'react';
import bridgeAPI from '../../../models/bridges/Bridges';

import StageOne from './StageOne';
import StageTwoChat from '../../bridge/ChatBridgeForm';
import StageTwoEvent from '../../bridge/EventBridgeForm';

import IconButton from 'material-ui/IconButton/IconButton';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import BuildBridgePanel from './buildBridgePanel';
var BuildBridge = {};

BuildBridge = React.createClass({

	displayName: "BuildBridge",

	getInitialState: function () {

		return {open:false, stage:1}

	},

	dialogTitles: {
		'chat': "BUILD A BRIDGE",
		'event': "ADD YOUR EVENT DETAILS"
	},

	updateStateFromStagesActions: function(partialStateObject){

		this.setState(partialStateObject);

	},

	handleBackClick: function(){

		this.setState({stage: this.state.stage -1});
	},

	checkBridgeNameAvailable: function(bridgeName, cb){

		return bridgeAPI.checkNameAvailable(this.props.profile.username, bridgeName, cb);

	},

	openBuildBridgeModal : function(){

		this.setState({'open': true});
	},

	closeBuildBridgeModal : function(){

		this.setState({'open': false, 'stage': 1});
	},

	createNewBridge: function(bridgeData){

		if(!bridgeData){
			return console.log('BUILDBRIDGES::createNewBridge ==> Why is there no data?');
		}

		var _self = this;

		console.log('BUILDBRIDGES::createNewBridge ==> ', bridgeData);
		bridgeAPI.saveNewBridge(
			this.props.profile,
			bridgeData,
			function ( error ){
				if( error ){
					console.log('There was a problem saving the bridge', e);
				}else{
					//TODO: Callback for error handling
					_self.setState({'open': false, 'stage': 1});
				}
				return false;
			}
		);

	},

	_open:function(){
		this.refs.buildBridgePanel._open();
	},

	render: function(){

		return (
			<div>
				<div className="icon">
					<IconButton
						onClick={ this._open }
					>
						<span
							className="material-icons mdl-color-text--white"
						>
							add
						</span>
					</IconButton>
				</div>
				<BuildBridgePanel ref="buildBridgePanel" profile = {this.props.profile}/>

			</div>
		);

	}

});

module.exports = BuildBridge;
