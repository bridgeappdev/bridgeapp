import React from 'react';
import Dialog from 'material-ui/Dialog';
import {IconButton} from 'react-mdl';
import PanelBuildBridgeSteps from './buildBridgeSteps';
export default class BuildBridgePanel extends React.Component{
	constructor(){
		super();
		this.state = {
			open:false,
			imageClass:'newBridge--form-image--event'
		};
	}
	_open(){
		this.setState({open:true});
	}
	_close(){
		this.setState({open:false});
	}
	_changeImage(type,e){
		switch (type) {
			case 'event':
				this.setState({imageClass:'newBridge--form-image--event'});
				break;
			case 'discussion':
				this.setState({imageClass:'newBridge--form-image--discussion'});
				break;

		}
	}
	_opentEvent(e){
		e.preventDefault();
		this.refs.PanelBuildBridgeSteps._open('EVENT');
		this._close();
	}
	_openDiscussion(e){
		e.preventDefault();
		this.refs.PanelBuildBridgeSteps._open('CHAT');
		this._close();
	}
	render(){
		return (
			<div>
				<Dialog
					modal={true}
					open={this.state.open}
					className="newBridge--form"
					bodyStyle={{padding:'0px'}}
				>
					<div className="mdl-grid">
						<div className="mdl-cell mdl-cell--12-col newBridge--form-head">
							<IconButton name="close" style={{float:'right'}}  onClick={this._close.bind(this)}/>
						</div>
						<div className={"mdl-cell mdl-cell--5-col newBridge--form-image " + this.state.imageClass}>
						</div>
						<div className="mdl-cell mdl-cell--7-col newBridge--form-options">
							<a href className="newBridge--form-option" onMouseOver ={this._changeImage.bind(this,'event')} onClick={this._opentEvent.bind(this)}>
								<span className="newBridge--form-option-add">Add</span>
								Event
								<div className="newBridge--form-option-description">
									an ongoing chat based on a particular theme, topic or interest
								</div>
							</a>
							<a href  className="newBridge--form-option"  onMouseOver ={this._changeImage.bind(this,'discussion')} onClick={this._openDiscussion.bind(this)}>
								<span className="newBridge--form-option-add">Add</span>
								Discussion
								<div className="newBridge--form-option-description">
									an ongoing chat based on a particular theme, topic or interest
								</div>
							</a>
						</div>
					</div>
				</Dialog>
				<PanelBuildBridgeSteps ref="PanelBuildBridgeSteps" profile = {this.props.profile}/>
			</div>
		);
	}
}
