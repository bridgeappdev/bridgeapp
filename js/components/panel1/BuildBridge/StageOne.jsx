/* components/panel1/BuildBridge/StageOne.jsx
 *
 * React components declaration for stage one of the build a new bridge
 * modal content
 *
 * This files is written using es6 syntax and currently compiled
 * using babel
 * */

/*jshint node: true*/
/**/

import React from 'react';
import Paper from 'material-ui/Paper';
import IconButton from 'material-ui/IconButton';

import SVGCloseIcon from '../../icons/SVGCloseIcon';

const StageOne = React.createClass({

	displayName: "StageOne",

	getInitialState(){
		'use strict';

		return {

			'chatZdepth': 0,
			'eventZdepth': 0

		}
	},

	handleMouseEnter: function(buttonType){
		'use strict';

		var zDepthtype;
		switch(buttonType){

			case 'chat':
			case 'CHAT':
				zDepthtype = 'chatZdepth';
				break;

			case 'event':
			case 'EVENT':
				zDepthtype = 'eventZdepth';
				break;

		}
		return this.setState({[zDepthtype]:2});

	},

	handleMouseLeave: function(){
		'use strict';

		return this.setState({'chatZdepth':0, 'eventZdepth':0});

	},

	_handleClick: function(buttonType){
		'use strict';

		return this.props.onStateAction({'stage':2, 'bridgeType': buttonType});
	},

	_handleClickClose: function(buttonType){
		'use strict';

		this.props.onClickClose();

	},

	render: function(){
		'use strict';

		const style = {
			float:'left',
			width: '40%',
			overflow: 'auto',
			textAlign: 'center',
			display: 'block',
			marginBottom: '10px',
			marginLeft: '10px'
		};

		return (

			<div className="build-bridge-container">
				<div className="build-bridge-close">
					<IconButton tooltip="close" tooltipPosition="top-left" onClick={this._handleClickClose}>
					<SVGCloseIcon onClick={this._handleClickClose}/>
					</IconButton>
				</div>
				<p className="build-bridge-subtitle">first thing, tell us whether it's</p>
				<div className="build-bridge-inner-container">

					<Paper zDepth={this.state.chatZdepth} style={style}>
						<div className="build-bridge-button chat"
								onMouseEnter={this.handleMouseEnter.bind(this, 'chat')}
								onMouseLeave={this.handleMouseLeave}
								onClick={this._handleClick.bind(this, 'chat')}>
							<img src="/assets/img/chat-bridge.jpg" />
							<h3>A DISCUSSION</h3>
							<p>an ongoing chat based on a particular theme, topic or interest</p>
						</div>
					</Paper>

					<h1 className="build-bridge-button-or">OR</h1>

					<Paper zDepth={this.state.eventZdepth} style={style}>
						<div className="build-bridge-button event"
								onMouseEnter={this.handleMouseEnter.bind(this, 'event')}
								onMouseLeave={this.handleMouseLeave}
								onClick={this._handleClick.bind(this, 'event')}>
							<img src="/assets/img/chat-bridge.jpg"/>
							<img src="/assets/img/event-bridge.svg"/>
							<h3>AN EVENT</h3>
							<p>a date specific event, either online (webinar) or offline (meetup)</p>
						</div>
					</Paper>
					<div className='clear' />
				</div>
			</div>

			);

	}

});

export default StageOne;
