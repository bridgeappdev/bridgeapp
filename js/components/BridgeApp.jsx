/* Main.js
* React components for bridge app.
* Entry point for the UI
*/
"use strict";

import React        from 'react';

module.exports = React.createClass({

	displayName: 'BridgeApp',

	render: function() {

		return (

			< div id = "app-wrapper" >

			{ this.props.children }

			< /div>

		);
	}

});
