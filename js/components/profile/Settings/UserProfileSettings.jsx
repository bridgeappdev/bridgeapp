'use strict';

import React from 'react';

import ProfileAPI from '../../../lib/UserProfileAPI';
import SearchAPI from '../../../lib/SearchAPI';
import TagAPI from '../../../models/tags/tags';

import AppBar from 'material-ui/AppBar';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import {List, ListItem} from 'material-ui/List';
import FontIcon from 'material-ui/FontIcon';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';

import TagSuggestions from '../../common/TagSuggestions';
import ImageUploader from '../../common/ImageUploader';

var _handleAddTag = function(tagName){

	TagAPI.incrementTagCount(tagName, (err, count)=>{
		if(err){
			return console.warn('userProfileSettings::incrementTagCount ==> ', err);
		}else{
			return console.log('userProfileSettings::incrementTagCount ==> '
			+ 'Update the tag count by one for '
			+ tagName + ' : ', count);
		}
	});
};

var _handleRemoveTag = function(tagName){

	TagAPI.decrementTagCount(tagName, (err, count)=>{
		if(err){
			return console.warn('userProfileSettings::decrementTagCount ==> ', err);
		}else{
			return console.log('userProfileSettings::decrementTagCount ==> '
			+ 'Decrease the tag count by one for '
			+ tagName + ' : ', count);
		}
	});
};

module.exports = React.createClass({

	displayName: 'ProfileSettings',

	MAX_BIO_LENGTH: 250,

	URL_REGEX: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-]*)?\??(?:[\-\+=&;%@\.\w]*)#?(?:[\.\!\/\\\w]*))?)/g,

	LINKEDIN_REGEX: /(ftp|http|https):\/\/?((www|\w\w)\.)?linkedin.com(\w+:{0,1}\w*@)?(\S+)(:([0-9])+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/,

	getInitialState: function() {

		const initialProfileState = {
			bio: this.props.initialProfile.bio
		};

		return {
			profile: initialProfileState,
			userId: this.props.initialProfile.id,
			errors: {},
			Message: "Click the save button to save your changes."
		};
	},

	_canSubmit: function (){

		let errorList = this.state.errors;
		return !(errorList.website || errorList.linkedin);

	},

	handleChange: function(inputName, event) {

		var ProfileChange = this.state.profile;
		ProfileChange[inputName] = event.target.value;

		this.setState({
			profile: ProfileChange,
			Message: "Click the save button to save your changes."
		});
	},

	_handleBioChange: function(event) {


		let newBio = event.target.value,
		profile = this.state.profile;

		newBio = newBio.substring(0, this.MAX_BIO_LENGTH);
		profile.bio = newBio;
		this.setState({
			profile: profile
		});
	},

	_handleBlurUrl: function(e){

		let errorList = this.state.errors;
		let errMessage = "That doesn\'t seem right.";

		if(e.target.value.length === 0){
			errorList.website = null;
		}else if(!this.URL_REGEX.test(e.target.value)){
			errorList.website = errMessage;
		}else{
			errorList.website = null;
		}
		return this.setState({errors: errorList});

	},

	_handleBlurLIUrl: function(e){

		let errorList = this.state.errors;
		let errMessage = "That doesn\'t seem right.";

		if(e.target.value.length === 0){
			errorList.linkedin = null;
		}else if(!this.LINKEDIN_REGEX.test(e.target.value)){
			errorList.linkedin = errMessage;
		}else{
			errorList.linkedin = null;
		}
		return this.setState({errors: errorList});

	},

	handleSaveProfile: function() {

		if(this.props.onClick){
			return this.props.onClick();
		}

		if(!this._canSubmit()){
			return;
		}

		const profileState = this.state.profile;
		const userProfile = this.props.initialProfile;

		userProfile.avatar = this.avatar.getValue();
		//if( profileState.name ){
			userProfile.name = profileState.name;
		//}
		//if( userProfile.website ){
			userProfile.website = profileState.website;
	//	}
		//if( profileState.linkedin ){
			userProfile.linkedin = profileState.linkedin;
	//	}

		userProfile.bio = profileState.bio;
		userProfile.tags = this.tags_input.getValue();

		userProfile.update( (err) => {
			let message = "Your changes have been saved.";
			if(err){
				message = "Something went wrong while saving you updates."
			}
			return this.setState({
				Message: message
			});
		})
	},

	render: function() {


		const containerStyle = this.props.rootStyle?this.props.rootStyle:{width:'60%', margin:'auto'};

		return (
			<div id="edit-profile" style={containerStyle}>
				<div
					className='edit-profile-title'
					style={{textAlign:'center'}}
					>
					<h1>Edit your Profile</h1>
				</div>
				<div className='user-profile-settings-photo'>
					<ImageUploader
						ref={(ref) => this.avatar = ref}
						initialValue={this.props.initialProfile.avatar}
						overlayText='Update your profile photo'
						/>
				</div>
				<div className="panel-content">
					<TextField
						ref={(ref) => this.name_input = ref}
						hintText="Name"
						floatingLabelText="Name"
						defaultValue={this.props.initialProfile.name}
						fullWidth={true}
						onChange={this.handleChange.bind(this, 'name')}
						/>
					<TextField
						ref={(ref) => this.bio_input = ref}
						floatingLabelText="Bio"
						value={this.state.profile.bio}
						multiLine={true}
						fullWidth={true}
						rows={2}
						onChange={this._handleBioChange}
						/>
					<TextField
						ref={(ref) => this.website_input = ref}
						floatingLabelText="Website"
						defaultValue={this.props.initialProfile.website}
						errorText={this.state.errors.website}
						fullWidth={true}
						onChange={this.handleChange.bind(this, 'website')}
						onBlur={this._handleBlurUrl}
						/>
					<TextField
						floatingLabelText="LinkedIn"
						fullWidth={true}
						defaultValue={this.props.initialProfile.linkedin}
						errorText={this.state.errors.linkedin}
						onChange={this.handleChange.bind(this, 'linkedin')}
						onBlur={this._handleBlurLIUrl}
						/>
					<div style={{overflow:'visible', textAlign:'center'}}>
						<div style={{display:'inline-block', width:'95%'}} className="large_bottom_margin">
							<TagSuggestions
								ref={(ref) => this.tags_input = ref}
								initialValue={this.props.initialProfile.tags}
								tagSuggest={SearchAPI.tagSuggest}
								onAddTag={_handleAddTag}
								onRemoveTag={_handleRemoveTag}
								/>
						</div>
						<div
							data-title='Add tags to indicate your interests. This will help people find you and we can suggest bridges you might like.'
							style={{display:'inline-block'}}
							className='uptags-tooltip'
							>
							?
						</div>
					</div>

					<RaisedButton
						label="Save changes"
						secondary={true}
						fullWidth={true}
						onClick={this.handleSaveProfile}
						disabled={!this._canSubmit()}
						/>
					<div style={{marginTop: '10px'}}>
						<p id="message">{this.state.Message}</p>
					</div>
				</div>

			</div>
		);
	}
});
