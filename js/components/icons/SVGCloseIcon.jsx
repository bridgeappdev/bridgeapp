import React from 'react';
import NavigationClose from 'material-ui/svg-icons/navigation/close';

const iconStyles = {
  marginRight: 24,
};

const SvgCloseIcon = () => (
  <div>
    <NavigationClose
      viewBox={'0,0,24,24'}
      style={iconStyles}
      color={'#000000'}
      hoverColor={'#f1f8e9'}
    />
  </div>
);

export default SvgCloseIcon;
