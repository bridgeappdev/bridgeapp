'use strict';

import React from 'react';

import ChatBridgeForm  from '../ChatBridgeForm';
import EventBridgeForm from '../EventBridgeForm';

import BridgesAPI from '../../../lib/BridgesAPI';

const BridgeSettings = React.createClass({
  displayName: 'BridgeSettings',

	checkBridgeNameAvailable(bridgeName, next){

		return BridgesAPI.checkNameAvailable(
			this.props.userProfile.username,
			bridgeName,
			next
		);

	},

	updateBridge(bridgeData){

		if(!bridgeData){
			return console.log('BRIDGESETTINGS::updateBridge ==> Why is there no data?');
		}

		console.log('BRIDGESETTINGS::updateBridge ==> ', bridgeData);

		const _self = this;
		const myUsername = this.props.userProfile.username;
		bridgeData.id = this.props.initialData.id;
		let bridgeAPI = require('../../../lib/BridgesAPI.js');
		bridgeAPI.updateBridge(myUsername, bridgeData, function (error,bridgeId){
			if(error){
				console.log('There was a problem saving the bridge', e);
			}else{
				_self.setState({'message': "All Good"});
			}
			return false;
		});

	},

	render(){

		let bridgeForm;
		switch (this.props.initialData.type) {
			case 'chat':
			case 'CHAT':
				bridgeForm = <ChatBridgeForm
				  initialData={this.props.initialData}
				  checkBridgeNameAvailable={this.checkBridgeNameAvailable}
				  buttonText='Save'
				  onSubmit={this.updateBridge}
				/>
				break;
			case 'event':
			case 'EVENT':
				this.props.initialData.eventSrc = 'edit';
				bridgeForm = <EventBridgeForm
				  initialData={this.props.initialData}
				  checkBridgeNameAvailable={this.checkBridgeNameAvailable}
				  buttonText='Save'
				  handleSubmit={this.updateBridge}
				/>
				break;
			default:

		}

		return (
			<div className='edit-bridge-container'>
				<div className='edit-bridge-header'>
					<h1>Update Bridge Details</h1>
				</div>
				{bridgeForm}
			</div>
		);
  }
});

export default BridgeSettings;
