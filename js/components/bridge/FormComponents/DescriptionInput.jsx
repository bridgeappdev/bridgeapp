var React = require('react');
import TextField from 'material-ui/TextField';

module.exports = React.createClass({

	displayName: "DescriptionInput",

  getInitialState: function() {
		'use strict';

		let intialValue = this.props.initialValue?this.props.initialValue:"";
    return {
			bridgeDescErrorText: null,
			bridgeDescValue: intialValue
		};
  },

	isValid: function(){
		'use strict';

		return true;

	},
	getValue: function(){
		'use strict';

		return this.refs.bridgedesc.getValue();

	},

	_handleDescriptionChange: function(e){
		'use strict';

		const maxLength = this.props.maxLength;
		this.setState({bridgeDescValue: e.target.value.substring(0,maxLength)});

	},

	render : function(){

		return (

			<div className="">

				<div className="">
					<TextField
						ref='bridgedesc'
						value={this.state.bridgeDescValue}
						floatingLabelText={this.props.floatingLabelText}
						hintText={this.props.hintText}
						multiLine={true}
						fullWidth={true}
						rows={3}
						rowsMax={3}
						errorText={this.state.bridgeDescErrorText}
						onChange={this._handleDescriptionChange}/>

					</div>
				</div>
			);
	}
});
