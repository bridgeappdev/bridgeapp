var React = require('react');
import TextField from 'material-ui/TextField';

module.exports = React.createClass({

	displayName: "TitleAndName",

  getInitialState: function() {
    return {
			bridgeTitleErrorText: null,
			bridgeNameErrorText: null,
			bridgeNameValue: this.props.initialValue.name,
			isValid: false
		};
  },

	componentDidMount: function(){
		'use strict';

		//Call the title blur after render to pass the event
		//name to bridge title if we are loading data from
		//Eventbrite.
		if(this.refs.eventtitle.getValue().length > 0){
		 return this.refs.eventtitle.props.onBlur();
		}

	},

	isValid: function(){
		'use strict';

		//return this.state.isValid;
		let titleValue = this.refs.eventtitle.getValue();
		let bridgeName = this.refs.bridgename.getValue();
		let noValue = (!titleValue.length || !bridgeName.length);
		return !(noValue || this.state.bridgeTitleErrorText || this.state.bridgeNameErrorText)

	},

	getTitleValue: function(){
		'use strict';

		return this.refs.eventtitle.getValue();

	},

	getNameValue: function(){
		'use strict';

		return this.refs.bridgename.getValue();

	},

	_handleBridgeTitleInputChange: function(e){
		'use strict';

		if(e.target.value.length === 0){
			return this.setState({
				bridgeTitleErrorText: 'Bridge Title is required',
				isValid:false});
		}else{
			return this.setState({
				bridgeTitleErrorText: null,
				isValid:true});
		}
	},

	_handleBridgeNameInputChange: function(e){
		'use strict';

		if(e.target.value.length === 0){
			return this.setState({
				bridgeNameErrorText: 'Bridge Name is required',
			 	bridgeNameValue: e.target.value,
				isValid:false});
		}else{
			return this.setState({
				bridgeNameErrorText: null,
			 	bridgeNameValue: e.target.value.replace(/\W+/g, "").toLowerCase(),
				isValid:true});
		}
	},

	_handleBridgeNameInputBlur: function(e){
		'use strict';

		var _self= this,
				bridgeName = this.refs.bridgename.getValue();
		if(this.props.initialValue){
			if(bridgeName === this.props.initialValue.name){
				return _self.setState({ bridgeNameErrorText: null, formIsValid: true });
			}
		}
		if(bridgeName.length === 0){
			return this.setState({ bridgeNameErrorText: 'Bridge Name is required', isValid:false}, this.props.isValid);
		}

		if(bridgeName.length > 21){
			return _self.setState({ bridgeNameErrorText: 'Bridge Names should be less than 21 characters', isValid:false }, this.props.isValid);
		}

		this.props.checkBridgeNameAvailable(bridgeName, function(err, isAvailable){
			if(err){
				return _self.setState({formMessage: 'Something went wrong! Please have another go.'});
			}
			if(isAvailable){
				return _self.setState({ bridgeNameErrorText: null, isValid: true }, _self.props.isValid);
			}else{
				_self.setState({ bridgeNameErrorText: 'That name is not available!' , isValid:false}, _self.props.isValid);
			}
		})
	},

	_handleBridgeTitleInputBlur: function(){
		'use strict';

		var titleValue = this.refs.eventtitle.getValue();
		if(!titleValue.length){
				return this.setState({
					bridgeTitleErrorText: 'Bridge Title is required.',
					isValid:false},
				 	this.props.isValid);
		}

		if(this.refs.bridgename.getValue().length === 0){
			let suggestedBridgeName =
				titleValue.replace(/\W+/g, "").toLowerCase();
			return this.setState({bridgeNameValue: suggestedBridgeName},
					this.refs.bridgename.props.onBlur);
		}

	},

	render : function(){
		'use strict';

		const floatingLabelText = this.props.titleLabelText ? this.props.titleLabelText : "BRIDGE TITLE";
		return (

			<div className='name-title-row' style={{marginBottom:'1.5em'}}>

				<TextField
					ref='eventtitle'
					defaultValue={this.props.initialValue.title}
					floatingLabelText={floatingLabelText}
					style={{float:'left', width:'55%'}}
					errorText={this.state.bridgeTitleErrorText}
					onChange={this._handleBridgeTitleInputChange}
					onBlur={this._handleBridgeTitleInputBlur} />

				<TextField
					ref='bridgename'
					value={this.state.bridgeNameValue}
					floatingLabelText="BRIDGE NAME"
					style={{float:'left', width:'40%', marginLeft:"5%"}}
					errorText={this.state.bridgeNameErrorText}
					onChange={this._handleBridgeNameInputChange}
					onBlur={this._handleBridgeNameInputBlur}/>

					<div style={{clear:'both'}}/>
				</div>
		);
	}
});
