import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

module.exports = React.createClass({

	displayName: "PublishButton",

	render : function(){
		'use strict';

		const style = {
			margin: 12,
		};

		const text = this.props.buttonText?this.props.buttonText:"Publish Bridge";

		return (
			<div>
				<RaisedButton
				  label={text}
				  primary={true}
				  style={style}
				  onTouchTap={this.props.handleSubmit}
				  disabled={!this.props.canSubmit}	/>
			</div>
		);
	}
});
