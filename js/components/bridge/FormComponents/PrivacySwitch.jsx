/* /components/common/bridge/PrivacySwitch.jsx
 *
 * Common components used for bridges.
 *
 * */
import React from 'react';
import {RadioButton, RadioButtonGroup} from 'material-ui/RadioButton';


module.exports = React.createClass({
	displayName: "PrivacySwitch",

	getValue: function(){
		'use strict';

		return this.refs.visibility.getSelectedValue();

	},

	setVisibiltyMessage: function(){
		'use strict';

		return;
	},

	render: function(){

		return (
			<div>
				<div className="visibility-label" style={{textAlign:"center", marginBottom:'.5em'}} >
					SET THE PRIVACY:
				</div>
				<RadioButtonGroup
					ref={'visibility'}
					name="bridgeVisibility"
					style={{width:'80%', margin:'auto'}}
					onChange={this.setVisibiltyMessage}
					defaultSelected="public">
					<RadioButton
					  value="public"
					  label="Public"
					  style={{width:'50%', float:'left'}} />
					<RadioButton
					  value="private"
					  label="Private"
					  style={{width:'50%', float:'left'}} />
				</RadioButtonGroup>
			</div>
		);
	}
});
