import React from 'react';

import TitleAndName from '../FormComponents/TitleAndName';
import AddressInput from './AddressInput';
import DateAndTime from './DataAndTime';
import ImageUploader from '../../common/ImageUploader';
//import PrivacySwitch from '../FormComponents/PrivacySwitch';
import DescriptionInput from '../FormComponents/DescriptionInput';
import PublishButton from '../FormComponents/PublishButton';

import moment from 'moment';
moment().format();

module.exports = React.createClass({

	displayName: "EventBridge",

	getInitialState: function() {
		return {
			formIsValid: false
		};
	},

	componentDidMount () {
		'use strict';

		this._isValid();
	},

	_isValid: function(){
		'use strict';

		var refs = this.refs;
		if(Object.keys(refs).length === 0){
			this.setState({ formIsValid: false });
		}

		var formIsValid = (
			refs.bridgenametitle.isValid() &&
			refs.venue.isValid()           &&
			refs.dateandtime.isValid()     &&
			refs.imgurl.isValid()          &&
			//refs.visibility.isValid()      &&
			//refs.bridgeTags.isValid()      &&
			refs.bridgedesc.isValid()
		);

		return this.setState({ formIsValid: formIsValid });
	},

	_onSubmit: function(){
		'use strict';

		var refs = this.refs;
		var brgName = refs.bridgenametitle.getNameValue();
		brgName = brgName.replace(/\W+/g, "").toLowerCase();
		if(brgName === null || brgName === '') {
			throw new Error('Bridge must have a name.');
		}

		var bridgeData = {'name': brgName};
		bridgeData.title = refs.bridgenametitle.getTitleValue();

		bridgeData.venue = refs.venue.getValue();
		bridgeData.date_and_time = refs.dateandtime.getValue();

		bridgeData.img_url = refs.imgurl.getValue();
		//bridgeData.visibility = this.refs.visibility.getValue(); //[private|public]
		bridgeData.visibility = 'public';

		bridgeData.desc = refs.bridgedesc.getValue();
		//bridgeData.tags = this.refs.bridgeTags.getValue().trim().split(' ');
		bridgeData.type = 'EVENT';

		return this.props.handleSubmit(bridgeData);
	},

	_stripHTML: function _stripHTML(html) {
		'use strict';

		var tmp = document.createElement("DIV");
		tmp.innerHTML = html;
		return tmp.textContent || tmp.innerText || "";
	},

	_formatVenueData: function _formatVenueData(venueObj, src){
		'use strict';

		let venue;
		switch (src) {
			case 'meetup':
			venue = {
				'address': {
					'address_1': venueObj.address_1,
					'address_2': venueObj.address_2,
					'city'     : venueObj.city,
					'region'   : venueObj.state
				}
			}
			venue.name = venueObj.name;
			break;
			default: //case 'edit'
			venue = {
				'address': {
					'address_1': venueObj.venue1,
					'address_2': venueObj.venue2,
					'city'     : venueObj.venue3,
					'region'   : venueObj.venue4
				}
			}
		}
		return venue;
	},

	getInitialValues: function(){
		'use strict';

		let data = this.props.eventData || this.props.initialData;

		//iV = intialValues
		var iV = {
			titleandname: '',
			venue: '',
			dataandtime: '',
			image: '',
			//visibility: '',
			description: ''
		};

		if(data){
			let src  = data.eventSrc; //Meetup or eventbrite

			switch(src){
				case 'eventbrite':
				iV.titleandname = { 'name': '', 'title': data.name.text};
				iV.venue = data.venue,
				iV.dateandtime = {
					start: data.start.utc,
					end:   data.end.utc
				};
				iV.image = data.logo.url;
				//iV.visibility = data.listed;
				iV.description = data.description.text;
				break;
				case 'meetup':
				let momentTime = moment(data.time);
				let startTime = momentTime.toISOString();
				let increment = data.duration?data.duration:10800000;
				let endTime = momentTime.add(increment, 'ms').toISOString();

				iV.titleandname = { 'name': '', 'title': data.name};
				iV.venue = this._formatVenueData(data.venue, src),
				iV.dateandtime = {
					start: startTime,
					end:   endTime
				};
				iV.image = data.photo_url;
				//iV.visibility = data.visibility;
				iV.description = this._stripHTML(data.description);
				break;
				default:
				iV.titleandname = { 'name': data.name, 'title': data.title};
				iV.venue = this._formatVenueData(data.venue, src),
				iV.dateandtime = data.date_and_time;
				iV.image = data.img_url;
				//iV.visibility = data.visibility;
				iV.description = data.desc;
			}
		}

		return iV;
	},

	render : function(){
		'use strict';

		const initialValues = this.getInitialValues();

		return (
			<div className="event-bridge-form">

				<TitleAndName
					ref='bridgenametitle'
					titleLabelText='EVENT TITLE'
					checkBridgeNameAvailable={this.props.checkBridgeNameAvailable}
					initialValue={initialValues.titleandname}
					isValid={this._isValid} />

				<div className='middle-two-columns'>
					<div style={{float:'left', marginRight:'5%'}}>
						<AddressInput ref='venue' initialValue={initialValues.venue}/>
						<DateAndTime ref='dateandtime' initialValue={initialValues.dateandtime}/>
					</div>

					<div style={{float:'left'}}>
						<ImageUploader
							ref='imgurl'
							initialValue={initialValues.image}
							overlayText='Upload event artwork'
						/>
					</div>

					<div style={{clear:'both'}}/>
				</div>

				<div className='description-row'>
					<div style={{float:'left', width:'60%'}}>
						<DescriptionInput
							ref='bridgedesc'
							floatingLabelText="EVENT DESCRIPTION"
							hintText="A brief description of your event (Optional)"
							initialValue={initialValues.description}/>
					</div>

					<div style={{float:'left', marginLeft:'8%', marginTop:'3%'}}>
						<PublishButton
							handleSubmit={this._onSubmit}
							canSubmit={this.state.formIsValid}
							buttonText={this.props.buttonText}
							/>
					</div>
				</div>
			</div>
		);
	}
});
