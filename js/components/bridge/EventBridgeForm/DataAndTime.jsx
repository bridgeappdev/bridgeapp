var React = require('react');
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';

module.exports = React.createClass({

	displayName: "DataAndTime",

	isValid: function(){
		'use strict';

		//Add validation if needed. For now it is valid.
		return true;

	},

	getInitialValues: function(){
		'use strict';

		var startdata = null, starttime = null, enddate = null, endtime = null,
				dateObject = this.props.initialValue, iV = {};

		if(dateObject && dateObject.start){
			iV.startdate = new Date(dateObject.start);
			iV.starttime = iV.startdate;
			iV.enddate = new Date(dateObject.end);
			iV.endtime = iV.enddate
		}else if(dateObject && dateObject.startdate){
			iV.startdate = new Date(dateObject.startdate);
			iV.starttime = new Date(dateObject.starttime);
			iV.enddate = new Date(dateObject.enddate);
			iV.endtime = new Date(dateObject.endtime)
		}

		return iV;
	},

	getValue: function(){
		'use strict';

		var valueObject = {},
				refs= this.refs;

		valueObject.startdate = refs.startdate.getDate().toString();
		valueObject.starttime = refs.starttime.getTime().toString();
		valueObject.enddate = refs.enddate.getDate().toString();
		valueObject.endtime = refs.endtime.getTime().toString();

		return valueObject;

	},

	render : function(){

		const datePickerInputStyle ={
			float:'left',
			width: '100%',
		};
		const datePickerStyle ={
			width: '40%',
			marginRight: '40px'
		};

		const initialValues = this.getInitialValues();

		return (
			<div className="mdl-grid mdl-grid--no-spacing">
				<div className="mdl-cell mdl-cell--3-col">
					<DatePicker
						ref='startdate'
						defaultDate={initialValues.startdate}
						autoOk={true}
					floatingLabelText="Start Date" fullWidth/>
				</div>
				<div className="mdl-cell mdl-cell--3-col">
					<TimePicker
						ref='starttime'
						defaultTime={initialValues.starttime}
						format="24hr"
						autoOk={true}
					floatingLabelText="Start Time" fullWidth/>
				</div>
				<div className="mdl-cell mdl-cell--3-col">
					<DatePicker
						ref='enddate'
						defaultDate={initialValues.enddate}
						autoOk={true}
					floatingLabelText="End Date"  fullWidth/>
				</div>
				<div  className="mdl-cell mdl-cell--3-col">
					<TimePicker
						ref='endtime'
						defaultTime={initialValues.endtime}
						format="24hr"
						autoOk={true}
					floatingLabelText="End Time" fullWidth/>
				</div>

			</div>
		);
	}
});
