import React from 'react';

import TextField from 'material-ui/TextField';

module.exports = React.createClass({

	displayName: "AddressInput",

	getInitialState: function(){
		'use strict';

		var address1 = '', address2 = '', address3 = '', address4 = '',
				venue = this.props.initialValue;

		if(venue){
			if(venue.name){
				venue.name = venue.name += ', ';
			}else{
				venue.name='';
			}
			address1 = venue.name + venue.address.address_1,
			address2 = venue.address.address_2,
			address3 = venue.address.city,
			address4 = venue.address.region
		}

		return {
			venue1: address1,
			venue2: address2,
			venue3: address3,
			venue4: address4
		}
	},

	getValue: function(){
		'use strict';

		var valueObject = {};

		valueObject.venue1 = this.refs.venueinput1.getValue();
		valueObject.venue2 = this.refs.venueinput2.getValue();
		valueObject.venue3 = this.refs.venueinput3.getValue();
		valueObject.venue4 = this.refs.venueinput4.getValue();

		return valueObject;
	},

	isValid: function(){
		'use strict';

		//Add validation if needed. For now it is valid.
		return true;

	},

	render : function(){

		return (
<div className="mdl-grid mdl-grid--no-spacing">
	<div className='mdl-cell mdl-cell--12-col'>
		<TextField
			ref='venueinput1'
			defaultValue={this.state.venue1}
			fullWidth={true}
			hintText="venue name"
		floatingLabelText="Venue Address / Location" />
	</div>
	<div className='mdl-cell mdl-cell--12-col'>
		<TextField
			fullWidth={true}
			floatingLabelText="district"
			defaultValue={this.state.venue2}
		ref='venueinput2'  />
	</div>
	<div className='mdl-cell mdl-cell--6-col'>
		<TextField
			floatingLabelText="city"
			defaultValue={this.state.venue3}
		ref='venueinput3' fullWidth/>
	</div>
	<div  className='mdl-cell mdl-cell--6-col'>
		<TextField
			floatingLabelText="county"
			defaultValue={this.state.venue4}
		ref='venueinput4' fullWidth/>
	</div>
</div>
			);
	}
});
