/* Invites/EmailInput.jsx
 * 
 * Component for collection an email address for bridge invites
 *
 * */

import React from 'react';
import TextField from 'material-ui/TextField';

const EmailInput = React.createClass({

	_handleOnChange(){

		return this.props.onChange(this.props.index, this.email_input.getValue());
	},

	_handleOnBlur(e){

		let candidateEmail = e.target.value;
		return this.props.onBlur(this.props.index, candidateEmail);
	},


	_handleClickRemove(){

		return this.props.onClickRemove(this.props.index);
	},

	render(){

		let removeField = this.props.showDelete ? 
		                    (<div onClick={this._handleClickRemove} className='email-input-remove'>x</div>) :
		                    null;

		return (
			<div style={{width:'110%'}}>
				<div className='email-input'>
					<TextField
					  ref={(ref) => this.email_input = ref}
					  value={this.props.data.email}
					  hintText="username@domain.com"
					  type="email"
					  fullWidth={this.props.fullWidth}
					  onBlur={this._handleOnBlur}
					  onChange={this._handleOnChange}
					  errorText={this.props.data.message}
					/>
				</div>
				{removeField}
			</div>
		);
	}
});

export default EmailInput;
