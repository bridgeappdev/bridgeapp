/*	Invites/Invites.jsx
 *
 *	UI component for sending out invites to a bridge
 *
 * */

import React from 'react';
import Reflux from 'reflux';

import Header from '../../common/Header';
import EmailInput from './EmailInput';
import BulkInput from './BulkInput';

import FloatingActionButton from 'material-ui/FloatingActionButton';
import Toggle from 'material-ui/Toggle';
import RaisedButton from 'material-ui/RaisedButton';

import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

import FontIcon from 'material-ui/FontIcon';

import Snackbar from 'material-ui/Snackbar';

import history from '../../../HistoryContainer';
import messageStore from '../../../stores/MessageStore';
import BridgeAPI from '../../../lib/BridgesAPI';

import MyRawTheme from '../../../themes/DefaultTheme';

const EMAILRX = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

const Invites = React.createClass({

	displayName: "Invites",

	getInitialState (){

		let field = {'email':'', 'message':null};
		return {
			'emailInvites': { 'inputs': [field] },
			'bulk': {'rawList': null, 'message':null},
			'useBulk': false,

			'sendMessage': '',
			'openSnackBar': false
		};
	},

	/*
	 * Event handlers
	 * */

	_handleToggleBulk (){
		return this.setState({useBulk: !this.state.useBulk});
	},

	_handleEmailInputChange(index, value){

		let emailInvites = this.state.emailInvites;
		emailInvites.inputs[index].email = value;
		return this.setState({'emailInvites': emailInvites});
	},

	_handleEmailInputBlur(index, candidateEmail){

		const errMessage = 'Are you sure that is an email address?';
		let emailInvites = this.state.emailInvites;

		if(candidateEmail === '' || EMAILRX.test(candidateEmail)){
			emailInvites.inputs[index].message = null;
		}else{
			emailInvites.inputs[index].message = errMessage;
		}

		return this.setState({'emailInvites': emailInvites});
	},

	_handleBulkInputChange(value){

		let bulkInvites = this.state.bulk;
		bulkInvites.rawList = value;
		return this.setState({'bulk': bulkInvites});
	},

	_handleBulkInputBlur(){

		if(!this.state.bulk.rawList){
			let resetLists = {
				'message': null,
				'duplicates': null,
				'errors': null,
				'parsedInvites': null
			}
			return this.setState({
				bulk: resetLists
			})
		}
		let parsedInvites = this._parseBulkEmails(this.state.bulk);
		return this.setState({'bulk': parsedInvites});
	},

	_handleAddInput(){

		let emailInvites = this.state.emailInvites;
		let newField = {'email':'', 'message':null};
		emailInvites.inputs.push(newField);
		return this.setState({'emailInvites': emailInvites});
	},

	_handleDeleteInput(index){

		let emailInvites = this.state.emailInvites;
		emailInvites.inputs.splice(index,1);
		return this.setState({'emailInvites': emailInvites});
	},

	/*
	 * Get email addressess from components
	 *
	 * */

	_parseBulkEmails(bulkEmailObj){
		let rawList = bulkEmailObj.rawList;
		let candidateEmails = rawList.split(/[\s,;]+/);
		let errors = [];
		let duplicates = [];
		let inviteAddresses = [];

		const errorMessage='There were some invalid emails submitted. Please check the list';

		for(let index = 0;index < candidateEmails.length;index++){
			let nextCandidateEmail = candidateEmails[index];

			if(nextCandidateEmail === ''){continue;}

			if(!EMAILRX.test(nextCandidateEmail)){
				errors.push(nextCandidateEmail);
				continue;
			}

			if(inviteAddresses.indexOf(nextCandidateEmail) === -1){
				inviteAddresses.push(nextCandidateEmail);
			}else{
				duplicates.push(nextCandidateEmail);
			}

		};

		let bulk = {'rawList': rawList};
		bulk.parsedInvites = inviteAddresses;
		if(errors.length !== 0){
			bulk.message = errorMessage;
			bulk.errors = errors;
		}else{
			bulk.message = null;
			bulk.errors = null;
		}

		if(duplicates.length !== 0){
			bulk.duplicates = duplicates;
		}else{
			bulk.duplicates = null;
		}

		return bulk;
	},

	_getBulkInvitesFromState(){

		return this._parseBulkEmails(this.state.bulk);

	},

	_getInvitesFromState(){

		let emailInvites = this.state.emailInvites;
		let emailList = this.state.emailInvites.inputs;
		let duplicates = [];
		let errors = [];
		let errorMessage='No invite was sent to this address! We don\'t think it is correct';
		let duplicateMessage ='We already found this email in the list. We only sent the invite once.';
		let inviteAddresses = [];

		for(let index = 0;index < emailList.length;index++){
			let emailObj = emailList[index];

			if(emailObj.email === ''){continue;}

			if(!EMAILRX.test(emailObj.email)){
				emailObj.message = errorMessage;
				errors.push(emailObj);
				continue;
			}

			if(inviteAddresses.indexOf(emailObj.email) === -1){
				inviteAddresses.push(emailObj.email);
			}else{
				emailObj.message = duplicateMessage;
				duplicates.push(emailObj);
			}

		};

		emailInvites.duplicates = duplicates;
		emailInvites.errors = errors;
		emailInvites.parsedInvites = inviteAddresses;

		return emailInvites;
	},

	_sendInvites(emailAddressList, cb){

		if(emailAddressList.length !== 0){
			let _self = this;
			let user = _self.props.userProfile;
			let userData = {
				'email': 'info@chakachat.com',
				'name': user.name,
				'username': user.username,
				'avatar': user.avatar
			};
			const bridge = _self.props.currBridge;
			const queryString = '?from=invite'
			const bridgeURL = location.origin + '/bridge/' + bridge.path + queryString;
			let bridgeData = {
				"name": bridge.name,
				"title": bridge.title,
				"description": bridge.desc,
				"url": bridgeURL
			};
			BridgeAPI.sendBridgeInvites(emailAddressList, userData, bridgeData, function(err, response){
				if(err){
					_self.setState({
						'sendMessage': err.message ? err.message : 'Invites were not sent.',
						'sendError': true,
						'openSnackBar': true
					});
				 	return cb(err);
				}

				_self.setState({
					'sendMessage': emailAddressList.length + ' invites were sent.',
					'sendError': false,
					'openSnackBar': true
				});
				return cb(null);

			});
		}else{
			console.log('INVITES::_sendInvites ==> Email list empty!');
		}

	},

	_submitInvites(){

		let parsedInvites, inviteAddresses = [], sendInviteCallback;

		const bulkSendCB = function(parsedInvites, err){
			if(err){
				return this.setState({'bulk': parsedInvites});
			}else{
				let resetLists = {
					'message': null,
					'duplicates': parsedInvites.duplicates,
					'errors': null,
					'rawList': '',
					'parsedInvites': null
				}
				return this.setState({
					bulk: resetLists
				})
			}
		};

		const inviteSendCB = function(parsedInvites, err){
			if(err){
				return;
			}else{
				let newInputs = parsedInvites.duplicates.concat(parsedInvites.errors);
				if(newInputs.length === 0){
					newInputs = [{'email':'', 'message':null}];
				}
				let newEmailInvites = {'inputs': newInputs};
				return this.setState({emailInvites: newEmailInvites});
			}
		};

		if(this.state.useBulk){
			parsedInvites = this._getBulkInvitesFromState();
			sendInviteCallback = bulkSendCB;
		}else{
			parsedInvites = this._getInvitesFromState();
			sendInviteCallback = inviteSendCB;
		}
		inviteAddresses = parsedInvites.parsedInvites;

		if(inviteAddresses.length !== 0){
			return this._sendInvites(inviteAddresses, sendInviteCallback.bind(this, parsedInvites));
		}

		return void 0;
	},

  closePanel: function(){
    this.path = /\/$/.test(location.pathname) ? location.pathname : location.pathname + "/";
		history.pushState({},this.path.split(/invite/)[0] );
  },

	handleSnackbarClose: function(){
		this.setState({
			openSnackBar: false,
		});
	},

	render(){

		const appBarStyle = {
			/*marginLeft: '-20px',
			padding: '0px 24px',
			width: '105%'*/
		};
		const appBarTitle = {
			paddingLeft:'10px'
		};
		let toolBarTitle = 'Invite to ' + this.props.currBridge.name;

		let  ToolbarTop = (
			<Toolbar
			className="panel4-header"
			style={appBarStyle}
			noGutter={true}>

				<ToolbarGroup  float="left">
					<ToolbarTitle text={toolBarTitle} style={appBarTitle}/>
				</ToolbarGroup>

				<ToolbarGroup float="right">
					<FontIcon className="fa fa-times" onClick={this.closePanel}/>
				</ToolbarGroup>

			</Toolbar>
			);

		let emailList = this.state.emailInvites.inputs;
		let bulkListObj = this.state.bulk;
		let showDelete = (emailList.length > 1);
		let emailInput, addInputField, bulkMaxEmailsText, reportDuplicates;

		if(this.state.useBulk){

			bulkMaxEmailsText = (<div><small>(max 500 at a time please)</small></div>);
			emailInput = <BulkInput
			fullWidth={true}
			data={bulkListObj}
			onBlur={this._handleBulkInputBlur}
			onChange={this._handleBulkInputChange}
			/>

			if(bulkListObj.duplicates){

				const duplicateMessage='Some emails were found twice. Only one email will be sent.';
				reportDuplicates = (
				<div style={{marginTop: '10px'}}>
					<p>{duplicateMessage}</p>
					<div className='dup-email-list'>
						<p>{bulkListObj.duplicates.join(', ')}</p>
					</div>
				</div>
				);

			}

		}else{

			emailInput = emailList.map((emailObj, index) => {
				return (
					<div
					key={'field_'+index}
					style={{width: '100%', overflow: 'visible'}}
					>
						<EmailInput
						fullWidth={true}
						index={index}
						data={emailObj}
						showDelete={showDelete}
						onBlur={this._handleEmailInputBlur}
						onChange={this._handleEmailInputChange}
						onClickRemove={this._handleDeleteInput}
						/>
					</div>
					);
			}, this);

			const addInputStyle = {
				float: 'left',
				marginTop: 10,
				marginBottom: 20,
			};

			addInputField =

			<FloatingActionButton
			mini={true}
			backgroundColor={MyRawTheme.palette.accent1Color}
			style={addInputStyle}
			onMouseDown={this._handleAddInput}
			>
				<span className='material-icons'>add</span>
			</FloatingActionButton>;

		}

		const submitStyle = {
			marginRight: 20,
			display:'inline',
			boxShadow:'none'
		};

		const toggleStyle = {
			marginBottom: 20,
			color: '#ffffff'
		};

		return (
			<div>
				<Header
				titleText={toolBarTitle}
				requestClose={this.closePanel}
				/>
				<div className="panel4-bridge-item">
					<div className='invite-header-text'>
					Send out emails to the people you want to join you!
					{bulkMaxEmailsText}
					</div>
					{emailInput}
					{addInputField}
					<div style={{float:'right' , marginTop:'20px', width: '40%'}}>
						<Toggle
						label="Add in bulk!"
						labelPosition="right"
						onToggle={this._handleToggleBulk}
						style={toggleStyle}
						/>
					</div>
				</div>


				<div className="panel4-bridge-item">
					<RaisedButton
					label="Send Invites"
					secondary={true}
					style={submitStyle}
					fullWidth={true}
					onMouseDown={this._submitInvites}
					/>
					{reportDuplicates}
					<Snackbar
					open={this.state.openSnackBar}
					message={this.state.sendMessage}
					autoHideDuration={4000}
					onRequestClose={this.handleSnackbarClose}
					/>
				</div>
			</div>
			);
	}
});

export default Invites;
