/* Invites/BulkInput.jsx
 * 
 * Component for collecting many email addresses for bridge invites
 *
 * */

import React from 'react';
import TextField from 'material-ui/TextField';

const BulkInput = React.createClass({

	_handleOnChange(){

		return this.props.onChange(this.bulk_input.getValue());
	},
	
	_handleOnBlur(e){

		let candidateEmail = e.target.value;
		return this.props.onBlur(candidateEmail);
	},

	render(){

		return (
			<div style={{width:'100%'}}>
				<div className='bulk-input'>
					<TextField
					  multiLine={true}
						rows={3}
						rowsMax={10}
					  ref={(ref) => this.bulk_input = ref}
					  value={this.props.data.rawList}
						floatingLabelText='Add multiple email addresses'
					  hintText="Paste you email list here, seperated by comma, semi-colon or space."
					  fullWidth={this.props.fullWidth}
					  onBlur={this._handleOnBlur}
					  onChange={this._handleOnChange}
					  errorText={this.props.data.message}
					/>
				</div>
			</div>
		);
	}
});

export default BulkInput;

