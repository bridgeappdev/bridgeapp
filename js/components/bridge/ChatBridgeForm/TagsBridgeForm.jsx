/* components/panel1/BuildBridge/StageTwoChat.jsx
 *
 * React components declaration for stage two of the build a new bridge
 *
 * This files is written using es6 syntax and currently compiled
 * using babel
 * */

import React from 'react';

import TextField  from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';

import TagSuggestionsInput from '../../common/TagSuggestions';
import PrivacySwitch       from '../FormComponents/PrivacySwitch';
import TitleAndName        from '../FormComponents/TitleAndName';
import DescriptionInput    from '../FormComponents/DescriptionInput';
import PublishButton       from '../FormComponents/PublishButton';

import SearchAPI from '../../../lib/SearchAPI';
import TagAPI from '../../../models/tags/tags';
import Chip from 'material-ui/Chip';

const styles = {
  chip: {
      margin: 4,
  },
  wrapper: {
      display: 'flex',
      flexWrap: 'wrap',
  },
};


export default class TagsBridgeForm extends React.Component{
	constructor(props){
		super(props);
        this.state = {
            tags:props.data.tags
        };
	}


    _handleAddTag(tagName){
        let self = this;
    	TagAPI.incrementTagCount(tagName, (err, count)=>{
    		if(err){
    			return console.warn('ChatBridgeForm::incrementTagCount ==> ', err);
    		}else{
                    self.props.changeProperty('tags',    self.refs.bridgeTags.getValue());
    			return console.log('ChatBridgeForm::incrementTagCount ==> '+ 'Update the tag count by one for '+ tagName + ' : ', count);
    		}
    	});
    }

    _handleRemoveTag(tagName){
        let self = this;
    	TagAPI.decrementTagCount(tagName, (err, count)=>{
    		if(err){
    			return console.warn('ChatBridgeForm::decrementTagCount ==> ', err);
    		}else{
                self.props.changeProperty('tags',    self.refs.bridgeTags.getValue());
    			return console.log('ChatBridgeForm::decrementTagCount ==> '+ 'Decrease the tag count by one for '+ tagName + ' : ', count);
    		}
    	});
    }

    getTags(){
        this.refs.bridgeTags.getValue();
    }
	render(){
		return (

            <div className='new-bridge-form'>
                <div className="modal-body clearfix" style={{minHeight:'250px'}}>

                    <TagSuggestionsInput
                        ref='bridgeTags'
                        tagSuggest={SearchAPI.tagSuggest}
                        onAddTag={this._handleAddTag.bind(this)}
                        onRemoveTag={this._handleRemoveTag.bind(this)}
                        initialValue={this.state.tags}      />

                </div>
            </div>
			);
	}

}
