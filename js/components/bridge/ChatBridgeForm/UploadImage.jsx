import React from 'react';
import Dropzone from 'react-dropzone';
import ReactDOM from 'react-dom';
import S3 from '../../../lib/s3';
export default class TagsBridgeForm extends React.Component{
	constructor(props){
		super(props);
		this.state = {
			file:null
		}
	}
	_onDrop(files){
		let self = this;
		var reader = new FileReader();
	      reader.onload = function (e) {
	      	self.imageBase64 = e.target.result;
	            self.setState({file:files[0],preview:true,image:e.target.result});
	       };
	       reader.readAsDataURL(files[0]);

	}
	_upload(next){
		let _self = this;
		if(this.state.preview){
			S3.getS3UrlUpload(
				this.state.file,
				true,
				this.props.username,
				( error, data ) => {
					const s3URL = data.s3url;
					const fileKey = data.key;
					S3.sendFile(
						this.state.file,
						s3URL,
						( loaded, total ) => {
							let progress = Math.round(loaded*100/total);
							_self.setState({progress:progress});
						},( error, data ) => {
							const fileMeta = {};
							fileMeta.url = s3URL.split('?')[0];
							fileMeta.key = fileKey;
							fileMeta.title = _self.state.title;
							fileMeta.name = _self.state.file.name;
							fileMeta.type = _self.state.file.name.split('.').pop().toLowerCase();
							//_self.setState({open:false});
							return next( null,true,fileMeta );
						}
					);
				}
			);
		}else{
			return next( null,false );
		}
	}
	render(){
		let style  = {
			borderColor:"#f2f2f2",
			height:'300px',
		      width:'100%',
		      borderStyle:'dashed',
		      textAlign:'center'
		    };
		 let activeStyle = {
		 	height:'300px',
		 	width:'100%',
		      borderColor: '#c2c2c2',
		      borderStyle: 'dashed'
		 };

		return(
				<Dropzone
					ref="dropzone"
					onDrop={this._onDrop.bind(this)}
					style={style}
					activeStyle={activeStyle}
				>
				{
					(this.state.file||this.props.data.img_url)?<img src={(this.state.file)?this.state.file.preview:this.props.data.img_url}  style={{maxWidth:'726px'}}/>:"Upload an image..."
				}
				</Dropzone>
		);
	}
}