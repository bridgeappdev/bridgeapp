import React from 'react';
import TextField  from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import TagSuggestionsInput from '../../common/TagSuggestions';
import PrivacySwitch       from '../FormComponents/PrivacySwitch';
import TitleAndName        from '../FormComponents/TitleAndName';
import DescriptionInput    from '../FormComponents/DescriptionInput';
import PublishButton       from '../FormComponents/PublishButton';
import SearchAPI from '../../../lib/SearchAPI';
import TagAPI from '../../../models/tags/tags';
import bridgeAPI from '../../../models/bridges/Bridges';

const ERRORS = {
	REQUIRED:{
		title:'Bridge Title is required',
		name:'Bridge Name is required',
		desc:'Description is required'
	},
	MAXLENGTH:{
		name:'Bridge Names should be less than 21 characters'
	}
};
const ValidationForm = {
	name:{maxLength:21,required:true},
	title:{maxLength:50,required:true},
	desc:{maxLength:200,required:false}
};
export default class  ChatBridgeForm extends React.Component{
	constructor(props){
		super(props);
		this.state =  {
			errors:{},
			bridgeDescErrorText: null,
			formIsValid: false,
			form:props.data
		};
	}

	componentWillMount () {
		if( this.props.data ){
			this.setState({form:this.props.data});
		}
	}
	componentDidMount(){
		if( !this.props.data){
			return this._validateAvailable();
		}
		return void 0;
	}

	_changeInput(key,e){
		let form = this.state.form;
		let errors = this.state.errors;
		form[key] = e.target.value;
		form = this._changeName(key,form);
		errors = this._validateField(key,e.target.value,errors);
		this.setState({errors:errors});
		this.props.changeProperty(key, e.target.value);

	}

	componentWillReceiveProps(nextProps){
		this.setState({form:nextProps.data});
	}

	_changeName( key, form ){
		if( (!this.props.data) && (key=='title' || key=='name') ){
			form.name = form[key].replace(/\W+/g, "").toLowerCase();
		}
		 return form;
	}
	_checkBridgeNameAvailable(key, value, cb){
		if(key==='name')
			return bridgeAPI.checkNameAvailable(this.props.profile.username, value, cb);
		}

	_validateAvailable(){
		let errors = this.state.errors;
		let self = this;
		let value = this.state.form.name;
		return bridgeAPI.checkNameAvailable(this.props.profile.username, value, (error,available)=>{
			if(!available){
				errors.name = "That name is not available!";
				self.setState({errors:errors});
			}
		});
	}

	_validateField(key,value,errors){
		errors[key] ="";
		if(ValidationForm[key].required){
			errors[key]  = (value.length === 0)?ERRORS.REQUIRED[key]:errors[key] ;
		}
		if(ValidationForm[key].maxLength){
			errors[key]  = (value.length > ValidationForm[key].maxLength)?ERRORS.MAXLENGTH[key]:errors[key] ;
		}

		return errors;

	}


	render(){
		const isEdit = !!this.props.data;

		return (
				<div  className="mdl-grid">
					<div className="mdl-cell mdl-cell--7-col">
						<TextField
							onChange={ (e)=>{this._changeInput('title',e);}}
							ref='title'
							value={this.state.form.title}
							floatingLabelText="Bridge Title"
							errorText={this.state.errors.title} fullWidth/>
					</div>
					<div className="mdl-cell mdl-cell--5-col">
						<TextField
							disabled={isEdit}
							onChange={ (e)=>{this._changeInput('name',e);}}
							onBlur={ isEdit ? null : this._validateAvailable.bind(this)}
							ref='name'
							value={this.state.form.name}
							floatingLabelText="Bridge Name"
							errorText={this.state.errors.name} fullWidth/>
					</div>
					<div className="mdl-cell mdl-cell--12-col">
						<TextField
							onChange={ (e)=>{this._changeInput('desc',e);}}
							ref='desc'
							value={this.state.form.desc}
							floatingLabelText="Description"
							errorText={this.state.errors.desc} fullWidth/>
					</div>

				</div>
			);
	}

}
