import React from 'react';
import TextField  from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import AddressInput from '../EventBridgeForm/AddressInput';
import DateAndTime from '../EventBridgeForm/DataAndTime';
import DatePicker from 'material-ui/DatePicker';
import TimePicker from 'material-ui/TimePicker';

export default class EventBridgeForm extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            form:props.data
        };
    }

    _changeInput(key,e){
		let form = this.state.form;
		let errors = this.state.errors;
		form[key] = e.target.value;
		form = this._changeName(key,form);
		errors = this._validateField(key,e.target.value,errors);
		this.setState({errors:errors});
		this.props.changeProperty(key, e.target.value);

	}

    render(){

        return (
                <div className="mdl-grid  mdl-grid--no-spacing">
                    <div className='mdl-cell mdl-cell--12-col'>
                        <TextField
                            ref='venueinput1'
                            defaultValue={this.state.form.venue1}
                            fullWidth={true}
                            hintText="venue name"
                            floatingLabelText="Venue Address / Location" />
                    </div>
                    <div className='mdl-cell mdl-cell--12-col'>
                        <TextField
                            fullWidth={true}
                            floatingLabelText="district"
                            defaultValue={this.state.form.venue2}
                            ref='venueinput2'  />
                    </div>
                    <div className='mdl-cell mdl-cell--6-col'>
                        <TextField
                            floatingLabelText="city"
                            defaultValue={this.state.form.venue3}
                            ref='venueinput3' fullWidth/>
                    </div>
                    <div  className='mdl-cell mdl-cell--6-col'>
                        <TextField
                            floatingLabelText="county"
                            defaultValue={this.state.form.venue4}
                            ref='venueinput4' fullWidth/>
                    </div>
                    <div className="mdl-cell mdl-cell--3-col">
                        <DatePicker
                            ref='startdate'
                            defaultDate={this.state.form.startdate}
                            autoOk={true}
                            floatingLabelText="Start Date" fullWidth/>
                    </div>
                    <div className="mdl-cell mdl-cell--3-col">
                        <TimePicker
                            ref='starttime'
                            defaultTime={this.state.form.starttime}
                            format="24hr"
                            autoOk={true}
                            floatingLabelText="Start Time" fullWidth/>
                    </div>
                    <div className="mdl-cell mdl-cell--3-col">
                        <DatePicker
                            ref='enddate'
                            defaultDate={this.state.form.enddate}
                            autoOk={true}
                            floatingLabelText="End Date"  fullWidth/>
                    </div>
                    <div  className="mdl-cell mdl-cell--3-col">
                        <TimePicker
                            ref='endtime'
                            defaultTime={this.state.form.endtime}
                            format="24hr"
                            autoOk={true}
                            floatingLabelText="End Time" fullWidth/>
                    </div>

                </div>
        );
    }
}
