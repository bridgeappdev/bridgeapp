var React = require('react');

module.exports = React.createClass({

	displayName: "ImageUploader",

	placeHolderImage: '/assets/img/placeholder.jpg',

	maxImageSize: 1048576,

	getInitialState: function() {
		'use strict';

		var imgUrl, message;
		if(this.props.initialValue){
			imgUrl = this.props.initialValue;
			message = null;
		}else{
			imgUrl = this.placeHolderImage;
			message = "";
		}

		return {
			data_uri: imgUrl,
			fileInputMessage: message
		};
	},

	isValid: function(){
		'use strict';

		//Add validation if needed.
		return true;

	},

	getValue: function(){
		'use strict';

		return JSON.parse(this.refs.urlinput.value).url;
	},
	// prevent form from submitting; we are going to capture the file contents
	handleSubmit: function(e) {
		e.preventDefault();
	},

	handleMessageDivClick: function(){
		'use strict';

		return this.refs.fileinput.click();

	},
	// when a file is passed to the input field, retrieve the contents as a
	// base64-encoded data URI and save it to the component's state
	handleFile: function(e) {
		var _self = this;
		var reader = new FileReader();
		var file = e.target.files[0];

		if(!file){
			return;
		}

		var imageType = /image.*/;
		if (!file.type.match(imageType)) {
			console.log("User file selection error: file selected is not a supported image-type");
			return this.setState({
				data_uri: this.placeHolderImage,
				fileInputMessage: "That is not an image file!"
			});
		}
		if(file.size > this.maxImageSize){
			console.log("User file selection error: file selected is too big");
			return this.setState({
				data_uri: this.placeHolderImage,
				fileInputMessage: "That image is too big! (Max 1MB)"
			});

		}

		reader.onload = function(upload) {
			var data_uri = upload.target.result;
			var img = document.createElement("img");
			img.src = data_uri;
			var w = parseInt(img.width);
			var h = parseInt(img.height);
			var body = {
				"upload_preset":"rqubjba2",
				"file" : data_uri,
				"folder": "russell"
			};
			var url = "https://api.cloudinary.com/v1_1/br1dg3co/image/upload",
			method = "POST",
			postData = JSON.stringify(body),
			async = true,
			request = new XMLHttpRequest(),
			_context = this;

			request.onload = function() {
				var data, div, status;
				status = request.status;
				if (status === 200) {
					_self.refs.urlinput.value = request.responseText;
					data = JSON.parse(request.responseText);
					_self.setState({
						data_uri: data.url,
						fileInputMessage:null
					});
				} else {
					data = request.responseText;
					console.log(status + " " + data);
				}
			};
			request.open(method, url, async);
			request.setRequestHeader("Content-Type", "application/json");
			return request.send(postData);

		}

		if(file){
			reader.readAsDataURL(file);
		}
	},

	render: function() {

		return (
			<div className='image-upload'>
				<form
					onSubmit={this.handleSubmit}
					encType="multipart/form-data"
				>
						<label htmlFor="file-input">
							<div className="center-cropped"
								style={{backgroundImage: "url('"+this.state.data_uri+"')"}}
							>
								<div className="image-upload-overlay">

									<span
										className="image-upload-overlay-text"
									>
										{this.props.overlayText}
									</span>

								</div>

							</div>
						</label>
						<input
							id="file-input"
							ref="fileinput"
							type="file"
							accept="image/*"
							onChange={this.handleFile}
						/>
						<input
							ref="urlinput"
							type="text"
							style={{'display':'none'}}
							defaultValue={ '{ "url": \"' + this.props.initialValue + '\"}'} />
				</form>

				<div
					className="image-upload-overlay-message-div"
					onClick={this.handleMessageDivClick}
				>
					{this.state.fileInputMessage}
				</div>

			</div>
		);
	},
});
