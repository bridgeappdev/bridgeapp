/* components/common/Input.js
* React components for messages.
* Props:
* {
* }
*/
"use strict";

import React from 'react';
import reactMixin from 'react-mixin';

import ChatMessage from '../../models/messages/ChatMessage';
import URLMessage from '../../models/messages/URLMessage';

import FontIcon   from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';

import EmojiPicker from './EmojiPicker';
import { Textfield } from 'react-mdl';
export default class MessageInput extends React.Component{
	constructor( props ){

		super( props );

		this.state = {
			inputValue: '',
			showEmojiPicker: true
		};

		this.toggleEmojiPicker = this.toggleEmojiPicker.bind(this);
		this._handleKeyDown = this._handleKeyDown.bind(this);
		this._handleTextChange = this._handleTextChange.bind(this);
		this.clearText = this.clearText.bind(this);
		this.addIcon = this.addIcon.bind(this);
		this.renderEmoji = this.renderEmoji.bind(this);

	}

	componentDidMount() {
		return this.toggleEmojiPicker();
	}


	toggleEmojiPicker() {
		const value = !this.state.showEmojiPicker;
		this.setState( { showEmojiPicker: value } );
	}

	_parseText( messageText ){
		const linkify = require('linkifyjs');

		const messageMetaObj = { 'message': messageText };
		const messageUrls = linkify.find( messageText, 'url' );
		if( messageUrls.length ){
			messageMetaObj.url = messageUrls[0].href;
		}

		return messageMetaObj;

	}

	_handleKeyDown( e ) {

		if ( e.which === 38 ) { //up arrow
			return console.log('Edit last message??');
		}

		if ( e.which === 13 ) {
			e.preventDefault();

			const messageText = e.target.value;
			if( messageText === "" ) { return void 0; }

			const messageData = {
				'meta': this._parseText( messageText )
			};

			const profile = this.props.userProfile;
			messageData.uId = profile.id;
			messageData.username = profile.username;

			if( profile.avatar ){
				messageData.avatar = profile.avatar;
			} else {
				messageData.avatar = "/assets/img/avatar/ph-avatar.png";
			}

			let message;
			switch (true) {
				case !!messageData.meta.url :
				message = new URLMessage( messageData );
				break;
				default:
				message = new ChatMessage( messageData );
			}


			return this.props.handleSend( message );

		}
	}

	_handleTextChange(e){

		return this.setState({inputValue:e.target.value});

	}

	clearText(){

		return this.setState( { inputValue: '' } );

	}

	addIcon( icon ){
		this.setState({
			inputValue : this.state.inputValue + icon
		});
		this.message_input.focus();
		this.toggleEmojiPicker();
	}

	renderEmoji(){
		if(this.state.showEmojiPicker){
			return (
				<EmojiPicker
					open={this.state.showEmojiPicker}
					addIcon={this.addIcon}>
				</EmojiPicker>
			);
		}
	}

	render() {

		let iconEmoji = 'fa fa-smile-o';
		if(this.state.showEmojiPicker){
			iconEmoji = 'fa fa-chevron-down';
		}
		return (
			<div>
				{this.renderEmoji()}
				<div className="new-panel-2-input-message">
					<IconButton
						className="emoticon"
						onClick = {this.props.dropzone}
					>
						<i className="material-icons">file_upload</i>
					</IconButton>
					<div className="input">

						<input
							type="text"
							ref={(ref) => this.message_input = ref}
							onKeyDown={this._handleKeyDown}
							onChange={this._handleTextChange}
							placeholder="Say something"
							style={{width: '100%'}}
							value={ this.state.inputValue }
							/>
					</div>
					<IconButton
						className="emoticon"
						onClick={this.toggleEmojiPicker}
						>
						<FontIcon className={iconEmoji} />
					</IconButton>
				</div>

			</div>
		);

	}

}
