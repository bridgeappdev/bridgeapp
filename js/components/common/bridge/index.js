/* /components/common/bridge/
 *
 * Common components used for bridges.
 *
 * */
import React from 'react';
import RadioButton from 'material-ui/RadioButton';
import RadioButtonGroup from 'material-ui/RadioButton-group';

const CommonBridge = module.exports = {};

CommonBridge.PrivacySwitch = React.createClass({
	displayName: 'PrivacySwitch',

	getValue: function () {
		'use strict';

		return this.refs.visibility.getSelectedValue();
	},

	setVisibiltyMessage: function () {
		'use strict';

		return;
	},

	render: function () {
		return (
			<RadioButtonGroup
				ref={'visibility'}
				name="bridgeVisibility"
				onChange={this.setVisibiltyMessage}
				defaultSelected="public"
			>
				<RadioButton
					value="public"
					label="Public"
					style={{ width: '25%', float: 'left' }}
				/>
				<RadioButton
					value="private"
					label="Private"
					style={{ width: '25%', float: 'left' }}
				/>
			</RadioButtonGroup>
		);
	},
});
