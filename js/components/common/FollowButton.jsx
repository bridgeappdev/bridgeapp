/*
* /lib/components/common/FollowButton.jsx
* React component for rendering a follow button for
* users depending on the current reltion between them
*
* Possible contact relationships are
*   following
*   follower
*   friend (mutual follow)
*   no relationship
*
*/

import React from 'react';

import RaisedButton from 'material-ui/RaisedButton';

module.exports = React.createClass({
	displayName: "FollowButton",

	_handleFollowClick( followType ){
		'use strict';

		return this.props.onClick(followType);

	},

	_getButton(){
		'use strict';

		const contacts = this.props.contactList;
		const profileId = this.props.profileId;
		let FollowButton;
		switch(true){
			case (!contacts):
			FollowButton = <RaisedButton
				label='Follow'
				onMouseUp={this._handleFollowClick.bind(this, 0)}
				/>
			break;
			case (contacts[profileId] === 1):
			FollowButton = <RaisedButton
				label='Follower'
				onMouseUp={this._handleFollowClick.bind(this, 1)}
				/>
			break;
			case (contacts[profileId] === 2):
			FollowButton = <RaisedButton
				label='Following'
				onMouseUp={this._handleFollowClick.bind(this, 2)}
				/>
			break;
			case (contacts[profileId] === 3):
			FollowButton = <RaisedButton
				label='Friend'
				onMouseUp={this._handleFollowClick.bind(this, 3)}
				/>
			break;
			default:
			FollowButton = <RaisedButton
				label='Follow'
				onMouseUp={this._handleFollowClick.bind(this, 0)}
				/>
		}

		return FollowButton;
	},

	render(){
		return (
			<div className='follow-button'>
				{this._getButton()}
			</div>
		)
	}
});
