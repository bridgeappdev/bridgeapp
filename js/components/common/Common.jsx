/* Utils.js
 * React components declaration for common components
 * */

/* jshint node: true*/


const React = require('react');
import {List, ListItem} from 'material-ui/List';

module.exports = {

	Logout:	React.createClass({

		handleClick: function (e) {
			'use strict';

			e.preventDefault();

			console.log('clicked on logout', e);
			const FireAPI = require('../../lib/FireAPI');
			FireAPI.logout();
		},

		render: function () {
			'use strict';

			return (

				<ListItem>
					<a href="#logout" title="logout" id="logout" onClick={this.handleClick}>sign out</a>
				</ListItem>

				);
		},
	}),

	NotFound: React.createClass({

		render: function () {
			'use strict';
			return (

				<h1>These are not the droids you are looking for!!!</h1>

				);
		},
	}),
};
