// jshint esnext:true
import React from 'react';
import emojiData from './emoji.js';
import {Tabs, Tab} from 'material-ui/Tabs';
import FontIcon from 'material-ui/FontIcon';

const iconStyles = {
  marginRight: 24,
};
export default class EmojiPicker extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount(){
		'use strict';
		$(".EmojiPicker").mCustomScrollbar({autoHideScrollbar:true,theme:"dark"});
		//$("#EmojiPicker").mCustomScrollbar("scrollTo","bottom");

	}

  render(){
    let emojisSmile =[];
    let emojisAnimals =[];
    let emojisSimbols = [];
    let emojisTransports = [];
    let emojisFood = [];
    let emojisOlympics  = [];
    var emoticonIndex=1;
    for (let key in emojiData.smiles) {
      emoticonIndex++;
      emojisSmile.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.smiles[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }
    for (let key in emojiData.animals) {
      emoticonIndex++;
      emojisAnimals.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.animals[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }
    for (let key in emojiData.simbols) {
      emoticonIndex++;
      emojisSimbols.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.simbols[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }
    for (let key in emojiData.transports) {
      emoticonIndex++;
      emojisTransports.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.transports[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }
    for (let key in emojiData.food) {
      emoticonIndex++;
      emojisFood.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.food[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }
    for (let key in emojiData.olympics) {
      emoticonIndex++;
      emojisOlympics.push(
        <a className="EmojiIcon" key={'emoji-icon-'+emoticonIndex}>
          <img  width="20px" height="20px" src={emojiData.olympics[key]} onClick={this.props.addIcon.bind(null,key)}/>
        </a>
      );

    }

    let styleEmoji = {
      maxHeight:'153px',
      overflow:'auto',
      backgroundColor:'#F6F6F6',
      textAlign:'center'
    };
    let styleTab = {
      backgroundColor:'#F6F6F6',
      color:'#000000'
    };
    let colorIcon = "#616161";
    return (
      <Tabs>
        <Tab icon={<FontIcon className="fa fa-smile-o" color={colorIcon}></FontIcon>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisSmile}
        </div>
        </Tab>
        <Tab  icon={<FontIcon className="fa fa-paw" color={colorIcon}/>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisAnimals}
        </div>
        </Tab>
        <Tab  icon={<FontIcon className="fa fa-smile-o" color={colorIcon}/>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisSimbols}
        </div>
        </Tab>
        <Tab icon={<FontIcon className="fa fa-futbol-o" color={colorIcon}/>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisOlympics}
        </div>
        </Tab>
        <Tab icon={<FontIcon className="fa fa-smile-o" color={colorIcon}/>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisFood}
        </div>
        </Tab>
        <Tab icon={<FontIcon className="fa fa-automobile" color={colorIcon}/>} style={styleTab}>
        <div className="EmojiPicker" style={styleEmoji}>
          {emojisTransports}
        </div>
        </Tab>
      </Tabs>

    );
  }
}
