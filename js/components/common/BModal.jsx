import React from 'react';
import FontIcon from 'material-ui/FontIcon';

const BModal = React.createClass({

	displayName: 'BModal',

	ESC: 27,

	renderChildren() {
		let childProps = {
			userProfile: this.props.userProfile,
			closeBModal: this.props.closeBModal
		};
		return React.Children.map(this.props.content, function (child) {
			return React.cloneElement(child, childProps);

		}.bind(this));
	},

	componentDidMount(){

		//React will bind this context to our component function
		document.onkeydown = this._handleKeyDown;
	},

	_handleKeyDown(e){

		if(e.keyCode === this.ESC){
			this.props.closeBModal(null);
		};

	},

	_handleClickClose(){
		return this.props.closeBModal(null);
	},

	render() {

		if(!this.props.content){
			return null;
		}

		return (

			<div
				id='bmodal'
				onKeyDown={this._handleKeyDown}
				>
				<div
					id='left-side-column'
					/>
				<div className='bmodal-content'>
					<div className="close-bmodal">
						<FontIcon
							className='fa fa-times'
							style={{cursor: 'pointer', color:'rgba(0, 0, 0, 0.4)'}}
							hoverColor='black'
							onClick={this._handleClickClose}
							/>
					</div>
					{this.renderChildren()}
				</div>
				<div
					id='right-side-column'
					>

				</div>
			</div>

		);
	}
});

export default BModal;
