import React from 'react';
import FontIcon  from 'material-ui/FontIcon';

const Header = React.createClass({
  displayName: 'Header',

  render(){
    'use strict';

    return(
      <div
        className='panel4-header'
      >

        <div className='panel4-title'>
          {this.props.titleText}
        </div>

        <div className='panel4-close'>

          <FontIcon
            className='fa fa-times'
            style={{cursor: 'pointer', color:'rgba(0, 0, 0, 0.4)'}}
            hoverColor='black'
            onClick={this.props.requestClose}
          />

        </div>
      </div>
    );
  }
});

export default Header;
