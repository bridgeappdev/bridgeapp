/* components/common/Input.js
* React components for messages.
* Props:
* {
* }
*/
"use strict";

import React    from 'react';

import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import TextField from 'material-ui/TextField';

import EmojiPicker from './EmojiPicker';

module.exports = React.createClass ({

	displayName: "MessageInput",

	getInitialState: function(){

		return {
			inputValue: '',
			showEmojiPicker: true
		};

	},

	componentDidMount: function() {
		return this.toggleEmojiPicker();
	},


	toggleEmojiPicker: function(e) {
		var value = !this.state.showEmojiPicker;
		this.setState({showEmojiPicker: value});
	},

	_handleKeyDown: function pressed( e ) {
		return this.props.handleKeyDown( e, this );
	},

	_handleTextChange: function(e){

		return this.setState({inputValue:e.target.value});

	},

	addIcon:function(icon){
		this.setState({inputValue:this.state.inputValue+icon});
		this.message_input.focus();
	},

	renderEmoji:function(){
		if(this.state.showEmojiPicker){
			return (
			<EmojiPicker
				open={this.state.showEmojiPicker}
				addIcon={this.addIcon}>
			</EmojiPicker>
		);
		}
	},

	render: function() {

		var iconEmoji = 'fa fa-smile-o';
		if(this.state.showEmojiPicker){
			iconEmoji = 'fa fa-chevron-down';
		}
		return (
			<div>
				{this.renderEmoji()}
				<div className="input-message">
					<TextField
						className="input-message-input"
						ref={(ref) => this.message_input = ref}
						hintText="Say something"
						value={this.state.inputValue}
						multiLine={true}
						fullWidth={true}
						onKeyUp={this._handleKeyDown}
						onChange={this._handleTextChange}		/>
					<IconButton
						className="input-message-emoticon"
						onClick={this.toggleEmojiPicker}
					>
						<FontIcon className={iconEmoji} />
					</IconButton>
				</div>

			</div>
		);

	}

});
