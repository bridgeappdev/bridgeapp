import  React from 'react';
import SuggestionItem from './SuggestionItem';
export default class SuggestionPanel extends React.Component{
	constructor(){
		super();
	}
	render(){
		var tagSuggestionList = this.props.tagSuggestionList,
		listLength = tagSuggestionList.length,
		tagSuggestionElements = [];

		for(var i=0;i<listLength;i++){
			let name = tagSuggestionList[i].name;
			tagSuggestionElements.push(<SuggestionItem
				onSuggestionClick={this.props.onSuggestionClick}
				key={name}
				tagName={name}
				data={tagSuggestionList[i]} />);
			}

			return(
				<div className="suggestionPanel  mdl-shadow--2dp">
					{tagSuggestionElements}
					<br className="clear" />
				</div>
			);
		}
	}
