import React from 'react';
export default class SuggestionItem extends React.Component{
    constructor(){
        super();
    }
	_handleClick(){
		return this.props.onSuggestionClick(this.props.tagName);
	}
	render(){
		return(
			<div
				ref='suggestionbox'
				className='suggestionItem tag-suggestion-box'
				onClick={this._handleClick.bind(this)}
   >
                <div className="tag-name">{this.props.tagName}</div>
                <div className="tag-description">{this.props.data.desc}</div>
                <div className="tag-count">{this.props.data.count}</div>

                <div className="ellipsis"></div>
			</div>
		);

	}
}
