/*TagSuggestions/TagSuggestions.jsx
*
* Writeahead component for tag suggestion
*
* */
'use strict';

import React from 'react';
import TagEditor from './TagEditor';
import SuggestionPanel from './SuggestionPanel';
import TagSuggestionsUnderline from './TagSuggestionsUnderline';

export default class TagSuggestInput extends React.Component {

	constructor( props ){
		super( props );

		this.state = {
			width: 0,
			inputValue: '',
			tagList: props.initialValue?props.initialValue:[],
			tagSuggestions: false,
			tagSuggestionList: [],
			hasFocus:false,
			tagsErrorText: null
		}

		this.clearSuggestions = this.clearSuggestions.bind( this );
		this._filterSuggestions = this._filterSuggestions.bind( this );
		this._handleTagOnKeyUp = this._handleTagOnKeyUp.bind( this );
		this._handleDeleteTag = this._handleDeleteTag.bind( this );
		this._handleSuggestionClick = this._handleSuggestionClick.bind( this );
		this._handleFocus = this._handleFocus.bind( this );
		this._handleBlur = this._handleBlur.bind( this );

	}

	componentDidMount(){

		var containerWidth = this.refs.tagSuggestContainer.clientWidth;
		return this.setState({width:containerWidth});

	}

	clearSuggestions(){

		this.setState({
			tagSuggestions: false,
			tagSuggestionList: []
		});

	}

	_filterSuggestions( suggestionList ){

		var _self = this;

		return suggestionList.filter(function (tag){
			return !(_self.state.tagList.indexOf(tag.name.toLowerCase()) > -1);
		});
	}

	_handleTagOnKeyUp( value ){

		var _self = this,
		inputValue = value;

		var tagText = inputValue.split(' ').pop();
		if(tagText.length === 0) {
			this.clearSuggestions();
			return this.setState({ inputValue: inputValue });
		}

		this.props.tagSuggest(tagText, function(error, result){
			if(error){
				console.log('Tag suggest didn\'t work', error);
			}else{
				result = _self._filterSuggestions(result);
				if(result.length > 0){
					return _self.setState({ inputValue: inputValue, tagSuggestions: true, tagSuggestionList: result });
				}else{
					_self.clearSuggestions();
					return _self.setState({ inputValue: inputValue });
				}
			}
		});

	}

	_handleDeleteTag(tagName){

		var tags = this.state.tagList;
		if(!tagName){
			tags.pop();
		}else{
			let index = tags.indexOf(tagName);
			if (index > -1) {
				tags.splice(index, 1);
			}
		}

		this.setState({tagList:tags});

		if(typeof this.props.onRemoveTag === 'function'){
			return this.props.onRemoveTag(tagName);
		}
	}

	_handleSuggestionClick(tagName){
		debugger;
		if(!tagName){ return void 0; }

		var tags = this.state.tagList;
		tags.push(tagName);

		this.setState({ tagList: tags,inputValue:'' });


		this.clearSuggestions();
		this.refs.tagEditor.clear();
		if(typeof this.props.onAddTag === 'function'){
			return this.props.onAddTag(tagName);
		}
	}

	_handleFocus(e){

		return this.setState({hasFocus:true});
	}

	_handleBlur(e){

		return this.setState({hasFocus:false});
	}

	render(){

		var tagSuggestions = (this.state.tagSuggestions ?
			<SuggestionPanel
				onSuggestionClick={this._handleSuggestionClick}
				tagSuggestionList={this.state.tagSuggestionList} /> :
				""),
				tagEditor = <TagEditor
					ref='tagEditor'
					
					width={this.state.width}
					tags={this.state.tagList}
					onKeyUp={this._handleTagOnKeyUp}
					deleteTag={this._handleDeleteTag}
					containerDiv={this.refs.tagSuggestContainer}	/>
				;

				const tagSuggestionStyle = this.props.tagSuggestionStyle ?
				this.props.tagSuggestionStyle	: {width:'100%', position:'relative'};
				return (

					<div
						style={tagSuggestionStyle}
						onBlur={this._handleBlur}
						onFocus={this._handleFocus}
					ref='tagSuggestContainer' >
						{tagEditor}
						{tagSuggestions}
						<TagSuggestionsUnderline
							underlineColorStyle={this.props.underlineColorStyle}
							hasFocus={this.state.hasFocus} />
					</div>

				);
			}

			getValue(){

				return this.state.tagList;
			}

		}
