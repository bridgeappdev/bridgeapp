/*TagSuggestions/TagSuggestionsUnderline.jsx
*
* Component to add underline effect to tag suggestion input
*
* */

import React from 'react';

export default class TagSuggestionsUnderline extends React.Component{
	constructor( props ){
		super( props );
	}

	render (){

		const borderColorStyle	= this.props.underlineColorStyle ?
		this.props.underlineColorStyle : 'rgb(0, 188, 212)',
		transformStyle 		= this.props.hasFocus ?
		'scaleX(1)' : 'scaleX(0)';

		return (

			<div>
				<hr
					style={{
						border:"none",
						borderBottom:"solid 1px",
						borderColor:'#e0e0e0',
						bottom:"8px",
						boxSizing:"content-box",
						margin:"0",
						width:"100%"}}/>
					<hr
						ref='inputunderline'
						style={{
							borderStyle: "none none solid",
							borderBottomWidth: "2px",
							borderColor: borderColorStyle,
							bottom: "8px",
							boxSizing: "content-box",
							margin: "0px",
							width: "100%",
							transform: transformStyle,
							transition: "all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms"}}/>
					</div>

				);
			}
		}
