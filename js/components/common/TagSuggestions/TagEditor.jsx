import React from 'react';
import ReactDOM from 'react-dom';
import TextField from 'material-ui/TextField';
import Chip from 'material-ui/Chip';

export default class TagEditor extends React.Component{
	constructor(){
		super();
		this.state =  {
			tag:''
		};
	}

	clear(){
		this.setState({tag:''});
		return this.tagEditorInput.focus();

	}

	_handleKeyUp(e){
		let self = this;
		let value = e.target.value;
		this.setState({tag:value},()=>{
			return self.props.onKeyUp(value);
		});
	}

	_handleKeyDown(e){
		if(e.keyCode === 8){
			if(e.target.selectionStart === 0){
				let tagsList = this.props.tags;
				let listLength = tagsList.length;
				if (listLength > 0){
					return this.props.deleteTag( tagsList[ listLength - 1 ] );
				}
			}
		}
	}

	_handleDeleteTag(tagName){

		this.props.deleteTag(tagName);
		return this.tagEditorInput.focus();
	}

	render(){

		var _self = this,
				tagChips = [],
				tagList = this.props.tags,
				floatingLabelText = tagList.length > 0 ? "" : "ADD SOME TAGS",
				inputElemWidth = 100;

			const styles = {
			  chip: {
			    margin: 4,
				height:'32px'
			  },
			  wrapper: {
				display:'flex',
			    flexWrap: 'wrap',
			  },
			};
			tagList.forEach(function(tag, index){
				tagChips.push(<Chip key={tag} style={styles.chip} onRequestDelete={() => _self._handleDeleteTag(tag)}>{tag}</Chip>);

			});


		return (
			<div className="containerChips">
				{tagChips}
			 	<TextField
					value = {this.state.tag}
					fullWidth
					ref={(ref) => this.tagEditorInput = ref}
					type='text'
					style={{flex:1}}
					underlineShow={false}
					floatingLabelText={floatingLabelText}
					onChange={this._handleKeyUp.bind(this)}
					onKeyDown={this._handleKeyDown.bind(this)} />
			</div>
		);
	}
}
