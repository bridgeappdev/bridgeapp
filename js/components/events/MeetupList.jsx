// Modules
import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import _ from 'lodash';

import Actions       from '../../actions/MeetupActions';
import meetupStore    from '../../stores/MeetupStore';
import EventAPI from '../../models/events/Meetups';

import OpenBridgeButton from './openEventBridge';
import RaisedButton from 'material-ui/RaisedButton';

import ImportEvent from './ImportEvent';
const Meetup = require('../../../config').Meetup;

export default class MeetupList extends React.Component{

	constructor( props ){

		super( props );

		this.state = {
			events: [],
			open: false,
			loading: false
		}

		this.openEventImportDialog = this.openEventImportDialog.bind( this );
		this.closeEventImportDialog = this.closeEventImportDialog.bind( this );

	}

	componentWillMount() {
		this.listenTo(
			meetupStore,
			this.onChangeFromEventStore
		);
		let muauth = this.props.userProfile.muauth;
		let medata = this.props.userProfile.mumedata;
		if( muauth ){
			this._muauth = muauth;
			if( medata ){
				this._medata = medata;
				return this._getUserEvents( muauth, medata );
			}else{
				return this._getMeData( muauth );
			}
		}
	}

	componentWillReceiveProps( nextProps ){

		let muauth = nextProps.userProfile.muauth;
		let medata = nextProps.userProfile.mumedata;
		if(muauth !== this._muauth){
			this._muauth = muauth;
			return this._getMeData(muauth);
		}
		if(medata !== this._medata){
			if( muauth ){
				this._medata = medata;
				return this._getUserEvents( muauth, medata );
			}
		}
	}

	onChangeFromEventStore(event, newData) {
		switch(event) {
			case 'getUserEvents':
			this.setState({
				'events': newData
			})
			break;
		}
	}

	_getUserEvents ( muauth, medata ){

		return this.setState(
			{'loading': true},
			EventAPI.getUserEvents( muauth, medata.id, (err) => {
				this.setState({'apiError': err, 'loading': false});
			})
		);
	}

	_getMeData( muauth ){

		return this.setState(
			{'loading': true},
			EventAPI.getMeData (
				muauth,
				this.props.userProfile,
				( err ) => {
					if( err ){
						this.setState({'apiError': err, 'loading': false});
					}
				}
			)
		)
	}

	openEventImportDialog(e){

		let eventData = _.filter(this.state.events, { 'id': e.currentTarget.value});
		eventData[0].eventSrc = 'meetup';
		return this.setState({'eventDataForImport': eventData[0], 'open':true});

	}

	closeEventImportDialog(){

		return this.setState({eventDataForImport: null, open:false});

	}

	renderEvents() {
		const url = "https://secure.meetup.com/oauth2/authorize"
		+ "?response_type=token"
		+ "&redirect_uri=http%3A%2F%2Fwww.local.bridgeapp.me%2Fmuauth"
		+ "&client_id="
		+ Meetup.key;

		if(this.state.apiError){
			return (
				<li className="list-group-item" key={"authorize"}>
					<h3>Opps!</h3>
					<div>
						<p>Something weird happened while trying to get your events.</p>
						<p>This is the message:
							<em>{this.state.apiError}</em>.
							</p>
							<p>You may need to reload the page or allow authorization again:</p>
						</div>
						<RaisedButton
							linkButton={true}
							href={url}
							target="_blank"
							secondary={true}
							label="Authorize Meetup"
							/>
					</li>
				)
		} else if (!this.props.userProfile.muauth) {
			return (
				<li className="list-group-item" key={"authorize"}>
					<h3>Please authorize Meetup to use Bridgeapp</h3>
					<div><p>Click the button below to go to the Meetup authorization page.</p></div>
					<RaisedButton
						linkButton={true}
						href={url}
						target="_blank"
						secondary={true}
						label="Authorize Meetup"
						/>
				</li>
			)
		} else if(this.state.events.length === 0){
			return(
				<li className="list-group-item" key={'empty'} >
					<h3>No events found to import</h3>
					<div><img></img></div>
					<div>We didn't find any upcoming events from your meetup groups...</div>
				</li>
			)
		}else{
			return this.state.events.map(function( eventObj ) {
				const event = eventObj.data;

				return (
					<li className="list-group-item" key={event.id} >
						{ eventObj.hasBridge?
							<OpenBridgeButton
								id={ eventObj.hasBridge }
								close={this.props.closeBModal}
							/>
							:<RaisedButton
								primary={true}
								onTouchTap={this.openEventImportDialog}
								value={event.id}
								label="Create Bridge"
							/>
						}
						<h3>{ event.name }</h3>
						<div dangerouslySetInnerHTML={{__html: event.description}}></div>
					</li>
				)
			}.bind(this))
		}
	}

	render() {
		if (!this.state.loading) {
			return (
				<div id="user-events">
					<h3><i className="fa fa-calendar"></i> Meetups</h3>
					<hr/>
					<ul className="list-group">
						{this.renderEvents()}
					</ul>
					<ImportEvent
						open={this.state.open}
						profile={this.props.userProfile}
						meetupAuth={this.state.auth}
						eventData={this.state.eventDataForImport}
						closeDialog={this.closeEventImportDialog}
						/>
				</div>
			)
		} else {
			return (
				<i className="loading fa fa-cog fa-spin"></i>
			)
		}
	}
}

reactMixin(MeetupList.prototype, Reflux.ListenerMixin);
