'use strict';

import React from 'react';
import history from '../../HistoryContainer';

function onClickOpenBridge( e ){
	e.preventDefault();

	history.pushState({}, '/bridge/' + this.id.replace(',', '/') );
	return this.close();

}

export default ( props ) => {
	return (
		<div className='event-import-open-bridge'>
			click <a onClick={ onClickOpenBridge.bind( props ) } >here</a> to open bridge
		</div>
	);
}
