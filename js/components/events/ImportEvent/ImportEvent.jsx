/* components/panel3/ImportEvent/ImportEvent.jsx
*
* React component for importing a new bridge from eventbrite.
*
* This files is written using es6 syntax and currently compiled
* using babel
* */

import React from 'react';
import bridgesAPI from '../../../models/bridges/Bridges';

import EventBridgeForm from '../../bridge/EventBridgeForm';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';

module.exports = React.createClass({

	displayName: "ImportEvent",

	checkBridgeNameAvailable: function(bridgeName, next){
		'use strict';

		return bridgesAPI.checkNameAvailable(
			this.props.profile.username,
			bridgeName,
			next
		);

	},

	createNewBridge: function(bridgeData){
		'use strict';

		if(!bridgeData){
			return console.log('BUILDBRIDGES::createNewBridge ==> Why is there no data?');
		}

		var _self = this,
		myUsername = this.props.profile.username,
		myUId = this.props.profile.id,
		eventData = this.props.eventData.data;

		bridgeData.members = {};
		bridgeData.members[myUId] = true;
		switch(eventData.eventSrc){
			case 'eventbrite':
			bridgeData.ebdata = {id : eventData.id};
			bridgeData.ebdata.url = eventData.url;
			bridgeData.ebdata.organizer = eventData.organizer;
			break;
			case 'meetup':
			bridgeData.mudata = {id : eventData.id};
			bridgeData.mudata.url = eventData.event_url;
			if(eventData.event_hosts){
				bridgeData.mudata.organizer = eventData.event_hosts;
			}
			break;
		}

		console.log('IMPORTEVENT::createNewBridge ==> ', bridgeData);
		bridgesAPI.saveNewBridge(
			this.props.profile,
			bridgeData,
			function ( error ){
				if( error ){
					console.log('There was a problem saving the bridge', e);
				}else{
					//TODO: Callback for error handling
					_self.props.eventData._hasBridge = myUsername + ',' + bridgeData.name;
					return _self.closeEventImportDialog();
				}
				return false;
			}
		);

	},

	getDialog: function(){
		'use strict';

		const eventData = this.props.eventData
		? this.props.eventData.data
		: null;
		if(eventData){
			eventData.auth = this.props.eventBriteAuth;
		}

		const actions = [
			<FlatButton
				label="Cancel"
				secondary={true}
				onTouchTap={this.closeEventImportDialog} />
		];
		const dialogContent = <EventBridgeForm
			key={'eventimport'}
			eventData={eventData}
			checkBridgeNameAvailable={this.checkBridgeNameAvailable}
			handleSubmit={this.createNewBridge}/> ;

		const titleStyle={
			textAlign: 'center',
		}
		const dialogContentStyle={
			width: '65%',
			maxWidth: 'none',
			transform: 'translate3d(0px, 0px, 0px)'
		}
		const dialogBodyStyle={
			paddingTop: 0,
			maxHeight: 'none',
			overflow: 'auto'
		}

		return (
			<Dialog
				ref='eventimportdialog'
				title={'Import event ' + (eventData?eventData.name.text:'')}
				style={{zIndex: 30000}}

				contentStyle={dialogContentStyle}
				actions={actions}
				modal={true}
				open={this.props.open}
				onRequestClose={this.closeEventImportDialog}
				children={dialogContent}/>
		);
	},

	closeEventImportDialog : function(){
		"use strict";

		this.props.closeDialog();
	},

	render: function(){
		'use strict';

		return (
			<div>
				{this.getDialog()}
			</div>
		);
	}

});
