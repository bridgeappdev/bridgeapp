// Modules
import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';
import Dialog from 'material-ui/Dialog';
import {IconButton} from 'react-mdl';

import _ from 'lodash';

import Actions         from '../../actions/EventBriteActions';
import eventBriteStore from '../../stores/EventBriteStore';
import EBEvent         from '../../models/events/EBEvent';
import EventAPI        from '../../models/events/Events';

import RaisedButton from 'material-ui/RaisedButton';
import OpenBridgeButton from './openEventBridge';

import ImportEvent from './ImportEvent';
var EventBrite = require('../../../config').EventBrite;
import PanelBuildBridgeSteps from '../panel1/BuildBridge/buildBridgeSteps';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import moment from 'moment';
export default class EventBriteList extends React.Component{

	constructor( props ){
		super( props );

		this.state = {
			events: [],
			apiError: false,
			open: false,
			loading: false
		}

		this.openEventImportDialog = this.openEventImportDialog.bind( this );
		this.closeEventImportDialog = this.closeEventImportDialog.bind( this );
	}

	componentWillMount() {
		this.listenTo(
			eventBriteStore,
			this.onChangeFromEventStore,
			this.initEventStoreListener
		)

		const profile = this.props.userProfile;

		if( profile.ebauth ){
			this._ebAuth = profile.ebauth;
			return this._getUserEvents(
				 profile.ebauth
			);
		}
	}

	componentWillReceiveProps( nextProps ){
		if( nextProps.userProfile.ebauth !== this._ebAuth ){
			this._ebAuth = nextProps.userProfile.ebauth;
			return this._getUserEvents(
				nextProps.userProfile.ebauth
			);
		}
	}

	initEventStoreListener ( data ){

		this.setState( { events: data.ebevents } );

	}

	onChangeFromEventStore( event, newData ) {

		this.setState({
			events: newData
		});

	}

	_getUserEvents( ebauth ){

		return this.setState(
			{'loading': true},
			EventAPI.getUserEvents( ebauth, (err) => {
				//err is null is there is no error
				this.setState({'apiError': err, 'loading': false});
			})
		);
	}

	_handleClickAuthorize() {
		return EventBriteAPI.auth();
	}

	openEventImportDialog( e ){

		const eventData = _.filter(this.state.events, { 'id': e.currentTarget.value});
		eventData[0].data.eventSrc = 'eventbrite';
		//return this.setState({'eventDataForImport': eventData[0], 'open':true});
		console.log(eventData[0].data)
		let data = this.createEventData(eventData[0].data);
		console.log(data);
		this.refs.PanelBuildBridgeSteps._open('EVENT',data);


	}
	createEventData(eventData){
		let bridgeData={};
		let myUId = this.props.userProfile.id;
		let myUsername = this.props.userProfile.username;
		bridgeData.title= eventData.name.text;
		bridgeData.name= eventData.name.text.replace(/\W+/g, "").toLowerCase();
		bridgeData.desc = eventData.description.text;
		bridgeData.venue1 = eventData.venue.address.address_1;
		bridgeData.venue2 = eventData.venue.address.address_2;
		bridgeData.venue3 = eventData.venue.address.city;
		bridgeData.venue4 = eventData.venue.address.country;
		bridgeData.members = {};
		bridgeData.members[myUId] = true;
		bridgeData.ebdata = {id : eventData.id};
		bridgeData.ebdata.url = eventData.url;
		bridgeData.ebdata.organizer = eventData.organizer;
		bridgeData.startdate = new Date(eventData.start.utc);
		bridgeData.starttime = new Date(eventData.start.utc);
		bridgeData.enddate = new Date(eventData.end.utc);
		bridgeData.endtime = new Date(eventData.end.utc);
		bridgeData.img_url = eventData.logo.url;
		return bridgeData;
	}
	/*createNewBridge: (bridgeData){
		if(!bridgeData){
			return console.log('BUILDBRIDGES::createNewBridge ==> Why is there no data?');
		}

		var _self = this,
		myUsername = this.props.profile.username,
		myUId = this.props.profile.id,
		eventData = this.props.eventData.data;

		bridgeData.members = {};
		bridgeData.members[myUId] = true;
		bridgeData.ebdata = {id : eventData.id};
		bridgeData.ebdata.url = eventData.url;
		bridgeData.ebdata.organizer = eventData.organizer;
		return

	}*/

	closeEventImportDialog(){

		return this.setState({'eventDataForImport': null, 'open':false});

	}


	componentDidMount() {
	 $('.item-event').mCustomScrollbar({
			autoHideScrollbar: true,
			theme:"dark"
		});
	}

	renderEvents() {
		const url = "https://www.eventbrite.com/oauth/authorize?response_type=token&client_id=" + EventBrite.key;
		if( this.state.apiError ){
			return (
				<li className="list-group-item" key={"authorize"}>
					<h3>Opps!</h3>
					<div>
						<p>Something weird happened while trying to get your events.</p>
						<p>This is the message:
							<em>{this.state.apiError}</em>.
							</p>
							<p>You may need to reload the page or allow authorization again:</p>
						</div>
						<RaisedButton
							linkButton={true}
							href={url} target="_blank"
							secondary={true}
							label="Authorize EventBrite"
							/>
					</li>
				)
			} else if ( !this.props.userProfile.ebauth ) {
				return (
					<li className="list-group-item" key={"authorize"}>
						<h3>Please authorize EventBrite to use Bridgeapp</h3>
						<div><p>Click the button below to go to the EventBrite authorization page.</p></div>
						<RaisedButton
							linkButton={true}
							href={url} target="_blank"
							secondary={true}
							label="Authorize EventBrite"
							/>
					</li>
				)
			} else {
				return (
					this.state.events.map(function( eventObj ) {

						const event = eventObj.data;
						console.log(eventObj);
						return (
							<div className="mdl-cell mdl-cell--6-col" key={event.id} >
							<Card expanded={this.state.expanded} onExpandChange={this.handleExpandChange}>
							        <CardHeader
							          title={event.name.text }
							          subtitle={event.venue.address.address_1+', '+event.venue.address.city}
							          avatar={event.logo.url}
							          actAsExpander={true}
							          showExpandableButton={true}
							        />


							        <CardText expandable={true}>
							          <div className="item-event mCustomScrollbar" style={{maxHeight:'240px',overflowY:'auto'}} dangerouslySetInnerHTML={{__html: event.description.html}}></div>
							        </CardText>
							        <CardActions>
							         	{
							          	(eventObj.hasBridge)?

							          		<OpenBridgeButton	id={ eventObj.hasBridge } close={this._close.bind(this)} />
							          		:
									          <FlatButton label="Create Bridge" value={event.id} onTouchTap={this.openEventImportDialog} />

									}
							        </CardActions>
						      </Card>

							</div>
						)
					}.bind(this))
				)
			}
		}
		_open(){
			this.setState({open:true});
		}
		_close(){
			this.setState({open:false});
		}
		render() {
				if(this.state.open){
					return  (
						<Dialog modal={true} open={this.state.open} className="eventbrite--list">
	<div className="mdl-cell mdl-cell--12-col newBridge--form-head-form">

	                    <IconButton name="close" style={{float:'right'}} onClick={this._close.bind(this)}/>
	                    </div>
								<h5><i className="fa fa-calendar"></i> Events</h5>
								<hr/>
								<div className="mdl-grid" style={{height:'400px',overflowY:'auto'}}>
									{this.renderEvents()}
								</div>
								<PanelBuildBridgeSteps ref="PanelBuildBridgeSteps" profile = {this.props.userProfile}/>


						</Dialog>
					);
				} else {
					return null;
				}

		}
}

reactMixin(EventBriteList.prototype, Reflux.ListenerMixin);
