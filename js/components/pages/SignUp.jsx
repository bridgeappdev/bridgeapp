/* Login.js
* React components for login UI.
*/
'use strict';

import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import Auth from '../../models/auth/Auth';

import profileStore from '../../stores/ProfileStore';
import authStore from '../../stores/AuthStore';
import AuthActions from '../../actions/AuthActions';

import validation from '../../lib/FormValidations';

import { Tooltip } from 'react-mdl';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class SignUp extends React.Component {
	constructor ( props ) {

		super( props );

		this.state = {
			messages: {
				statusMessage: null,
				emailMessage: null,
				passwordMessage: null,
			},
			validForm: {
				validEmail: false,
				validPassword: false,
			},
			hasFormBeenSubmitted: false,
		};

		this.handleSignUpClick = this.handleSignUpClick.bind( this );
		this.signupRedirect = this.signupRedirect.bind( this );
	}

	componentDidMount() {

		this.listenTo(
			profileStore,
			this.onChange
		);

		//document.getElementById('submit').disabled = true;
	}

	onChange( eventName, data ) {

		if ( eventName === 'profileLoaded' ) {
			this.signupRedirect( data );
		}
	}

	signupRedirect( profile ) {

		const location = this.props.location;
		const nextState = {
			'profile': profile
		};

		if (location.state && location.state.nextPathName) {
			nextState.nextPathName =
				this.props.location.state.nextPathName;
		}

		return this.props.history.replaceState(
			nextState,
			'/welcome'
		);
	}

	_getErrorMessage( err ){
		let message;

		switch ( err.code ) {
			case "EMAIL_TAKEN":
			message = err.message;
				break;
			default:
			message = "There was an error signing up. Please try again.";

		}

		return message;
	}

	handleSignUpClick( e ) {

		e.preventDefault();

		this.setState({ hasFormBeenSubmitted: true });
		const email = e.target[0].value;
		const	password = e.target[1].value;
		const	_self = this;

		const submitButton = document.getElementById('submit');
		submitButton.innerHTML = "Signing up...";
		submitButton.disabled = true;

		Auth.signup(email, password, function (err, authData) {
			if (err) {
				const errorMessage = _self._getErrorMessage( err );
				_self.setState({ errorMessage: errorMessage });

			} else {

				Auth.login(email, password, function (err, authData) {
					if (err) {
						console.log('Something went wrong', err);
						const errorMessage = _self._getErrorMessage( err );
						_self.setState({ errorMessage: errorMessage });
					}
				});
			}
		});
	}

	handleLoginClick(e) {

		e.preventDefault();
		this.props.history.pushState(
			this.props.location.state,
			'/login'
		);
	}

	handleEmailOnBlur( e ) {

		const messages = this.state.messages;
		const validEmail = this.state.validForm;

		if (validation.isCandidateEmailValidEmail(e.target.value)) {
			messages.emailMessage = null;
		} else {
			messages.emailMessage = 'Not a valid email address. Try again';
		}
		this.setState({ messages: emailMessage });
	}

	render(){

		return (
			<MuiThemeProvider >
				<div className='login-page'>
					<nav className='topbar-nav' >
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
					</nav>
					<div className="login-bg">
						<div className="login-container">
							<img
								className="login-logo"
								src="/img/chakachat-logo_smll.png"
							/>
							<form
								className='login-form'
								autoComplete="turnitoff"
								ref="signupform"
								id="signupform"
								onSubmit={ this.handleSignUpClick.bind( this ) }
							>
								<div
									className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
									style={ { width: "100%" } }
								>
									<input
										className='login-email-input mdl-textfield__input'
										type='text'
										placeholder="Email"
										title='Enter your Email'
										id='email'
										onBlur={ this.handleEmailOnBlur.bind( this) }
									/>
									<input
										className='login-password-input mdl-textfield__input'
										title='Enter your Password'
										placeholder="Password"
										type='password'
										id='password'
									/>

								</div>
								<button
									id="submit"
									className="
										login-button mdl-button mdl-js-button
										mdl-button--raised mdl-js-ripple-effect
										mdl-color-text--white
									"
								>
									Sign Up
								</button>
								<p className='login-error'>{this.state.errorMessage}</p>
								<div className="signup">
									<span>Already have and account? </span>
									<a
										className="signup-anchor"
										href="#"
										onClick={ this.handleLoginClick.bind( this ) }
									>
										Sign in here
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</MuiThemeProvider >
		);
	}
}

reactMixin(SignUp.prototype, Reflux.ListenerMixin);
