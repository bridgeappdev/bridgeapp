/* BridgeCard.jsx
 *
 * UI Card component for listing bridge data returned from search.
 */

import React from 'react';
import history from '../../../HistoryContainer';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';

const BridgeCard = React.createClass({
  displayName: 'BridgeCard',

  _handleClickCard: function(e, bridgePath) {
    'use strict';

    e.preventDefault();

    // return this.props.history.replaceState(
    // 	{ "open": true },
    // 	'/bridge/' + this.props.data.path
    // );
    return history.push({
      state: {open: true},
      'pathname': '/bridge/' + this.props.data.path
    });

  },

  render: function() {
    'use strict';

    var noOfMembers = '00',
      bridgeData = this.props.data;

    if (bridgeData.members) {
      noOfMembers = Object.keys(bridgeData.members).length;
    }
    let noOfMessages;
    if (bridgeData.messagesMeta) {
      noOfMessages = bridgeData.messagesMeta.count;
    }else {
      noOfMessages = 0;
    }
    const path = this.props.data.path;

    const cardStyle = {
    	borderRadius: '5px'
    };

    const cardTextStyle = {
    	height: '84px',
      	padding: '5px',
      	overflow: 'hidden',
      	position: 'relative'
  	};

    return (
    <a className='bridge-card' onClick={this._handleClickCard}>
		<Card style={cardStyle} >
			<div className="card-padder">
				<div className="bridge-card-header">
					<h3>{this.props.data.title}</h3>
		     		<div className='bridge-card-resume'>
						<small>{noOfMembers} members - </small>
						<small>{noOfMessages} Messages</small>
					</div>
				</div>
				<div className="clearfix"></div>
	  			<CardText
	         		style={cardTextStyle}
	        		className='bridge-card-text' >
					{this.props.data.desc}
				</CardText>
				<span className="user">{path.substring(0, path.length - 1)}</span>
			</div>

		</Card>
		<div className='clear'/>
	</a>
    );
  }

});

export default BridgeCard;
