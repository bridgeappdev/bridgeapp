/* BridgeSearch.js
 * React components for listing bridges from search.
 */

/*jshint node: true*/

import React from 'react';
import FireAPI from '../../../lib/FireAPI';
import BridgeAPI from '../../../models/bridges/Bridges';
import SearchAPI from '../../../lib/SearchAPI.js';
import history from '../../../HistoryContainer';

import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import BridgeCard from './BridgeCard';

const BS = {};

BS.Deck = React.createClass({
	displayName: 'BridgeSearchDeck',

	render: function () {
		'use strict';

		const _self = this;
		let searchResult = null;
		const bridgeDataList = this.props.bridges;
		if(bridgeDataList){
			searchResult = bridgeDataList.map(function( bridge ){
				return (
					<BridgeCard
						key={bridge.id}
						data={bridge}
						history={ _self.props.history }
					/>
				);
			});
		}
		return(
			<div className='bridge-search-result-deck'>
				{searchResult}
			</div>
			);
	}

});

export default React.createClass({
	displayName: 'BridgeSearch',

  getInitialState: function (){
		"use strict";
    return {bridges: [] };
  },

	componentWillMount: function() {
		'use strict';
		this._search(null);
	},

	componentWillReceiveProps: function(nextProps){
		'use strict';
		if(nextProps && this.props.location.search !== nextProps.location.search){
			this._search(nextProps);
		}
	},

	_search: function(nextProps){
		'use strict';

		var cb = function(err, bridges){
			if(!err){ this.setState( {'bridges': bridges} );}
		};
		//TODO: Fix this. Why is this working before the app is loaded?
		var authStore = require('../../../stores/AuthStore');

		if (!authStore.isLoggedIn()) {
			FireAPI.init();
  	}

		var params = nextProps?nextProps.location.search:this.props.location.search;
		BridgeAPI.search( params, cb.bind(this));

	},

	_onSearchKeyDown : function(event){
		'use strict';

		var tagsForSearch;
		if(event.keyCode == 13){
			tagsForSearch = event.target.value.split(/[ ]+/).join(',');
			console.log('Going to search for ', tagsForSearch);
			this.setState({tagsForSearch:tagsForSearch})
			if( tagsForSearch.length ) tagsForSearch = '?'+tagsForSearch;
			this.props.history.replaceState({}, '/bsearch'+tagsForSearch);
		}

	},

	render: function (){
		'use strict';
		return (
			<MuiThemeProvider >
				<div className='wrapper clearfix'>
					<div className='center-block'>
						<div className='search-result-header'>
							<h1 className="title">Explore <em>bridges</em></h1>
							<span className="search-resume">{this.state.bridges.length} results found for <em>{this.state.tagsForSearch}</em></span>
							<input
								name="tag-search" id="tag-search" type="search"
								className="search-result-header__search"
								placeholder="Search for tags... e.g. startups"
								onKeyDown={this._onSearchKeyDown} />

						</div>
						<div className='search-result-content'>
							<BS.Deck
								bridges={this.state.bridges}
								history={ this.props.history }
							/>
						</div>
					</div>
				</div>
			</MuiThemeProvider >

			);
	}

});
