'use strict';

import React from 'react';

import { Button, Textfield } from 'react-mdl';

import Profiles from '../../../models/profiles/Profiles';

module.exports = React.createClass({

	displayName:"Username Input",

	delayTimer: null,

	getInitialState: function () {

		return {
			canSubmit: false,
			username: ''
		};
	},

	_handleTextChange: function( e ){

		const parsedValue =
			e.target.value.replace(/\W+/g, "").toLowerCase();
		this.setState( { 'username': parsedValue } );

		if(this.delayTimer){
			clearTimeout(this.delayTimer);
		}
		this.delayTimer = setTimeout((function( value ) {
			this._checkAvailable( value );
		}).bind( this, parsedValue ), 1000);
	},

	_checkAvailable: function( candidateUsername ){

		if( candidateUsername === ''){
			return this.setState({
				canSubmit:false,
				errMessage:''
			});
		}

		Profiles.checkUsernameAvailable(
			candidateUsername,
			( err, isAvailable ) => {
				if(err){
					return this.setState({
						canSubmit:false,
						errMessage:"Something went wrong. Please try again"
					});
				}

				if(isAvailable){
					return this.setState({
						canSubmit:true,
						errMessage: null
					});
				}else{
					return this.setState({
						canSubmit:false,
						errMessage:"Already in use, I´m afraid"
					});
				}
			}
		)
	},

	_handleSubmit: function( e ){
		this.setState( { canSubmit : false } );
		const newUsername = this.state.username;
		return this.props.handleSubmit( newUsername );
	},

	render(){
		const availableTick = (
			<svg style={{marginBottom: '-8px'}} width="40" height="35">
				<path id="check" d="M10,20 l11,10 l15,-20" />
			</svg>
		);

		return(

			<div className='onboarding-username-form' >
					<div className='onboarding-username-form-enter'>
						<div className="onboarding-username-form-input">
							<Textfield
								ref={(ref) => this.username_input = ref}
								label='Enter a username'
								floatingLabel
								value={this.state.username}
								onChange={this._handleTextChange}
								error={this.state.errMessage}
								/>
						</div>
						<div className='onboarding-username-form-tick'>
							{this.state.canSubmit?availableTick:null}
						</div>
					</div>
					<div className="onboarding-username-form-button">
						<Button
							type='button'
							raised
							accent
							disabled={!this.state.canSubmit}
							onMouseUp={this._handleSubmit}
						>
							Continue
						</ Button>
					</div>
			</div>
		);
	}
});
