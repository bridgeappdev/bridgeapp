'use strict';

import React from 'react';

import TagSuggestions from '../../common/TagSuggestions';

import SearchAPI from '../../../lib/SearchAPI';
import Tags from '../../../models/tags/tags';

import { Button, Textfield } from 'react-mdl';

export default class UserTagsInput extends React.Component{

	constructor( props ){
		super( props );

		this._handleClick = this._handleClick.bind( this );

	}

	_handleAddTag( tagName ){

		Tags.incrementTagCount( tagName, ( err, count ) => {
			if( err ){
				return console.warn('userProfileSettings::incrementTagCount ==> ', err);
			}else{
				return console.log('userProfileSettings::incrementTagCount ==> '
				+ 'Update the tag count by one for '
				+ tagName + ' : ', count);
			}
		});
	}

	_handleRemoveTag( tagName ){

		Tags.decrementTagCount( tagName, ( err, count )=>{
			if( err ){
				return console.warn('userProfileSettings::decrementTagCount ==> ', err);
			}else{
				return console.log('userProfileSettings::decrementTagCount ==> '
				+ 'Decrease the tag count by one for '
				+ tagName + ' : ', count);
			}
		});
	}

	_handleClick( e ){

		return this.props.handleClickContinue(
			this.tags_input.getValue()
		);

	}

	render(){
		return (
			<div
				className='onboarding-tags-form'
			>
				<div className='onboarding-tags-form-input' >
					<TagSuggestions
						ref={(ref) => this.tags_input = ref}
						initialValue={this.props.initialValue}
						tagSuggest={SearchAPI.tagSuggest}
						onAddTag={this._handleAddTag}
						onRemoveTag={this._handleRemoveTag}
						/>
				</div>
				<div className="onboarding-tags-button">
					<Button
						type='button'
						raised
						accent
						onMouseUp={ this._handleClick }
					>
						Take me to Chaka!
					</ Button>
				</div>
			</div>
		);
	}
}
