/*	/pages/Welcome/index.js
*
*	UI component for onboarding new users
*
* */
"use strict";

import React  from 'react';

import profileStore from '../../../stores/ProfileStore';

import Bridges from '../../../models/bridges/Bridges';

import SearchAPI from '../../../lib/SearchAPI';
import TagAPI from '../../../models/tags/tags';

import SideMedia from './SideMedia';
import ContentHeader from './ContentHeader';
import UserNameInput from './UserNameInput';
import UserTagsInput from './UserTagsInput';
import ProfileForm from './ProfileForm';
import Footer from './Footer';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class Welcome extends React.Component{

	constructor( props ){
		super( props );

		this.state = {
			stage: 1
		}

		this._handleSubmitUsername =
			this._handleSubmitUsername.bind( this );
		this._clickFinish = this._clickFinish.bind( this );
		this._handleSubmitProfile = this._handleSubmitProfile.bind( this );

	}

	_handleSubmitUsername( newUsername ){
		const _self = this;

		const profile = profileStore.getProfile();
		profile.username = newUsername;

		profile.create( (err) => {
			if(err){}else{
				//Create home bridge and redirect.
				Bridges.createHomeBridge(profile, ( err ) => {
					if(err){

					} else {
						profile.getMyBridges((error) => {
							if (error) {
							} else {
								_self.setState( { 'stage': 2 } );
							}
						});
					}
				});
			}
		});
	}

	_handleSubmitProfile( profile ){
		const _self = this;

		profile.update((error) => {
			if (error) {
				console.log('Error updateing profile on signup ==> ', error);
			} else {
				_self.setState( { 'stage': 3 } );
			}
		});

	}

	_redirectToBridge( profile ){

		let path;
		if(
			this.props.location.state
			&& this.props.location.state.nextPathName
		){
			path = this.props.location.state.nextPathName;
		}else{
			path = '/bsearch';
			if(
				profile
				&& profile.tags){
					path += '?' + this.props.initialProfile.tags.join(',');
			}
		}

		this.props.history.pushState(
			{
				'returnURL':location.pathname,
				'open':true
			},
			path
		);
	}

	_clickFinish( tagList ){
		const _self = this;
		const profile = profileStore.getProfile();
		profile.tags = tagList;

		profile.update((error) => {
			if (error) {
				console.log('Error updateing profile on signup ==> ', error);
			} else {
				_self._redirectToBridge();
			}
		});

	}

	_handleAddTag( tagName ){

		TagAPI.incrementTagCount(tagName, (err, count)=>{
			if(err){
				return console.warn('userProfileSettings::incrementTagCount ==> ', err);
			}else{
				return console.log('userProfileSettings::incrementTagCount ==> '
				+ 'Update the tag count by one for '
				+ tagName + ' : ', count);
			}
		});
	};

	_handleRemoveTag( tagName ){

		TagAPI.decrementTagCount(tagName, (err, count)=>{
			if(err){
				return console.warn('userProfileSettings::decrementTagCount ==> ', err);
			}else{
				return console.log('userProfileSettings::decrementTagCount ==> '
				+ 'Decrease the tag count by one for '
				+ tagName + ' : ', count);
			}
		});
	};

	_getContentByStage(){

		const profile = profileStore.getProfile();
		switch ( this.state.stage ) {
			case 1:
				return (
					<UserNameInput
						handleSubmit={ this._handleSubmitUsername }
					/>
				)
				break;
			case 2:

			return (
				<ProfileForm
					handleClickContinue={ this._handleSubmitProfile }
					initialProfile={ profile }
				/>
			)
				break;
			case 3:

			return (
				<UserTagsInput
					ref={(ref) => this.tags_input = ref}
					initialValue={ profile.tags }
					handleClickContinue={ this._clickFinish }
					tagSuggest={ SearchAPI.tagSuggest }
					onAddTag={ this._handleAddTag }
					onRemoveTag={ this._handleRemoveTag }
				/>
			)
				break;
			default:

		}
	}

	render(){

		return (
			<MuiThemeProvider>
				<div className="onboarding-container">
					<SideMedia stage={ this.state.stage } />
					<div className="onboarding-main">
						<div className="onboarding-main-header" >
							<div className="onboarding-main-header-logo" >
								<a href='http://www.chakachat.com' >Chakachat</a>
							</div>
						</div>
						<div className="onboarding-main-content" >
							<ContentHeader stage={this.state.stage} />
							{ this._getContentByStage() }
						</div>
						<Footer stage={this.state.stage} />
					</div>
				</div>
			</MuiThemeProvider>
		);
	}
}
