'use strict';

import React from 'react';

export default function Footer( props ) {
	let steps;
	switch (props.stage) {
		case 1:
			steps = (
				<div className="onboarding-steps ">

					<div className="onboarding-step selected" />
					<div className="onboarding-step" />
					<div className="onboarding-step" />

				</div>
			);

			break;
		case 2:
			steps = (
				<div className="onboarding-steps ">

					<div className="onboarding-step" />
					<div className="onboarding-step selected" />
					<div className="onboarding-step" />

				</div>
			);

			break;
		case 3:
			steps = (
				<div className="onboarding-steps ">

					<div className="onboarding-step" />
					<div className="onboarding-step" />
					<div className="onboarding-step selected" />

				</div>
			);

			break;
		default:

	}

	return (
			<div className='onboarding-main-footer'>
				{ steps }
				<div className='terms-of-service'>
					All rights reserved (c) 2016. MORS Tech Inc.&nbsp;
					<span>
						<a
							href="https://chakachat.com/privacy_policy"
							target="_blank"
							title="Read the Chakachat Privacy Policy"
						>
							Privacy Policy
					</a>
						&nbsp;|&nbsp;
					<a
						href="http://chakachat.com/terms_of_use"
						target="_blank"
						title="Read the Chakachat Terms of Use"
					>
						Terms & Conditons
					</a>
				</span>
				</div>
			</div>
	);
}
