'use strict';

import React from 'react';

import ImageUploader from '../../../common/ImageUploader';
import { Button, Textfield } from 'react-mdl';

export default class ProfileForm extends React.Component{

	constructor( props ){
		super( props );

		this.state = {
			firstname: this.props.initialProfile.firstname,
			lastname: this.props.initialProfile.lastname,
			bio: this.props.initialProfile.bio
				? this.props.initialProfile.bio
				: ''
		};

		this.MAX_BIO_LENGTH = 250;

		this._handleClick = this._handleClick.bind( this );
		this._handleBioChange = this._handleBioChange.bind( this );

	}

	handleChange( inputName, event ) {

		var ProfileChange = this.state;
		ProfileChange[inputName] = event.target.value;

		this.setState( ProfileChange );
	}

	_handleClick(){
		const profile = this.props.initialProfile;

		profile.firstname = this.state.firstname;
		profile.lastname = this.state.lastname;
		profile.bio = this.state.bio;

		profile.avatar = this.avatar.getValue();

		return this.props.handleClickContinue( profile );
	}

	_handleBioChange( event ) {

		let newBio = event.target.value;
		newBio = newBio.substring( 0, this.MAX_BIO_LENGTH );

		this.setState({
			bio: newBio
		});
	}

	render(){
		return (
			<div
				className='onboarding-profile-form'
			>
				<div className='onboarding-profile-form-photo' >
					<ImageUploader
						ref={(ref) => this.avatar = ref}
						initialValue={this.props.initialProfile.avatar}
						overlayText='Click to add a profile photo'
						/>
				</div>
				<div className="onboarding-profile-form-details">
					<Textfield
						ref={(ref) => this.firstname_input = ref}
						className='profile-form-details-firstname'
						label="First Name"
						floatingLabel
						defaultValue={this.state.firstname}
						onChange={this.handleChange.bind(this, 'firstname')}
						/>
					<Textfield
						ref={(ref) => this.lastname_input = ref}
						className='profile-form-details-lastname'
						label="Family Name"
						floatingLabel
						value={this.state.lastname}
						onChange={this.handleChange.bind(this, 'lastname')}
						/>
					<Textfield
						ref={(ref) => this.bio_input = ref}
						label="Bio"
						floatingLabel
						value={this.state.bio}
						rows={2}
						onChange={this._handleBioChange}
						/>
				</div>
				<div className="onboarding-profile-form-button">
					<Button
						type='button'
						raised
						accent
						onMouseUp={this._handleClick}
					>
						Continue
					</ Button>
				</div>
			</div>
		);
	}
}
