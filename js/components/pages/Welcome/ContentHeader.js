'use strict';

import React from 'react';

export default function ContentHeader( props ) {
	let text;
	switch (props.stage) {
		case 1:
			text = (
				<div className="onboarding-main-content-header">
					<h2
						className="onboarding-step-name"
					>
						Choose A Username
					</h2>
					<p
						className="onboarding-step-description"
					>
					Username must be unique and cannot be changed.
					So choose wisely my dear friend.
					</p>
				</div>
			);

			break;
		case 2:
			text = (
				<div className="onboarding-main-content-header">
					<h2
						className="onboarding-step-name"
					>
						Tell us a little about yourself
					</h2>
					<p
						className="onboarding-step-description"
					>
					Let people know who you are and why your here, It's all about
					networking!
					</p>
				</div>
			);

			break;
		case 3:
		text = (
			<div className="onboarding-main-content-header">
				<h2
					className="onboarding-step-name"
				>
					Choose some topics of interest
				</h2>
				<p
					className="onboarding-step-description"
				>
				Select up to 5 tags so we can help you find some bridge of interest and
				let people know what you are looking for.
				</p>
			</div>
		);

			break;
		default:

	}

	return (
			text
	);
}
