'use strict';

import React from 'react';


function _scrollBarWidth() {
	document.body.style.overflow = 'hidden';
	let width = document.body.clientWidth;
	document.body.style.overflow = 'scroll';
	width -= document.body.clientWidth;
	if(!width) width = document.body.offsetWidth - document.body.clientWidth;
	document.body.style.overflow = '';
	return width;
}

export default function ContentHeader( props ) {
	switch (props.stage) {
		case 1:
			return (
				<div
					className="onboarding-sidemedia"
					style={
						{ 'backgroundImage' : "url('https://assets2-production-mightybell.imgix.net/assets/onboarding/image_1-30b9fbb415727979a7f651be86f224dc.jpg')",
							'right': '0px' }
					}
				/>
			);

			break;
		case 2:
			return (
				<div
					className="onboarding-sidemedia"
					style={
						{ 'backgroundImage': "url('http://chakachat.com/wp-content/uploads/chaka-networking-app-1.jpg')" ,
						'right': _scrollBarWidth() }
					}
				/>
			);

			break;
		case 3:
		return (
			<div
				className="onboarding-sidemedia"
				style={
					{ 'backgroundImage' : "url('https://assets2-production-mightybell.imgix.net/assets/onboarding/image_1-30b9fbb415727979a7f651be86f224dc.jpg')",
						'right': '0px' }
				}
			/>
		);
			break;
	}
}
