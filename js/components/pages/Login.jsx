/* Login.js
* React components for login UI.
*/
'use strict';

import React from 'react';
import Reflux from 'reflux';
import reactMixin from 'react-mixin';

import Auth from '../../models/auth/Auth';

import profileStore from '../../stores/ProfileStore';
import authStore from '../../stores/AuthStore';
import AuthActions from '../../actions/AuthActions';

import { Tooltip } from 'react-mdl';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

export default class Login extends React.Component {
	constructor ( props ) {

		super( props );

		this.state = {
			buttonEnabled: false,
			errorMessage: ""
		};

		this.handleLoginClick = this.handleLoginClick.bind( this );
		this.loginRedirect = this.loginRedirect.bind( this );
	}

	componentDidMount() {

		this.listenTo(
			authStore,
			this.onChange,
			this.onInitialChange
		);

		this.listenTo(
			profileStore,
			this.onChange
		);

		//document.getElementById('submit').disabled = true;
	}

	onInitialChange( data ) {

		if ( data.eventName === 'login' ) {
			this.loginRedirect();
		}
	}

	onChange( eventName, data ) {

		if ( eventName === 'profileLoaded' ) {
			this.loginRedirect( data );
		}
	}

	loginRedirect( profile ) {

		const location = this.props.location;
		if ( location.state && location.state.nextPathName ) {
			this.props.history.replaceState( {}, location.state.nextPathName );
		} else if( profile.id ) {
			this.props.history.replaceState(
				{},
				'/bridge/' + profile.username + '/home'
			);
		} else {
			//redirect to error page
		}
	}

	_getErrorMessage( err ){
		let message;

		switch ( err.code ) {
			case "INVALID_EMAIL":
			message = "There was an error logging in. Please try again.";
				break;
			default:
			message = "There was an error logging in. Please try again.";

		}

		return message;
	}

	handleLoginClick( e ) {

		e.preventDefault();

		const email = document.getElementById('email').value;
		const password = document.getElementById('password').value;
		const _self = this;

		const submitButton = document.getElementById('submit');
		submitButton.innerHTML = "Logging in...";
		submitButton.disabled = true;

		Auth.login(email, password, function ( err, authData ) {
			if ( err ) {
				const errorMessage = _self._getErrorMessage( err );
				_self.setState({ errorMessage: errorMessage });
				submitButton.innerHTML = 'Submit';
				submitButton.disabled = false;
			}
		}.bind( this ));
	}

	handleSignUpClick(e) {

		e.preventDefault();
		this.props.history.pushState(
			this.props.location.state,
			'/signup'
		);
	}

	render(){

		return (
			<MuiThemeProvider >
				<div className='login-page'>
					<nav className='topbar-nav' >
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">How it works</a></li>
							<li><a href="#">Contact</a></li>
							<li><a href="#">Blog</a></li>
						</ul>
					</nav>
					<div className="login-bg">
						<div className="login-container">
							<img
								className="login-logo"
								src="/img/chakachat-logo_smll.png"
							/>
							<div className='login-form'>
								<div
									className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"
									style={ { width: "100%" } }
								>
									<input
										className='login-email-input mdl-textfield__input'
										type='text'
										placeholder="Email"
										title='Enter your Email'
										id='email'
										pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
										msgError="Invalid email"
									/>
									<input
										className='login-password-input mdl-textfield__input'
										title='Enter your Password'
										placeholder="Password"
										type='password'
										id='password'
									/>

								</div>
								<button
									id="submit"
									onClick={ this.handleLoginClick.bind(this) }
									className="
										login-button mdl-button mdl-js-button
										mdl-button--raised mdl-js-ripple-effect
										mdl-color-text--white
									"
								>
									Log In
								</button>
								<p className='login-error'>{this.state.errorMessage}</p>
								<div className="signup">
									<span>Don't have and account? </span>
									<a
										className="signup-anchor"
										href="#"
										onClick={ this.handleSignUpClick.bind( this ) }
									>
										Register here
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</MuiThemeProvider >
		);
	}
}

reactMixin(Login.prototype, Reflux.ListenerMixin);
