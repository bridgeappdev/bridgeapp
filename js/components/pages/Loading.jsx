/* components/pages/Loading.jsx
* React components for loading page.
* Show this page while the app data is loaded.
*/

import React from 'react';

import Avatar from 'material-ui/Avatar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

module.exports = React.createClass({

	displayName: 'Loading Page',

	getInitialState: function () {
		'use strict';

		return { message: 'We\'ll cross that bridge ... today!' };
	},

	render: function () {
		'use strict';

		const statusMessage = this.state.message;

		return (

			<MuiThemeProvider >
				<div className='loading-container' >
						<img
							className="loading-logo"
							src="/img/chakachat-logo_smll.png"
						/>
					<div className="loading-panel">
						<div className="loading-panel-avatar">
							<Avatar
								style={
									{
										'height': '75px',
										'width': '75px'
									}
								}
								src={this.props.profile.avatar}
						/>
					</div>
					<div className="loading-panel-slogan">{statusMessage}</div>
							<div className="spinner-container">
								<div className="clear-loading loading-effect-1">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>
					</div>
				</div>
		</MuiThemeProvider >
		);
	},
});
