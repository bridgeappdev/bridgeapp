import React from 'react';

const StatusMessage = React.createClass({
	render: function () {
		return (<div className="status">{this.props.message}</div>);
	},
});

module.exports = StatusMessage;
