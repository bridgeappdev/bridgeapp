import React from 'react';

const SocialMediaButtons = React.createClass({
	render: function () {
		return (
			<div>
			<a
				className="btn btn-social btn-twitter"
				onClick={this.props.handleTwitterClick}
			>
				<span className="fa fa-twitter"></span>
				Sign in with Twitter
			</a>
			</div>
		);
	},
});

module.exports = SocialMediaButtons;
