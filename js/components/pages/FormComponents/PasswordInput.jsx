import React from 'react';
import TextField from 'material-ui/TextField';

const PasswordInput = React.createClass({
	render: function () {
		return (
		<TextField
			type="password"
			required="true"
			hintText="Password"
			floatingLabelText="Password"
			value={this.props.passwordInput}
			onChange={this.props.handlePasswordChange}
		/>
		);
	},
});

module.exports = PasswordInput;
