import React from 'react';

const FormTitle = React.createClass({
	render: function () {
		return (
			<div className="formtitle">
			<h1>{this.props.formTitle}</h1>
			</div>);
	},
});

module.exports = FormTitle;
