import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';

const SubmitBtn = React.createClass({
	render: function () {
		return (
			<RaisedButton
				type="submit"
				className="loginbtn"
				form="accessform"
				label="Submit"
			/>
	);
	},
});

module.exports = SubmitBtn;
