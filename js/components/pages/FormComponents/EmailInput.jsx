import React from 'react';
import TextField from 'material-ui/TextField';

const EmailInput = React.createClass({

	render: function () {
		return (
		<TextField
			type="email"
			required="true"
			hintText="username@domain.com"
			floatingLabelText="Email"
			value={this.props.emailInput}
			onChange={this.props.handleEmailChange}
		/>
		);
	},
});

module.exports = EmailInput;
