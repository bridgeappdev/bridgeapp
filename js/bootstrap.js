/* bootstrap.js
*
* Get the app up and running
*
* */
'use strict';

import React from 'react';

import profileStore from './stores/ProfileStore';

import Bridges from './models/bridges/Bridges';
import Profiles from './models/profiles/Profiles';

let nextFunc = null;

function _onChange( eventName, profile ){
	if (eventName === 'profileLoaded') {
		nextFunc();
		_stopListeningToProfileStore();
		if( !!profile.id ){
			return profile.getMyBridges();
		} //Otherwise its a new user so we wait.

	}
};
const _stopListeningToProfileStore = profileStore.listen( _onChange );

export default function bootstrap( authData, next ){
	nextFunc = next;

	Profiles.getMyProfileData( authData );

}
