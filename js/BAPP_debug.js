/* BApp.js
 *
 * Put everything on the Global so we can debug
 * TODO: REMOVE THIS FOR PRODUCTION
 *
 * */

module.exports = {

	Actions : require('./actions'),

	Stores 	: require('./stores'),

	API			: require('./lib')

};
