export default {
	fontFamily: 'Roboto, sans-serif',
	palette: {
		primary1Color:"#00BCD4",
		primary2Color: "#0097A7",
		primary3Color: "#78909C",
		accent1Color: "#8BC34A",
		accent2Color: "#F5F5F5",
		accent3Color: "#9E9E9E",
		textColor: "#000000",
		alternateTextColor: "#FFFFFF",
		canvasColor: "#FFFFFF",
		borderColor: "#E0E0E0",
		disabledColor: "#EEEEEE"
	}
};
