/*	BridgeApp v0.0.1
*	main entry script
*/

/*	Development notes
* MVP code intended for major re-factoring.
* Will attempt to decouple the js library as much as possible
* with an eye on future developments.
*
*/

/*Add BApp, with a reference to the Stores and the APIs, to global for ease
of debugging and dev. Take this off for production.*/
'use strict';

window.BApp = require('./BAPP_debug');

//bootstrap the actions and stores
import bootstrap from './bootstrap';

import injectTapEventPlugin from 'react-tap-event-plugin';

// Needed for onTouchTap
// Can go away when react 1.0 release
// Check this repo:
// https://github.com/zilverline/react-tap-event-plugin
injectTapEventPlugin();

import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from 'react-router';
import routes from './routes';
import createHistory from './HistoryContainer';

import Auth from './models/auth/Auth';

import authStore from './stores/AuthStore';

let shouldRenderApp = true;

if (window.location.hash) {
	window.EBA = window.location.hash;
}

function renderApp(){
	if (shouldRenderApp) {
		const route = (
			< Router history={ createHistory } >
			{routes}
			< /Router>
		);
		ReactDOM.render( route, document.getElementById( "app" ) );
		shouldRenderApp = false;
	}
}

function _onAuth( eventName, authData ) {

	if (eventName === 'login') {
		_stopListeningToAuthStore();

		bootstrap( authData, () => {
			renderApp();
		} );

	} else {
		renderApp();
	}
}

const _stopListeningToAuthStore = authStore.listen(_onAuth);
Auth.authUser();
