/*SearchAPI
*
* Search module for BridgeApp
*
* */

import ChatBridge from '../models/bridges/ChatBridge';
import EventBridge from '../models/bridges/EventBridge';

const http = require('http');
const searchConfig = require('../../config').searchConfig;
//import Bridge from './Bridge';

const SAPI = module.exports;

function httpPostRequest(options, data, callback){
	'use strict';

	if(typeof data === "function" && !callback){
		callback = data;
		data = null;
	}

	var req = http.request(options, function(res) {
		console.log('STATUS: ' + res.statusCode);
		console.log('HEADERS: ' + JSON.stringify(res.headers));
		var result = '';
		res.on('data', function (chunk) {
			console.log('BODY: ' + chunk);
			result+=chunk;
		});

		res.on('end', function() {
			console.log('No more data in response.');
			callback(null, JSON.parse( result ));
		});
	});

	req.on('error', function(e) {
		console.log('problem with request: ' + e.message);
	});

	// write data to request body
	req.write(JSON.stringify(data));
	req.end();

}


SAPI.searchBridgesByTag = function searchBridgesByTag(tags, next) {
	'use strict';

	var buildBridgeCallBack = function(err, searchResult){
		if(err){
			return next(err);
		}

		let result = Object.keys(searchResult).map(function(bridgeId){
			searchResult[bridgeId].id = bridgeId;
			switch ( searchResult[bridgeId].type ) {
				case 'chat':
				case 'CHAT':
					return new ChatBridge( searchResult[bridgeId] );
					break;
				case 'event':
				case 'EVENT':
					return new EventBridge( searchResult[bridgeId] );
					break;
				default:

			}
		});
		return next(null, result);

	};

	var postData = {
		'index' : 'bappbridges',
		'query': tags
	};

	var options = {
		hostname: searchConfig.hostname,
		port: searchConfig.port,
		path: '/search/tags',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		}
	};

	return httpPostRequest(options, postData, buildBridgeCallBack.bind( this ));

};

SAPI.tagSuggest = function tagSuggest(text, callback){
	'use strict';

	if(!text || text.length === 0){return callback(null, []);}
	var postData = {
		'index' : 'bappbridges',
		'text': text
	};

	var options = {
		hostname: searchConfig.hostname,
		port: searchConfig.port,
		path: '/search/tags/_suggest',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		}
	};

	return httpPostRequest(options, postData, callback);

};

SAPI.messages = function messages(queryString, bridgeList, next) {
	if(!queryString || queryString.length === 0){
		return callback(null, []);
	}
	var postData = {
		'query' : queryString,
		'bridges': bridgeList
	};

	var options = {
		hostname: searchConfig.hostname,
		port: searchConfig.port,
		path: '/search/messages',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
		}
	};

	return httpPostRequest(options, postData, next);

	}
