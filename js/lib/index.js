/*
* API (main.js)
*
* Entry point for the API classes
*/
/*jshint node:true*/

module.exports = {

	fireAPI: require('./FireAPI'),

	bridgesAPI: require('./BridgesAPI')
};
