/* lib/UserProfileAPI.js
* Module for handling the functionality of the user profile object
* in the Bridge App
* */
'use strict';

const FireAPI = require('./FireAPI'),
profileActions = require('../actions/ProfileActions');

module.exports = {

	saveUserProfile: function(id, userProfileData, cb){

		return FireAPI.profiles.saveUserProfile(id, userProfileData, cb);

	}
};
