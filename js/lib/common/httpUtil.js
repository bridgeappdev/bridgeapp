/* httpUtil helper functions
 * 
 * Common http methods for BridgeApp
 *
 * */
//jshint node:true

var http = require('http');
var HTTP = module.exports = {}; 

HTTP.httpPostRequest =  function httpPostRequest(options, data, callback){
	'use strict'; 

	if(options.method && options.method !== 'POST'){
		console.log('Wrong or missing http verb for POST request. Setting option.method to POST');
	}

	options.method = 'POST';
	if(!options.headers){
		if(JSON.stringify(data)){
  		options.headers =  { 'Content-Type': 'application/json'	};
		}else{
			return callback(new Error('httpUtil: Content-Type header provider and data not of default type json '));
		}
	}
	return HTTP.httpRequest(options, data, callback);

};

HTTP.httpRequest =  function httpRequest(options, data, callback){
	'use strict'; 

	if(typeof data === "function"){
			if(!callback){
				callback = data;
				data = null; 
		}else{
			return callback(new Error('httpRequest: Incorrect parameters'));
		}
	}

	var req = http.request(options, function(res) {
		console.log('STATUS: ' + res.statusCode);
		console.log('HEADERS: ' + JSON.stringify(res.headers));
		var result = ''; 
		res.on('data', function (chunk) {
			console.log('BODY: ' + chunk);
			result+=chunk;
		});
 	 
		res.on('end', function() {
			console.log('No more data in response.');
			if(result){
				return callback(null, JSON.parse( result ));
			}
	 });
	});

	req.on('error', function(e) {
  	console.log('problem with request: ' + e.message);
		return callback(e);
	});

	// write data to request body
	//TODO: What if this is a GET? Body should be empty.
	req.write(JSON.stringify(data));
	req.end();

};

