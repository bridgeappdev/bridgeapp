/* lib/common/bridgesUtil.js 
 *
 * helper functions for the bridges objects in
 * BridgeApp
 *
 * */
//jshint node:true

var bridgesUtil = module.exports = {}; 

bridgesUtil.getIdFromURLParams = function getIdFromURLParams(params){
	'use strict';

	return params.username + ',' + params.bname;

};

bridgesUtil.getPathFromBridgeObject = function(bridgeObj){
	'use strict';

	var bridgeId = bridgeObj.id;

	return bridgeId.slice(0, bridgeId.indexOf(',')) + '/' + bridgeObj.name;
};
