import moment from 'moment';
moment().format();

let Util = module.exports = {};

Util.getDayRangeByTimestamp = function(timestamp, noOfDays, direction){
  'use strict';

  let rangeObj = {'startDate': 0, 'endDate': 0};
  let endDate = parseInt(moment(timestamp)
                    .startOf('day')
                      .format('x'));
  let startDate = parseInt(moment(endDate)
                  .subtract(noOfDays, 'days')
                    .startOf('day')
                      .format('x'));
  rangeObj.startDate = startDate;
  rangeObj.endDate = endDate;

  return rangeObj;

}
