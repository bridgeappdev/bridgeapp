/** Various validations. */
/* eslint max-len: 0 */

const Firebase = require('firebase');

function isValidPassword(candidatePassword) {
	const passwordRegex = new RegExp('^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})');
	return passwordRegex.test(candidatePassword);
}

function isCandidateEmailUnique(enteredEmail, cb) {
	'use strict';

	let ref = new Firebase('https://br1dg3co.firebaseio.com/users');
	ref.orderByChild('email')
	.startAt(enteredEmail)
	.endAt(enteredEmail)
	.once('value', function (snapshot) {
		let query = snapshot.exists();
		let avai = true;
		if (query === true) {
			avai = false;
		}
		cb(null, avai);
	});
}

function isCandidateEmailValidEmail(candidateEmail) {
	'use strict';
	const EMAILRX = new RegExp(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/);
	return EMAILRX.test(candidateEmail);
}

function canSubmitForm(submittedForm) {
	'use strict';
	if (submittedForm.validForm.validEmail === true && submittedForm.validForm.validPassword === true
		&& submittedForm.hasFormBeenSubmitted === false) {
		return true;
	}
}

export default { isCandidateEmailValidEmail, canSubmitForm, isValidPassword, isCandidateEmailUnique };
