//jshint esnext:true
import q from 'q';

export default class FBReplies {
	constructor(fbRef){
		this.rootRef = fbRef;
		this.refs = { root: fbRef.child('replies')};

	}
	getMessageReply(bridgeId,messageId){
		var defer = q.defer();
		var obj = {};
		this.rootRef.child('messages').child(bridgeId).child(messageId).once('value', function(messageSnap) {
			if(messageSnap.exists()){
				obj = messageSnap.val();
				obj.id=messageId;
				obj.bridge = bridgeId;
				defer.resolve(obj);
			}else{
				defer.reject(null);
			}
		});
		return defer.promise;
	}

	listReplies(bridgeId,userId,next){
		let _self = this;
		let promises = [];
		console.log(".....",bridgeId,userId)
		const replyRef = this.rootRef.child('users').child(userId).child('replies').child(bridgeId);
		replyRef.once('value', (dataSnapshot) => {
			if(dataSnapshot.exists()){
				let data = dataSnapshot.val();
				for(var key in data){
					promises.push(_self.getMessageReply(bridgeId,key));
				}

				q.all(promises).then((data)=>{
					return next(null,data);
				});

			}
		});
	}
	addBridgeListeners(bridgeId,userId,next){
		console.log("ESCUCHANDOOO================================",bridgeId,userId)
		let _self = this;
		const replyRef = this.rootRef.child('users').child(userId).child('replies').child(bridgeId);
		replyRef.on('child_added', (dataSnapshot) => {
			if(dataSnapshot.exists()){
				let key = dataSnapshot.key();
				_self.getMessageReply(bridgeId,key).then((result)=>{
					next(null,result);
				});
			}
		});
	}
	removeBridgeListeners(bridgeId,userId){
		const replyRef = this.rootRef.child('users').child(userId).child('replies').child(bridgeId);
		replyRef.off();
	}
	addMessageListeners(bridgeId,messageId,next){
		const replyRef = this.refs.root.child(bridgeId).child(messageId);
		replyRef.on('child_added', (dataSnapshot) => {
			console.log(dataSnapshot);
			if(dataSnapshot.exists()){
				next(null,dataSnapshot.val());
			}
		});
	}

	removeMessageListeners(bridgeId,messageId){
		const replyRef = this.refs.root.child(bridgeId).child(messageId);
		replyRef.off();
		//delete replyRef;
	}
	listMessages(bridgeId,messageId,next){
		const replyRef = this.refs.root.child(bridgeId).child(messageId);
		replyRef.once('value', (dataSnapshot) => {
			if(dataSnapshot.exists()){
				next(null,dataSnapshot.val());
			}
		});
	}
	save(bridgeId,messageId,model,next){
		const replyRef = this.refs.root.child(bridgeId).child(messageId);
		replyRef.push(model, ( FBError )=>{
			if(FBError){
				return next(FBError);
			} else {
				let userReplies = this.rootRef.child('users').child(model.userId).child('replies');
				userReplies.child(bridgeId).child(messageId).set(true,()=>{
					console.log("ok....",model)
				});
				return next(null);
			}
		});
	}
}
