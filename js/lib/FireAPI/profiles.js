/*	/lib/FireAPI/profile.js
*
*	FireBase API.
*	Util library for FB interactions with user profile document.
*/

import Firebase from 'firebase';

import profileActions from '../../actions/ProfileActions';

let FBProfile = {},
refs ={};

FBProfile.init = function init(fbRef){
	'use strict';

	//TODO: This doc should be called profile in FB
	//this.refs = { root: fbRef.child('profiles')};
	//this.refs = { root: fbRef.child('users')};
	this.refs = { root: fbRef.child('members')};

	return this;

};

FBProfile.setMeRef = function setMeRef(id){
	'use strict';

	this.refs.me = this.refs.root.child(id);

};

FBProfile.getProfileByUId = function getProfileByUId(UId, cb){
	'use strict';

	var ref = this.refs.root,
	queryref = ref.orderByKey().equalTo(UId);

	queryref.on('value', function(snapshot) {
		//TODO Add reference to ref and turn off if the profile
		//doesn't exist.
		cb(null, snapshot.val());
	}, function(error) {
		cb(error);
	});
};

FBProfile.saveUserProfile = function saveUserProfile(id, profile, cb) {
	'use strict';

	const ref = this.refs.root;
	return ref.child(id).update(profile, (err) => {
		cb(err);
	});
}

FBProfile.setBridgeLastVisited = function setBridgeLastVisited(bridgeId, next){
	this.refs.me.child('bridges').child(bridgeId).set(
		Firebase.ServerValue.TIMESTAMP,
		(err) => {
			return next(err);
		}
	);
}

module.exports = FBProfile;
