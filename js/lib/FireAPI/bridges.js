/*	/lib/FireAPI/bridges.js
*
*	FireBase API.
*	Util library for FB interactions with bridges document.
*/
'use strict';

var FBBridges = {},
//TODO: This file should be a class extending FireAPI
Firebase = require('firebase'),
bridgesUtil = require('../common/bridgesUtil'),
FireAPI = require('./'),
bridgeActions = require('../../actions/BridgesActions'),
refs = {};

FBBridges.init = function init(fbRef){

	this.refs = { root: fbRef.child('bridges')};

	return this;

};

FBBridges.saveNewBridge = function saveNewBridge(id, bridgeData, next){

	var generateRef = this.refs.root.push(),
	bridgeId = id + ',' + generateRef.key();
	var bridgeRef = this.refs.root.child(bridgeId);
	bridgeData.createdAt = Firebase.ServerValue.TIMESTAMP;
	bridgeRef.set(bridgeData, function(error) {
		if (error) {
			console.log('Synchronization failed');
			return next(error);
		} else {
			console.log('Synchronization succeeded');
			return next(null, bridgeId);
		}
	});
};

FBBridges.updateBridge = function updateBridge(id, bridgeData, next){

	const bridgeRef = this.refs.root.child(bridgeData.id);
	bridgeRef.update(bridgeData, function(error) {
		if (error) {
			console.log('Synchronization failed');
			return next(error);
		} else {
			console.log('Synchronization succeeded');
			return next(null, bridgeData);
		}
	});
};

FBBridges.addUserToBridge = function addUserToBridge(bridgeId, user, next){

	if(!bridgeId || !user) {
		return;
	}

	var queryref = this.refs.root.child(bridgeId).child('members');
	var data = {};
	data[user] = true;
	queryref.update(data, function(error){

		if(error){
			console.error('Didn\'t add user to bridge. So how do we rollback??', error);
			if(typeof next === 'function'){
				return next(error);
			}
		}else{
			if(typeof next === 'function'){
				return next(null);
			}
		}

	});
};


module.exports = FBBridges;
