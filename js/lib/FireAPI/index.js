/*	FireAPI.js
 *
 *	FireBase API.
 *	Util library for FB interactions.
 */
'use strict';

import FBReplies from './replies';

const FB = {};
let serverOffset;
FB.ref = null;
FB.refs = {};

FB.init = function( refUrl ){

	var Firebase = require('firebase');

	if (!refUrl) {refUrl = "https://br1dg3co.firebaseio.com";}
	this.ref = new Firebase(refUrl);

	//Set up common refs library
	this.refs.bridges = this.ref.child('bridges');
	this.refs.messages = this.ref.child('messages');
	this.refs.users = this.ref.child('members');

	const offsetRef = this.ref.child(".info/serverTimeOffset");
	offsetRef.on("value", function(snap) {
		serverOffset = snap.val();
	});

	//TODO; This should be class definitons that extend FireAPI

	this.bridges = require('./bridges').init(this.ref);
	this.profiles = require('./profiles').init(this.ref);

	this.replies = new FBReplies( this.ref );

	return this;

};

FB.getEstimatedServerTimeMs = function() {
	'use strict';

	return new Date().getTime() + this.offset;
};

FB.addAuthListener = function (cb) {
	'use strict';

	this.ref.onAuth(cb);

};

FB.getIdFromSnapshot = function(docType, snapshot){
	'use strict';

	switch(docType){
		case 'my_bridges':
			return snapshot.key().replace(',','/');
			break;
		default:
			return;

	};
};

module.exports = FB;
