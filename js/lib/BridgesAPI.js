/*BridgesApi.js
 *
 * Interaction with the backend server for the bridges application domain
 */

import bridgeActions  from '../actions/BridgesActions';
import messageActions from '../actions/MessageActions';

import FireAPI        from './FireAPI';
import SearchAPI      from './SearchAPI';

import config         from '../../config';
import httpUtil       from '../lib/common/httpUtil';

var BAPI = {};

BAPI.sendBridgeInvites = function(invites, userData, bridgeData, cb){
	'use strict';

	let isArray = Object.prototype.toString.call( invites ) === '[object Array]';
	if(!isArray || invites.length === 0){
	 return cb(new Error('Missing or badly formed invites array'));
	}

	if (!userData || !bridgeData){
	 return cb(new Error('Bridge invites require user and bridge data'));
	}

	const postData = {
  	'emails' : invites,
		'user': userData,
		'bridge': bridgeData
	};

	const options = {
  	hostname: config.inviteConfig.hostname,
  	port: config.inviteConfig.port,
  	path: '/bridges/invites',
  	method: 'POST',
  	headers: {
   	 'Content-Type': 'application/json',
  	}
	};

	return httpUtil.httpRequest(options, postData, cb);

};

BAPI.updateBridge = function updateBridge (id, bridgeData, next) {
	'use strict';

	console.log('BRIDGESAPI::Update bridge ==> ', bridgeData);

	if(!bridgeData||!id){
		return next(new Error('No bridge data to update!'));
	}

	return FireAPI.bridges.updateBridge(id, bridgeData, next);

};

module.exports = BAPI;
