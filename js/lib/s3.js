let url = "http://192.241.170.241:8100/s3/";

export default class S3{

	static getS3UrlUpload( file, isImage, username, next ){
		const ACL = isImage ? 'public-read' : 'private';
		const sendData = {
			ACL : ACL,
			ContentType : file.type,
			ContentLength : file.size,
			filename: file.name,
			username: username
		};
		$.ajax({
			url: url + 'getS3URL',
			type: 'POST',
			data:sendData,
			success: function (data) {
				next(null,data)
			}
		});
	}

	static getS3UrlDownload( fileKey, next ){

		const sendData = {
			key: fileKey
		};
		$.ajax({
			url: url + 'getS3Download',
			type: 'POST',
			data:sendData,
			success: function ( data ) {
				next( null, data.s3Downloadurl )
			}
		});
	}

	static sendFile( file,url,progress,next ){
		$.ajax({
			url: url,
			type: 'PUT',
			data: file,
			xhr: function() {  // Custom XMLHttpRequest
				var myXhr = $.ajaxSettings.xhr();
				if(myXhr.upload){ // Check if upload property exists
					myXhr.upload.addEventListener('progress',function progressHandlingFunction(e){
						if(e.lengthComputable){
							progress(e.loaded,e.total);
						}
					}, false); // For handling the progress of the upload
				}
				return myXhr;
			},
			success: function (data) {
				next(null,data)
			},
			cache: false,
			contentType: false,
			processData: false,
			multipart:false
		});
	}
}
