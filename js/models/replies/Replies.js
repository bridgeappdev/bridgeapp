'use strict';

import FBase from '../firebase/fbase';

import replyActions from '../../actions/ReplyActions';
import replyStore from '../../stores/ReplyStore';

import Messages from '../messages/Messages';

import moment from 'moment';
moment().format();

/**
*
* BEGIN HELPER/PRIVATE FUNCTION DEFINITION
*
**/
function _updateUserReplyIndex( setRef, eventType ){

	setRef.transaction( function( metaData ) {

		if( metaData ){

			metaData.lastViewed = Firebase.ServerValue.TIMESTAMP;
			if( eventType === 'open'){
				metaData.viewCount = metaData.viewCount + 1;
			}

			return metaData;
		} else {
			return;
		}

	}, function(error, committed, snapshot) {
		if (error) {
			console.log( error );
			//return next(error);
		}
		console.log(snapshot.val());
		//return next(null, snapshot.val());
	});
}

function _onChildAddedCallback( dataSnapshot ) {

	const message = Messages.parseFBData( dataSnapshot );
	replyActions.receiveThreadMessage( message );

}
function _onChildChangedCallback ( dataSnapshot ) {

	const message = Messages.parseFBData( dataSnapshot );
	replyActions.updateThreadMessage( message );

}
function _onChildRemovedCallback ( dataSnapshot ) {

	replyActions.removeThreadMessage( dataSnapshot.key() );

}
/**
*
* BEGIN CLASS DEFINITION
*
**/

const repliesRef = FBase.buildRef('replies');
export default class Replies {

	static getRef(){
		return repliesRef;
	}

	static getRepliesForBridge( bridgeId, userRef, next ){

		const replyIndexRef = userRef.child( 'replies/' + bridgeId );

		replyIndexRef.once('value', ( dataSnapshot ) => {
			const replyIndex = dataSnapshot.val();
			replyActions.loadIndex(
				replyIndex
				? replyIndex
				: {}
			 );

			const noOfThreads = dataSnapshot.numChildren();
			let iterations = 1;
			let lastReplyKey = "0";
			dataSnapshot.forEach( ( childSnapshot ) => {
				lastReplyKey = childSnapshot.key();
				Messages.getMessageById(
					bridgeId,
					lastReplyKey,
					( err, received ) => {
						if( received ){
							replyActions.newReplyThread( received );
						}
						if(iterations === noOfThreads){
							replyActions.loadList();
						}
						iterations+=1;
					}
				);
			} );

			//replyIndexRef.orderByKey().startAt( lastReplyKey ).on(
			replyIndexRef.on(
				'child_added',
				( dataSnapshot ) =>{
					const messageId = dataSnapshot.key();
					if( messageId === lastReplyKey ){ return void 0 };

					const replyIndex = dataSnapshot.val();
					replyActions.updateIndex( messageId, replyIndex );
					Messages.getMessageById(
						bridgeId,
						messageId,
						( err, received ) => {
							if( received ){
								replyActions.newReplyThread( received );
							}
						}
					);
				}, (error) => {
				console.log(error);}
			);

			replyIndexRef.on('child_changed', ( dataSnapshot ) => {
				const replyMessageId = dataSnapshot.key();
				const replyIndexData = dataSnapshot.val();
				return replyActions.updateIndex( replyMessageId, replyIndexData );
			});
			replyIndexRef.on('child_removed', ( dataSnapshot ) => {
				const replyMessageId = dataSnapshot.key();
				return replyActions.removeIndex( replyMessageId );
			});

		}, (error) => {
			console.log(error);
		} );

	}

	static closeBridgeReplies( bridgeId, userRef, next ){

		replyActions.closeBridge();

		const replyIndexRef = userRef.child( 'replies/' + bridgeId );
		replyIndexRef.off();
	}

	static getThread( bridgeId, userRef, messageId, next ){

		if(typeof bridgeId !== 'string'){
			const error = new Error('bridgeId is required and should be a string');
			if (next){
				return next( error );
			}else{
				throw error;
			}
		}

		const noOfMessagesToLoad = 50;
		const _self = this;

		//TODO: How do we know if we have loaded all the messages?
		const threadRef = repliesRef.child( bridgeId + '/' + messageId)
			.orderByKey()
			.limitToLast( noOfMessagesToLoad );
		threadRef.once('value', ( dataSnapshot ) =>{
			if( dataSnapshot.numChildren() < noOfMessagesToLoad ){
				replyActions.allRepliesLoaded();
			}
		});
		threadRef.on( 'child_added', _onChildAddedCallback, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		})
		threadRef.on('child_changed', _onChildChangedCallback, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		})
		threadRef.on('child_removed', _onChildRemovedCallback, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		})

		const userReplyIndexRef = userRef.child(
			'replies/' + bridgeId + '/' + messageId
		)
		_updateUserReplyIndex(  userReplyIndexRef, 'open' );

		return next( null );

	}
	static getPreviousReplies(
		bridgeId,
		messageId,
		oldestMessageId,
		noOfMessagesToLoad,
		next ){

		if(typeof bridgeId !== 'string'){
			const error = new Error('bridgeId is required and should be a string');
			if (next){
				return next( error );
			}else{
				throw error;
			}
		}

		const _self = this;

		const threadRef = repliesRef.child( bridgeId + '/' + messageId);
		const getThreadRef = threadRef.orderByKey()
			.endAt( oldestMessageId )
			.limitToLast( noOfMessagesToLoad );
		getThreadRef.once( 'value', ( dataSnapshot ) => {
				const	messages = [];

				if(dataSnapshot.exists()){
					let nextMessage, messageInstance;
					const messageList = dataSnapshot.val();
					if( dataSnapshot.numChildren() < noOfMessagesToLoad ){
						replyActions.allRepliesLoaded();
					}
					for(let key in messageList){
						if(messageList.hasOwnProperty(key)){
							nextMessage = messageList[key];
							nextMessage.id = key;
							nextMessage.ref = threadRef.child( key );
							nextMessage.bridgeId = bridgeId;
							messageInstance = Messages.createMessageInstance( nextMessage );

							messages.push( messageInstance );
						}
					}
					replyActions.getPreviousReplies( messages );
					return next( null );
				}
				return next( null );
			}, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		});
		getThreadRef.on('child_changed', _onChildChangedCallback, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		});
		getThreadRef.on('child_removed', _onChildRemovedCallback, ( error ) => {
			//How to pass the error to next only on setup??
			//return next( error );
		});

		return next( null );

	}

	static closeThread( bridgeId, userRef, messageId, next ){

		if(typeof bridgeId !== 'string'){
			const error = new Error('bridgeId is required and should be a string');
			if (next){
				return next( error );
			}else{
				throw error;
			}
		}

		const threadRef = repliesRef.child( bridgeId + '/' + messageId);
		threadRef.off();

		const userReplyIndexRef = userRef.child(
			'replies/' + bridgeId + '/' + messageId
		)
		_updateUserReplyIndex(  userReplyIndexRef, 'close' );

		return next( null );

	}

	static deleteThread( replyThreadPath, next ){
		//replyThreadPath = bridgeId/messageId
		const threadRef = repliesRef.child( replyThreadPath );
		threadRef.remove( ( err ) => {
			next( err );
		} );
	}
}
