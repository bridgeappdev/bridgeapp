'use strict';

import Firebase from 'firebase';

const refUrl = "https://br1dg3co.firebaseio.com";
const ref = new Firebase( refUrl );

let serverOffset;
const offsetRef = ref.child(".info/serverTimeOffset");
offsetRef.on( "value", ( snap ) => {
	serverOffset = snap.val();
});

export default class FBase {

		static getRef(){
			return ref;
		}
		static buildRef( path ){
			return ref.child( path );
		}

		getEstimatedServerTimeMs() {

			return new Date().getTime() + serverOffset;
		};
}
