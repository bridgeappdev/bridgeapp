'use strict';

import Firebase from 'firebase';

import FBase from '../firebase/fbase';

import authActions from '../../actions/AuthActions';
import authStore from '../../stores/AuthStore';

function _authListenerCallback( authData ) {

	if ( authData ) {
		const uidindexRef = FBase.buildRef('uidindex');
		uidindexRef.child( authData.uid ).once('value', ( dataSnapshot ) => {

			if ( dataSnapshot.exists() ) {
				authData.fbKeyId = dataSnapshot.val();
			}else{
				authData.fbKeyId = null;
			}

			console.log(
				"User " + authData.uid + " is logged in with " + authData.provider,
				authData
			);

			authActions.login( authData );
		});

	} else {

		console.log( "User is logged out" );
		if( authStore.isLoggedIn() ){

			window.location.replace( window.location.origin );

		}else{

			authActions.logout();

		}
	}
}

export default class Auth {

	static authUser(){
		FBase.getRef().onAuth( _authListenerCallback );
	}

	static logout(){

		FBase.getRef().unauth();
		return authActions.logout();

	}

	static login( email, password, next ) {

		FBase.getRef().authWithPassword(
			{
				"email":email,
				"password":password
			},
			function( error, authData ) {
				if (error) {
			   console.log("Login Failed!", error);
				 switch (error.code){
					 case "INVALID_USER":
						 error.message = 'The username or password was not recognized. Please try again.';
						 break;
					 default:
						 error.message = "There was an error logging in. Try again.";
				 }

				 next(error);
			  } else {
			   console.log("Authenticated successfully with payload:", authData);
				 next(null, authData);
			  }
			}
		);
	}

	static signup( email, password, next ){
		const FBref = FBase.getRef();

		FBref.createUser({
			email: email,
			password: password
		}, function(error, userData) {
			if (error) {
				switch (error.code) {
					case "EMAIL_TAKEN":
						console.log("The new user account cannot be created because the email is already in use.");
						error.message = "Email already in use. Did you forget your password?";
						break;
					case "INVALID_EMAIL":
						console.log("The specified email is not a valid email.");
						error.message = "The specified email is not a valid email.";
						break;
					default:
						console.log("Error creating user:", error);
						error.message = "There was an error. Please try again.";
				}

				return next( error );

			} else {
				console.log("Successfully created user account with uid:", userData.uid);
				return next( null, userData );
			}
		});
	}

	static addUserIdToIndex( authId, uId, next ){
		const uidindexRef = FBase.buildRef('uidindex/' + authId );
		uidindexRef.set( uId, ( err ) => {
			if( err ){
				console.log('Error setting index for new user ', err);
			}
			if( typeof next === 'function'){
				next( err );
			}
		} );
	}

}
