'use strict';

import Firebase from 'firebase';

import Bridges from '../bridges/Bridges';
import Auth from '../auth/Auth';

import profileActions from '../../actions/ProfileActions';
import bridgeActions from '../../actions/BridgesActions';

/*
{
	"avatar" : "http://res.cloudinary.com/br1dg3co/image/upload/v1458012740/russell/c7mfz80zt4vj5oqyfdwy.jpg",
	"bio" : "Tech Startup http://t.co/Ud9hbQ22wV keeping my hand in the mix developing highly-scalable web apps, mostly in the early hours #newdad !!Tech Startup http://t.co/Ud9hbQ22wV keeping my hand in the mix developing highly-scalable web apps, mostly in the ",
	"email" : "russ@bridgeapp.me",
	"linkedin" : "",
	"name" : "Russell Ormes",
	"tags" : [ "limavady" ],
	"website" : "http://www.chakachat.com/",
	"updatedAt" : 1463020406031,
	"createdAt" : 1463020406031
}
*/

function _parseProfileData( profileData ){
	this._tags = [];

	if( profileData.id ){
		this._id = profileData.id;
	}
	if( profileData.authid ){
		this._authid = profileData.authid;
	}
	if( profileData.ref ){
		this._ref = profileData.ref;
		this._userRef = profileData.ref.parent();
	}
	if( profileData.email ){
		this._email = profileData.email;
	}
	if(profileData.username){
		this._username = profileData.username;
	}

	if(profileData.avatar){
		this._avatar = profileData.avatar;
	}
	if(profileData.bio){
		this._bio = profileData.bio;
	}
	if(profileData.name){
		this._name = profileData.name;
	}
	if(profileData.firstname){
		this._firstname = profileData.firstname;
	}
	if(profileData.lastname){
		this._name = profileData.lastname;
	}
	if(profileData.website){
		this._website = profileData.website;
	}
	if(profileData.linkedin){
		this._linkedin = profileData.linkedin;
	}
	if(profileData.tags){
		this._tags = profileData.tags;
	}

	if(profileData.bridges){
		this._bridgesIndex = profileData.bridges;
	}else{
		this._bridgesIndex = {};
	}
	if(profileData.contacts){
		this._contacts = profileData.contacts;
	}
	if(profileData.replies){
		this._replies = profileData.replies;
	}
	//
	// if(profileData.eventauth){
	// 	this._eventauth = profileData.eventauth;
	// }
	// if(profileData.meetup_me_data){
	// 	this._meetup_me_data = profileData.meetup_me_data;
	// }
}

export default class Profile {

	constructor ( profileData  ){
		_parseProfileData.call( this, profileData );
	}

	getPrivateData(){
		this._getEventIntegrationData();
	//	this._getContacts();
	//	this._getReplies();
	}
	_getEventIntegrationData(){
		const queryref = this._userRef.child( 'eventintegration' );

		queryref.on('value', ( dataSnapshot ) => {
			if( dataSnapshot .exists() ){
				this._eventintegration = dataSnapshot.val();
				return profileActions.updateEventIntegration();
			}
		})

	}

	get id(){
		return this._id;
	}
	get ref(){
		return this._ref;
	}
	get userRef(){
		return this._userRef;
	}

	set avatar( avatarURL ) {
		this._avatar = avatarURL;
	}
	get avatar( ) {
		return this._avatar;
	}

	set bio( bioText ) {
		this._bio = bioText;
	}
	get bio(){
		return this._bio;
	}

	set name( nameText ) {
		this._name = nameText;
	}
	get name() {
		return this._name;
	}
	set firstname( firstnameText ) {
		this._firstname = firstnameText;
	}
	get firstname() {
		return this._firstname;
	}
	set lastname( lastnameText ) {
		this._lastname = lastnameText;
	}
	get lastname() {
		return this._lastname;
	}

	get username() {
		return this._username;
	}
	//TODO: Not allow username to be changed.
	set username( username ) {
		if( !this._username ){
			this._username = username;
		}
	}

	set website( websiteUrl ) {
		this._website = websiteUrl;
	}
	get website() {
		return this._website;
	}
	set linkedin( linkedinUrl ) {
		this._linkedin = linkedinUrl;
	}
	get linkedin() {
		return this._linkedin;
	}

	set createdAt(date){
		this._createdAt = date;
	}
	get createdAt(){
		return this._createdAt;
	}
	set updatedAt(date){
		this._updatedAt = date;
	}
	get updatedAt(){
		return this._updatedAt;
	}

	set tags( newTags ){
		this._tags = newTags;
	}
	get tags(){
		return this._tags;
	}

	get bridges(){
		return this._bridgesIndex;
	}
	get contacts(){
		if( this._contacts ){
			return this._contacts;
		}

		this._getMyContacts( ( err )=>{
			if( err ){
				return console.log('profile.contacts error: ', err);
			} else {
				return this._contacts;
			}
		} );

	}
	get replies(){
		return this._replies;
	}
	get ebauth(){
		if(
			this._eventintegration
			&& this._eventintegration.auth
		){
			return this._eventintegration.auth.eventbrite_auth;
		} else {
			return null;
		}
	}
	get muauth(){
		if(
			this._eventintegration
			&& this._eventintegration.auth
		){
			return this._eventintegration.auth.meetup_token;
		} else {
			return null;
		}
	}
	get mumedata(){
		if(
			this._eventintegration
			&& this._eventintegration.meetup_me_data
		){
			return this._eventintegration.meetup_me_data;
		} else {
			return null;
		}
	}

/*
	"eventauth" : {
		"eventbrite_auth" : AUTH_KEY,
		"meetup_token" : AUTH_KEY
	}

	set eventauth( dataObj ){
		this._eventauth = dataObj;
	}
*/
/*
	"meetup_me_data" : {
		"city" : "Lima",
		"country" : "pe",
		"id" : 178991782,
		"joined" : 1413923125000,
		"lang" : "en_US",
		"lat" : -12.069999694824219,
		"link" : "http://www.meetup.com/members/178991782",
		"lon" : -77.05000305175781,
		"name" : "Rein Petersen",
		"photo" : {
			"highres_link" : "http://photos3.meetupstatic.com/photos/member/e/5/e/4/highres_236758852.jpeg",
			"photo_id" : 236758852,
			"photo_link" : "http://photos1.meetupstatic.com/photos/member/e/5/e/4/member_236758852.jpeg",
			"thumb_link" : "http://photos3.meetupstatic.com/photos/member/e/5/e/4/thumb_236758852.jpeg"
		},
		"status" : "active",
		"topics" : [ {
			"id" : 827,
			"name" : ".NET",
			"urlkey" : "dotnet"
			}, {
			"id" : 2260,
			"name" : "C#",
			"urlkey" : "csharp"
		}],
		"visited" : 1463108949000
	}
	set meetup_me_data( data ){
		this._meetup_me_data = data;
	}
	get meetup_me_data(){
		return this._meetup_me_data;
	}
*/

	toObject(){

		const returnObj = {};

		// if( this._authid ){
		// 	returnObj.authid = this._authid;
		// }

		if( typeof this._email !== 'undefined' ){
			returnObj.email = this._email;
		}
		if( typeof this._username !== 'undefined' ){
			returnObj.username = this._username;
		}

		if(typeof this._avatar !== 'undefined' ){
			returnObj.avatar = this._avatar;
		}
		if(typeof this._bio !== 'undefined' ){
			returnObj.bio = this._bio;
		}
		if(typeof this._name !== 'undefined' ){
			returnObj.name = this._name;
		}
		if(typeof this._firstname !== 'undefined' ){
			returnObj.firstname = this._firstname;
		}
		if(typeof this._lastname !== 'undefined' ){
			returnObj.lastname = this._lastname;
		}
		if(typeof this._website !== 'undefined' ){
			returnObj.website = this._website;
		}
		if(typeof this._linkedin !== 'undefined' ){
			returnObj.linkedin = this._linkedin;
		}
		if(typeof this._tags !== 'undefined' ){
			returnObj.tags = this._tags;
		}

/*
		if(this._bridgesIndex){
			returnObj.bridges = this._bridgesIndex;
		}
		if(this._contacts){
			returnObj.contacts = this._contacts;
		}
		if(this._replies){
			returnObj.replies = this._replies;
		}
		if(this._eventauth){
			returnObj.eventauth = this._eventauth;
		}
		if(this._meetup_me_data){
			returnObj.meetup_me_data = this._meetup_me_data;
		}
*/
		return returnObj;

	}

	create( next ){
		const _self = this;
		const Profiles = require('./Profiles').default;

		this._userRef = Profiles.getRef().push();
		this._ref = this._userRef.child('profile');
		this._userRef.set(
			{
				"authid": this._authid,
				"profile": this.toObject()
			},
			( err ) => {
				if( err ){
					//TODO How to recover?
					return next( err );
				}else{

					Auth.addUserIdToIndex(
						_self._authid,
						_self._userRef.key(),
						( err ) => {
							if( err ){
								//what to do here?
								return next( err )
							}
							_self._id = _self._userRef.key();
							return next( null );
						}
					);
					Profiles.addUsernameToIndex(
						_self._username,
						( err ) => {
							if( err ){
								//what to do here?
								return next( err )
							}
							return next( null );
						}
					);
				}
			}
		);
	}

	update( next ){
		const profileData = this.toObject();
		profileData.updatedAt = Firebase.ServerValue.TIMESTAMP;

		this._ref.update( profileData, ( err ) => {
			if( err ) {
				console.log('Error updating profile ==>> ', err);
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	setEventbriteAuthId( EventBriteCode, next ) {

		const queryref = this._userRef.child( 'eventintegration/auth/eventbrite_auth' );

		if (EventBriteCode.length > 0) {
			queryref.set(EventBriteCode, function(){
				next();
			});
		}
	}

	setMeetupAccessToken( meetupCode,	next ){

		const queryref = this._userRef.child( 'eventintegration/auth/meetup_token' );

		queryref.set( meetupCode, function( error ){
			next();
		});
	}

	setMeetupMe( meData, next){

		const queryref = this._userRef.child( 'eventintegration/meetup_me_data' );
		queryref.set( meData, function( error ){
			next( error );
		});
	}

	merge( newProfileData ){ //json object, not profile.
		if( newProfileData.constructor.name === "Profile" ) {
			newProfileData = newProfileData.toObject();
		}
		return _parseProfileData.call( this, newProfileData );
	}

	joinBridge( bridgeId, next ){
		this._userRef.child( 'bridges/' + bridgeId ).set(
			{
				lastVisit: Firebase.ServerValue.TIMESTAMP,
				visitCount: 1
			},
			( err ) => {
				if( typeof next === 'function'){
					return next( err );
				}
			}
		)
	}

	toggleBridgeStar( bridgeId, next ){
		if( this._bridgesIndex[bridgeId] ){
			const isFavorite = this._bridgesIndex[bridgeId].isFavorite;
			const myBridgeIndexRef = this._userRef.child(
				'/bridges/' + bridgeId
			);
			myBridgeIndexRef.update(
				{
					"isFavorite": !isFavorite
				},
				( err ) => {
					if( err ) {
						console.log('Error setting favorite');
					}
					if( typeof next === 'function' ){
						return next( err );
					}
				}
			);
		}else{
			const message = "Bridge is not in your bridgeIndex";
			console.log( message );
			if( typeof next === 'function' ){
				return next( new Error( message ) );
			}
		}

	}

	getMyBridges ( next ) {
		const _self = this;

		const myBridgesIndexRef = this._userRef.child( '/bridges' );
		myBridgesIndexRef.once('value', ( dataSnapshot ) => {
			_self._bridgesIndex = dataSnapshot.val();

			const noOfBridges = dataSnapshot.numChildren();
			let iterations = 1;
			let lastBridgeKey;
			dataSnapshot.forEach( ( childSnapshot ) => {
				lastBridgeKey = childSnapshot.key();
				Bridges.getBridgeById( lastBridgeKey, ( err, received ) => {
					if(iterations === noOfBridges){
						bridgeActions.bridgesLoaded();
					}
					iterations+=1;
				} )
			} );

			//myBridgesIndexRef.orderByKey().startAt( lastBridgeKey ).on(
			myBridgesIndexRef.on(
				'child_added',
				( dataSnapshot ) =>{
					const bridgeId = dataSnapshot.key();
					if(!this._bridgesIndex[bridgeId]){
						Bridges.getBridgeById( bridgeId, ( err, received ) => {
							if( received ){
								this._bridgesIndex[bridgeId] = dataSnapshot.val();
								return profileActions.updateBridgeIndex();
							}
						} );
					}
				}
			);

			myBridgesIndexRef.on('child_changed', ( dataSnapshot ) => {
				const bridgeId = dataSnapshot.key();
				this._bridgesIndex[bridgeId] = dataSnapshot.val();
				return profileActions.updateBridgeIndex();
			})

			if( typeof next === 'function' ){
				return next( null );
			}

			return void 0;

		});

	}

	_getMyContacts( next ){
		const _self = this;

		const myContactsRef = this._userRef.child( '/contacts' );
		myContactsRef.on('value', ( snapshot )=>{
			this._contacts = snapshot.val();
			return profileActions.updateContactIndex();
		})

		next( null );

	}

	hasConnection( uId, next ){
		const _self = this;

		if (this._contacts){
			return next ( this._contacts[uId] );
		}

		const myContactRef = this._userRef.child( '/contacts/' + uId );
		myContactsRef.once('value', ( err, snapshot )=>{
			if( err ){
				return next( err );
			}
			return next( null, snapshot.val() );
		})

	}

	addDisconnectListener( bridgeId, next ){

		if( this._bridgesIndex[bridgeId] ){
			const disconnectRef =
				this._userRef.child('bridges').child(bridgeId);

				disconnectRef.onDisconnect().update(
					{
						"lastVisit":Firebase.ServerValue.TIMESTAMP,
						"visitCount": this._bridgesIndex[bridgeId].visitCount + 1
					},
					(err) => {
						//return next(err);
						if(err){
							console.log(err);
						}
				});

				return next(null);
			}else{
				const message = "Bridge is not in your bridgeIndex";
				console.log( message );
				if( typeof next === 'function' ){
					return next( new Error( message ) );
				}
			}

	}

	removeDisconnectListener( bridgeId, next ){

		const disconnectRef =
			this._userRef.child('bridges').child( bridgeId );
			disconnectRef.onDisconnect().cancel( ( err ) => {
				//return next(err);
				console.log(err);
			});

		return next(null);

	}

	setBridgeLastVisited( bridgeId, next ){
		if( this._bridgesIndex[bridgeId] ){
			this._userRef.child( 'bridges' ).child( bridgeId ).update(
				{
					"lastVisit":Firebase.ServerValue.TIMESTAMP,
					"visitCount": this._bridgesIndex[bridgeId].visitCount + 1
				},
				( err ) => {
					if( err ) {
						console.log('Error setting last visited timestamp');
					}
					if( typeof next === 'function' ){
						return next( err );
					}
				}
			);
		}else{
			const message = "Bridge is not in your bridgeIndex";
			console.log( message );
			if( typeof next === 'function' ){
				return next( new Error( message ) );
			}
		}
	}

	addNewReplyToIndex( bridgeId, messageId, next ){
		const myRepliesIndexRef = this._userRef.child( '/replies' );
		myRepliesIndexRef.child( bridgeId + '/' + messageId).set(
			{
				lastViewed: Firebase.ServerValue.TIMESTAMP,
				viewCount: 1
			}
		)
	}
	removeReplyFromIndex( bridgeId, messageId, next ){
		const myRepliesIndexRef = this._userRef.child( '/replies' );
		myRepliesIndexRef.child( bridgeId + '/' + messageId).remove(
			( err ) => {
				next( err )
			}
		);
	}

}
