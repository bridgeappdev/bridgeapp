'use strict';

import FBase from '../firebase/fbase';

import profileActions from '../../actions/ProfileActions';
import profileStore from '../../stores/ProfileStore';

import Profile from './Profile';

import httpUtil from '../../lib/common/httpUtil';
import config   from '../../../config';


function _parseProfileData( dataSnapshot ) {

	if ( dataSnapshot.exists() ) {

		const profileid = dataSnapshot.ref().parent().key();

		const profileData = dataSnapshot.val();
		profileData.id = profileid;
		profileData.ref = dataSnapshot.ref();

		return new Profile( profileData );

	} else {

		return null; //null or undefined

	}
};

//const profileRef = FBase.buildRef('users');
const profileRef = FBase.buildRef('members');

export default class Profiles {

	static getRef(){
		return profileRef;
	}

	static getProfileByUsername( username, next ){

		const queryref = profileRef.orderByChild('username').equalTo( username );

		queryref.once('value', ( snapshot ) => {
			const profile = _parseProfileData( snapshot );
			next(null, profile);
		}, function(error) {
			console.error(
				"PROFILE::getProfileByUsername ==> Error retrieving Profile",
				error
			);
			next(error);
		});
	}

	static getProfileById( uId, next ){

		const queryref = profileRef.child( uId + '/profile' );
		queryref.on('value', ( dataSnapshot ) => {
			let profile;
			if ( dataSnapshot.exists() ) {

				profile = _parseProfileData( dataSnapshot );
				return next( null, profile );

			}else{
				console.log( "Didn\'t find a profile with id ", uId );
				return next( null, null );
			}
		}, function( error ) {
			console.log("FIREAPI.PROFILE::getMyProfile ==> Error retrieving Profile ", error);
			return next( error );
		});
	}

	static getMyProfileData( authData ){

		const _self = this;

		if( authData.fbKeyId ){

			Profiles.getProfileById( authData.fbKeyId, ( err, profile ) => {
				if( err ){
					return profileActions.updateProfile( profile );
				} else {
					profile.getPrivateData();
					return profileActions.updateProfile( profile );
				}
			} );

		}else{

			const profile = Profiles.getProfileFromAuthData( authData );
			return profileActions.updateProfile( profile );

		}
	}

	static checkUsernameAvailable( candidateName, next ){

		const postData = {
			'candidate': candidateName
		};
		const options = {
	  	hostname: config.inviteConfig.hostname,
	  	port: config.inviteConfig.port,
	  	path: '/profile/checkUsername',
	  	method: 'POST',
	  	headers: {
	   	 'Content-Type': 'application/json',
	  	}
		};

		httpUtil.httpRequest(options, postData, ( err, result ) => {
			return next( err, result.result );
		})
	}

	static addUsernameToIndex( username, next ){

		const postData = {
			'username': username
		};
		const options = {
	  	hostname: config.inviteConfig.hostname,
	  	port: config.inviteConfig.port,
	  	path: '/profile/addUsername',
	  	method: 'POST',
	  	headers: {
	   	 'Content-Type': 'application/json',
	  	}
		};

		httpUtil.httpRequest(options, postData, ( err, result ) => {
			return next( err, result.result );
		})
	}

	static getProfileFromAuthData( authData ){
		let profile = {},
		data = {};

		if (authData.provider) {
			switch (authData.provider) {
				case "twitter":
				data = authData.twitter;
				profile = {
					"authid": authData.uid,
					"name": data.displayName,
					"username": data.username,
					"avatar": data.profileImageURL,
					"bio": data.cachedUserProfile.description,
				};
				break;

				case "password":
				data = authData.password;
				var name = data.email.substring(0, data.email.lastIndexOf("@"));
				profile = {
					"authid": authData.uid,
					"name": name,
					"username": name,
					"avatar": data.profileImageURL,
					"email": data.email
				};
				break;

				default:
				profile = {
					"authid": authData.uid
				};
				break;
			}
		}

		return new Profile( profile );

	}

	static removeContact(
		myRef,
		myId,
		contactRef,
		contactId,
		next
	){

		myRef.child('contacts').child( contactId ).remove((err)=>{
			if (err) {
				return next(err);
			} else {
				return contactRef.remove((err)=>{
					return next(err);
				});
			}
		})
	}

	static updateContact( actionType, contactId, next ){

		let newValue, contactValue;
		switch ( actionType ) {
			case 1:
			console.log('FollowButton::_handleFollowClick ==> Friend');
			newValue = contactValue = 3; //Friend
			break;
			case 2:
			console.log('FollowButton::_handleFollowClick ==> Un-Follow');
			newValue = 0; //No contact
			break;
			case 3:
			console.log('FollowButton::_handleFollowClick ==> Un-Friend');
			newValue = 1; //Follower
			contactValue = 2;
			break;
			default:
			console.log('FollowButton::_handleFollowClick ==> Follow');
			newValue = 2; //Following
			contactValue = 1;
		}

		const profile = profileStore.getProfile();
		const myId = profile.id;
		const myRef = profile.userRef;
		const contactRef = profileRef.child( contactId + '/contacts/' + myId );
		if (!newValue) {
			return this.removeContact( myRef, myId, contactRef, contactId, next );
		}

		myRef.child('contacts').child( contactId ).set(newValue, (err) => {
			if(err){
				return next(err)
			}else{
				contactRef.set( contactValue, (err) => {
					if(err){
						return next(err)
					}else{
						return next(null);
					}
				})
			}
		});
	}
}

Profiles.userProfileLoaded = false;
