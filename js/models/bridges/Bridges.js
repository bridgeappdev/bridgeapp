'use strict';

import FBase from '../firebase/fbase';

import bridgeActions from '../../actions/BridgesActions';
import profileActions from '../../actions/ProfileActions';

import ChatBridge from './ChatBridge';
import EventBridge from './EventBridge';

import Messages  from '../messages/Messages';
import Profiles  from '../profiles/Profiles';
import Profile   from '../profiles/Profile';
import SearchAPI from '../../lib/SearchAPI';

const bridgeRef = FBase.buildRef('bridges');
export default class Bridges {

	static getRef(){
		return bridgeRef;
	}

	static buildBridge( bridgeData ){

		let newBridge = null;
		if( !bridgeData.ref ){
			bridgeData.ref = FBase.buildRef(
				'bridges/' + bridgeData.id
			);
		}
		switch (bridgeData.type) {
			case "CHAT":
			case "chat":
				newBridge = new ChatBridge( bridgeData );
				break;
			case "EVENT":
			case "event":
				newBridge = new EventBridge( bridgeData );
				break;
			default:

		}

		return newBridge;
	}

	static createHomeBridge( profile, next ){

		const username = profile.username;
		const uId = profile.id;

		const bridgeId = username + ',home';
		const homeBridgeRef = bridgeRef.child( bridgeId );
		const member = {};
		member[ uId ] = true;
		const messagesMeta = {
			lastMessageDate: Firebase.ServerValue.TIMESTAMP,
			count: 0
		};
		const bridgeData = {
			'id': bridgeId,
			'title': 'home channel for ' + username,
			'name': 'home',
			'desc': 'home feed',
			'members': member,
			'visibility': 'private',
			'messagesMeta': messagesMeta
		};

		const homeBridge = new ChatBridge( bridgeData );
		homeBridge.create( ( err ) => {
			if ( err ) {
				console.log('Synchronization failed');
				return next( err );
			} else {
				console.log('Synchronization succeeded');
				profile.joinBridge( homeBridge.id, ( err ) => {
					return next( err );
				});
			}
		});

	}

	static saveNewBridge( profile, bridgeData, next ){
		const myUsername = profile.username;
		const myUId = profile.id;

		bridgeData.members = {};
		bridgeData.members[myUId] = true;

		const generateRef = bridgeRef.push();
			bridgeData.id = myUsername + ',' + generateRef.key();

		bridgeData.messagesMeta = {
			lastMessageDate: Firebase.ServerValue.TIMESTAMP,
			count: 0
		};

		let newBridge;
		switch ( bridgeData.type ) {
			case 'CHAT':
				newBridge = new ChatBridge( bridgeData );
				break;
			case 'EVENT':
				newBridge = new EventBridge( bridgeData );
				break;
			default:

		}
		newBridge.create( ( err ) => {
			if ( err ) {
				console.log('Synchronization failed');
				return next( err );
			} else {
				console.log('Synchronization succeeded');
				profile.joinBridge( newBridge.id, ( err ) => {
					return next( err );
				});
			}
		});

	}

	static getBridgeById( bridgeId, next ){

		bridgeRef.child( bridgeId ).on( 'value' , ( dataSnapshot ) => {
			if(!dataSnapshot.exists()){
				console.log(
					"Didn't find a bridge with id "
					+ bridgeId
				);

				if(typeof next === 'function'){
					return next( null, newBridge );
				}

				return void 0;
			}

			const bridgeData = dataSnapshot.val();
			bridgeData.id = bridgeId;
			bridgeData.ref = dataSnapshot.ref();
			let newBridge;
			switch (bridgeData.type) {
				case "CHAT":
				case "chat":
					newBridge = new ChatBridge( bridgeData );
					break;
				case "EVENT":
				case "event":
					newBridge = new EventBridge( bridgeData );
					break;
				default:

			}

			bridgeActions.receiveNewBridge( newBridge );

			if(typeof next === 'function'){
				return next( null, newBridge );
			}

			return void 0;

		});
	}

	static getBridgeByPath( bridgeParams, next ){

		const _self = this;

		bridgeRef
			.orderByKey()
				.startAt(bridgeParams.username).endAt(bridgeParams.username+"\uf8ff")
					.once('value',
					function(dataSnapshot)
		{
			if(!dataSnapshot.exists()){
				return next(new Error('No such Bridge'));
			}

			const bridgesData = dataSnapshot.val();
			const bridgeName = bridgeParams.bname;
			const bridgeIds = Object.keys( bridgesData );

			let bridgeId = null;
			let requestedBridge = null;

			for( let index = 0; index<bridgeIds.length; index++ ){
				if( bridgesData[ bridgeIds[ index ] ].name === bridgeName ){
					requestedBridge = bridgesData[ bridgeIds[ index ] ];
					requestedBridge.id = bridgeIds[ index ];
					break;
				}
			}

			if( requestedBridge ){
				const receivedBridge = Bridges.buildBridge( requestedBridge );
				next( null, receivedBridge );

				return _self.getBridgeById( receivedBridge.id );
			}
			return next( new Error('No such Bridge') );

		}, function(err){
			//TODO Define BApp errors to manage differnet error types.
			console.log('You don\'t have permission: ', err);
			return next( err );
		})
	}

	static search( bridgeTagList, next ){
		if (bridgeTagList) {
			const arrayOfTags = bridgeTagList.slice(1).split(',');
			console.log('search for these tags:', arrayOfTags);
			return SearchAPI.searchBridgesByTag(
				arrayOfTags,
				function(error, result){
					if(error){
						console.log('oppps ', error);
					}	else {
						next(null, result);
					}
				}
			);
		}else{
			return this.getBridgesForSearch( 20, next );
		}
	}

	static getBridgesForSearch( noOfResults, next ){

		const limit = noOfResults ? noOfResults : 20;
		bridgeRef
			.orderByChild('visibility').equalTo('public')
			.limitToFirst( limit )
			.once('value', function( data ){
				console.log('Bridge search data is: ', data.val());
				const bridges = data.val();
				const bridgeArray = [];
				for( let bridgeId in data.val()){
					bridges[bridgeId].id = bridgeId;
					switch ( bridges[bridgeId].type ) {
						case 'chat':
						case 'CHAT':
								bridgeArray.push( new ChatBridge( bridges[bridgeId] ) );
							break;
						case 'event':
						case 'EVENT':
								bridgeArray.push( new EventBridge( bridges[bridgeId] ) );
							break;
						default:

					}
				}
				next( null, bridgeArray );
		}, function( err ){
			next( err );
		});
	}

	static openBridge( bridge, profile ){
		profile.addDisconnectListener( bridge.id, ( err ) => {
			if( err ) {/* blah blah*/}
		});

		return bridgeActions.openBridge( bridge );
	}

	static closeBridge( bridgeId, userProfile, next ){

		userProfile.setBridgeLastVisited( bridgeId, ( err ) => {
			if(err){/* blah blah*/}
		});

		userProfile.removeDisconnectListener( bridgeId, function( err ){
			if(err){/* blah blah*/}
		});

		Messages.removeListener( bridgeId, ( err ) => {
			const breakhere = null;
			if(err){/* blah blah*/}
		});

		return next( null );
	}

	static checkNameAvailable( username, candidateName, next ){

		bridgeRef.orderByKey()
			.startAt( username ).endAt( username + "\uf8ff" )
				.once( 'value', function( dataSnapshot ){

			if( !dataSnapshot.exists() ){
				return next( null, true );
			}

			const bridgesData = dataSnapshot.val(),
			bridgeIds = Object.keys( bridgesData );

			for( let index = 0; index<bridgeIds.length; index++ ){
				if( bridgesData[bridgeIds[index]].name === candidateName ){
					return next(null, false);
				}
			}

			return next( null, true );
		}, function( err ){
			return next( err );
		})
	}

	checkEBEventID( eId, next ){
		const ebEventIndexRef = FBase.buildRef('ebbridgeindex/' + eId );
		ebEventIndexRef.once( 'value', ( snapshot )=>{
			return next( snapshot.exists() );
		});
	}

}
