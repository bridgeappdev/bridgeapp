import BaseBridge from './BaseBridge';

export default class ChatBridge extends BaseBridge {

	constructor ( bridgeDataObject ){

		super( bridgeDataObject );

		this._type = "CHAT";

	}

	toObject(){
		return super.toObject();
	}
}
