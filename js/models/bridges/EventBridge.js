'use strict';

import FBase from '../firebase/fbase';
import BaseBridge from './BaseBridge';

export default class EventBridge  extends BaseBridge {

	constructor ( bridgeDataObject ){

		super( bridgeDataObject );

		this._type = "EVENT";

		if( bridgeDataObject.date_and_time ){
			this._date_and_time = bridgeDataObject.date_and_time
		}
		if( bridgeDataObject.ebdata ){
			this._ebdata = bridgeDataObject.ebdata
		}
		if( bridgeDataObject.mudata ){
			this._mudata = bridgeDataObject.mudata
		}
		if( bridgeDataObject.venue ){
			this._venue = bridgeDataObject.venue
		}

	}

	set date_and_time( dateAndTime ){
		this._date_and_time = dateAndTime;
	}
	get date_and_time(){
		return this._date_and_time;
	}

	set venue ( eventVenue ){
		this._venue = eventVenue;
	}
	get venue(){
		return this._venue
	}

	set ebdata( eventBriteData ){
		this._ebdata = eventBriteData;
	}
	get ebdata(){
		return this._ebdata
	}
	set mudata( meetupData ){
		this._mudata = meetupData;
	}
	get mudata(){
		return this._mudata
	}

	toObject(){

		const returnObj = super.toObject();

		if( this._venue ){
			returnObj.venue = this._venue;
		}
		if( this._ebdata ){
			returnObj.ebdata = this._ebdata;
		}
		if( this._date_and_time ){
			returnObj.date_and_time = this._date_and_time;
		}

		return returnObj;

	}

	create( next ){

		if( this._ebdata && this._ebdata.id ){
			this._updateEBIndex( this._ebdata.id );
		} else if( this._mudata && this._mudata.id ){
			this._updateMUIndex( this._mudata.id );
		}
		return super.create( next );
	}

	_updateEBIndex( eventID ){

		return FBase.buildRef( 'ebbridgeindex/' + eventID ).set(
			//this._id
			this._id.slice(0, this._id.indexOf(',')) + '/' + this._name
		);
	}
	_updateMUIndex( eventID ){

		return FBase.buildRef( 'mubridgeindex/' + eventID ).set(
			this._id
		);
	}

}
