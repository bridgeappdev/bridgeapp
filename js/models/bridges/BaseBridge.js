/*
* BaseBridge class
*
*/
"use strict";

import FBase from '../firebase/fbase'

export default class Bridge {

	constructor( dataObj ){

		if( dataObj.id ){
			this._id = dataObj.id;
			this._path = dataObj.id.slice(
				0, dataObj.id.indexOf(',')
			) + '/' + dataObj.name;
		}else{
			throw new TypeError(
				'Cannot create a new Bridge instance without passing an id.'
			);
		}

		if( dataObj.ref ){
			this._ref = dataObj.ref;
		}

		if( dataObj.createdAt ){
			this._createdAt = dataObj.createdAt;
		}

		if( dataObj.messagesMeta ){
			this._messagesMeta = dataObj.messagesMeta;
		}

		if( dataObj.desc ){
			this._desc = dataObj.desc;
		}
		if( dataObj.lastMessage ){
			this._lastMessage = dataObj.lastMessage;
		}
		if( dataObj.members ){
			this._members = dataObj.members;
		}
		if( dataObj.name ){
			this._name = dataObj.name;
		}else{
			throw new TypeError(
				'Cannot create a new Bridge instance without passing a bridge name.'
			);
		}
		if( dataObj.title ){
			this._title = dataObj.title;
		}else{
			throw new TypeError(
				'Cannot create a new Bridge instance without passing a bridge title.'
			);
		}
		if( dataObj.tags ){
			this._tags = dataObj.tags;
		}
		if( dataObj.img_url ){
			this._img_url = dataObj.img_url;
		}
		if( dataObj.visibility ){
			this._visibility = dataObj.visibility;
		}else{
			throw new TypeError(
				'Cannot create a new Bridge instance without setting public or private'
			);
		}

	}

	get id() {
		return this._id;
	}

	get ref() {
		return this._ref;
	}

	get createdAt() {
		return this._createdAt;
	}

	get messagesMeta() {
		return this._messagesMeta;
	}

	get name() {
		return this._name;
	}

	get title() {
		return this._title;
	}
	set title( bridgeTitle ){
		this._title = bridgeTitle;
	}

	get desc() {
		return this._desc;
	}
	set desc( bridgeDesc ){
		this._desc = bridgeDesc;
	}

	get type() {
		return this._type;
	}

	get visibility() {
		return this._visibility;
	}

	get members() {
		return this._members;
	}

	get tags() {
		return this._tags;
	}
	set tags( tagList ){
		this._tags = tagList;
	}

	get img_url(){
		return this._img_url;
	}
	set img_url( url ){
		this._img_url = url;
	}

	get lastMessage() {
		//return this._lastMessage;
		return this._messagesMeta.lastMessageDate;
	}

	get path() {
		//return this._id.slice(0, this._id.indexOf(',')) + '/' + this._name;
		return this._path;
	}
	get isHome() {
		return this._path.split('/')[1] === 'home';
	}

	toObject(){

		const returnObj = {};

		// if( this._id ){
		// 	returnObj.id = this._id;
		// }
		if( this._createdAt ){
			returnObj.createdAt = this._createdAt;
		}
		if(this._desc){
			returnObj.desc = this._desc;
		}
		if(this._img_url){
			returnObj.img_url = this._img_url;
		}
		if(this._members){
			returnObj.members = this._members;
		}
		if(this._name){
			returnObj.name = this._name;
		}
		if(this._title){
			returnObj.title = this._title;
		}
		if(this._type){
			returnObj.type = this._type;
		}
		if(this._visibility){
			returnObj.visibility = this._visibility;
		}
		if(this._tags){
			returnObj.tags = this._tags;
		}
		if( this._messagesMeta ) {
			returnObj.messagesMeta = this._messagesMeta;
		}

		return returnObj;

	}

	isMember ( uid ){

		if(!this._members || !uid) {
			return false;
		}

		return this._members.hasOwnProperty( uid );

	}

	isOwner ( uid ){

		if(	!uid	) {
			return false;
		}

		return this._owner === uid
		|| this._id.slice( 0, this._id.indexOf(',') ) === uid;

	}

	create( next ){
		const _self = this;

		this._createdAt = Firebase.ServerValue.TIMESTAMP;
		this._ref = FBase.buildRef('bridges').child( this._id );
		this._ref.set( this.toObject(), ( err ) => {
			if( err ){}
			return next( err );
		});

	}

	update( next ){
		if( !this._ref ){ return void 0 }

		this._updatedAt = Firebase.ServerValue.TIMESTAMP;
		this._ref.update( this.toObject(), ( err ) => {
			if( err ) {
				console.log('Error updating message ==>> ', err);
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	updateLastMessageDate(
		next
	){
		const setRef = this._ref.child('messagesMeta');
		setRef.transaction( function( metaData ) {

			if( metaData ){

				metaData.lastMessageDate = Firebase.ServerValue.TIMESTAMP;
				metaData.count = metaData.count + 1;
				return metaData;

			} else {

				return {
					lastMessageDate: Firebase.ServerValue.TIMESTAMP,
					count: 1
				}
			}
		}, function(error, committed, snapshot) {
			if (error) {
				console.log( error );
				//return next(error);
			}
			console.log(snapshot.val());
			//return next(null, snapshot.val());
		});
	}

	addMemberToBridge( uId, next ){
		const newMember = {};
		newMember[uId] = true;
		this._ref.child( 'members' ).update(
			newMember,
			( err ) => {
				if( err ){}
				return next( err );
			}
		)
	}


}
