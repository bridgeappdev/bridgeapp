'use strict';

import FBase from '../firebase/fbase';

import profileStore from '../../stores/ProfileStore';

import messageActions from '../../actions/MessageActions';

import ChatMessage from './ChatMessage';
import URLMessage from './URLMessage';
import FileMessage from './FileMessage';

import moment from 'moment';
moment().format();

/**
*
* BEGIN HELPER/PRIVATE FUNCTION DEFINITION
*
**/

function _onChildAddedCallback (
	dataSnapshot,
	startAt
) {

	const message = Messages.parseFBData( dataSnapshot );
	if( message.id === this.startAt ){
		return void 0;
	}

	return messageActions.receiveNewMessage( message );

};

function _onChildChangedCallback (
	dataSnapshot
) {

	const message = Messages.parseFBData( dataSnapshot );
	messageActions.updateMessage( message );

};

function _onChildRemovedCallback(
	removedMessageSnapshot
)
{

	const removedMessage = { "id" : removedMessageSnapshot.key() };
	removedMessage.bridge = removedMessageSnapshot.ref().parent().key();
	return messageActions.removeMessage( removedMessage );

};

function _addListener ( bridgeId, startAt ){

	const thisBridgeMessagesRef = messagesRef.child(bridgeId);

	//TODO What happens if the ref does not exist? The listener will not be cancelled.
	if( startAt ){
		thisBridgeMessagesRef.orderByKey().startAt(startAt).on(
			'child_added',
			_onChildAddedCallback.bind( {startAt:startAt} )
		);
	}
	thisBridgeMessagesRef.on('child_changed', _onChildChangedCallback);
	thisBridgeMessagesRef.on('child_removed', _onChildRemovedCallback);

};

/**
*
* BEGIN CLASS DEFINITION
*
**/

const messagesRef = FBase.buildRef( 'messages' );
export default class Messages {

	static getRef(){
		return messagesRef;
	}

	static parseFBData( dataSnapshot ){

		const messageData = dataSnapshot.val();
		messageData.id = dataSnapshot.key();
		//TODO Maybe storing the ref is a bad idea. Store a string path instead.
		messageData.ref = dataSnapshot.ref();
		messageData.bridgeId = dataSnapshot.ref().parent().key();

		return Messages.createMessageInstance( messageData );

	}

	static createMessageInstance( messageData ){

		let newMessage;
		switch ( messageData.type ) {
			case 'CHAT':
				newMessage = new ChatMessage( messageData );
				break;
			case 'URL':
				newMessage = new URLMessage( messageData );
				break;
			case 'FILE':
				newMessage = new FileMessage( messageData );
				break;
			default:
			 console.log("Unknown messsage type from firebase");
		}

		return newMessage;
	}

	static addListener (bridgeId, content, next){

		if(!bridgeId || !content || typeof content === 'function') {
			throw new TypeError('Missing bridge and/or content');
		}

		const contentLength = content.length;
		let startAt = '0';
		if(contentLength > 0){
			startAt = content[content.length - 1].id; //TODO: This won't work if the last index is not length -1
		}
		_addListener(bridgeId, startAt);

		return next( null );

	}

	static removeListener( bridgeId, next ){

		const thisBridgeMessagesRef = messagesRef.child(bridgeId);
		thisBridgeMessagesRef.off(); //Does this throw an error?

		if( typeof next === 'function' ) {
			return next( null );
		} else {
			return void 0;
		}

	}

	static getBridgeMessages( bridge, userId, next ){

		const noOfMessagesToLoad = 50;
		const _self = this;

		// if(!bridge.lastMessage){
		// 	return next(null, []);
		// }

		if(typeof bridge.id !== 'string'){
			const error = new Error('bridgeId is required and should be a string');
			if (next){
				return next( error );
			}else{
				throw error;
			}
		}
		const bridgeId = bridge.id;
		const getMessagesRef = messagesRef.child( bridgeId );
		getMessagesRef.orderByKey().limitToLast( noOfMessagesToLoad )
		.once('value', ( dataSnapshot ) => {

			const	messages = [];

			if(dataSnapshot.exists()){
				let nextMessage, messageInstance;
				const messageList = dataSnapshot.val();
				if( Object.keys( messageList ).length < noOfMessagesToLoad ){
					messageActions.allMessagesLoaded();
				}
				for(let key in messageList){
					if(messageList.hasOwnProperty(key)){
						nextMessage = messageList[key];
						nextMessage.id = key;
						nextMessage.ref = getMessagesRef.child( key );
						nextMessage.bridgeId = bridgeId;
						messageInstance = Messages.createMessageInstance( nextMessage );

						messages.push( messageInstance );
					}
				}
			} else {
				messageActions.allMessagesLoaded();
			}

			messageActions.getMessageList( messages );
			this.addListener( bridge.id, messages, (err) => {
				if( err ){/*blah blah*/}
			});
		});
	}

	static getPreviousMessages(
		bridgeId,
		oldestMessageId,
		noOfMessagesToLoad,
		next
	){
		const getMessagesRef = messagesRef.child( bridgeId );
		getMessagesRef.orderByKey()
			.endAt( oldestMessageId )
			.limitToLast( noOfMessagesToLoad ).once('value', ( dataSnapshot ) => {
				const	messages = [];

				if(dataSnapshot.exists()){
					let nextMessage, messageInstance;
					const messageList = dataSnapshot.val();
					if( Object.keys(messageList).length < noOfMessagesToLoad ){
						messageActions.allMessagesLoaded();
					}
					for(let key in messageList){
						if(messageList.hasOwnProperty(key)){
							nextMessage = messageList[key];
							nextMessage.id = key;
							nextMessage.ref = getMessagesRef.child( key );
							nextMessage.bridgeId = bridgeId;
							messageInstance = Messages.createMessageInstance( nextMessage );

							messages.push( messageInstance );
						}
					}
					messageActions.getPreviousMessages( messages );
					_addListener( bridgeId );
					return next( null );
				}
				return next( null );
			})
	}

	static getMessageById( bridgeId, messageId, next ){

		const getMessageRef = messagesRef.child(
			bridgeId + '/' + messageId
		);

		getMessageRef.on('value', ( dataSnapshot ) => {

			if( dataSnapshot.exists() ){
				const message = Messages.parseFBData( dataSnapshot );

				if( typeof next === 'function' ) {
					return next( null, message );
				} else {
					return void 0;
				}
			} else {
				return profileStore.getProfile().removeReplyFromIndex(
					bridgeId,
					messageId,
					next
				);
			}
		}, ( error ) => {
			if( typeof next === 'function' ) {
				return next( error );
			} else {
				return void 0;
			}
		});
	}

	static removeMessageListenerById(
		bridgeId,
		messageId,
		next
	){

		const messageRef = messagesRef.child(
			bridgeId + '/' + messageId
		);

		messageRef.off( ( err ) => {

			( error ) => {
				if( typeof next === 'function' ) {
					return next( error );
				} else {
					return void 0;
				}
			}
		});
	}

}
