import BaseMessage from './BaseMessage';
import URLMessage from './URLMessage';

export default class ChatMessage extends BaseMessage {

	constructor ( messageObject ){

		messageObject.type = "CHAT";

		if(!messageObject.meta.hasOwnProperty('message')){
			throw new TypeError('Missing message property from messsage data');
		}

		super( messageObject );

	}

	update( next ){
		if( this._type === 'URL'){
			const dataObject = this.toObject();
			dataObject.ref = this._ref;
			const newURLMessage = new URLMessage( dataObject );
			newURLMessage.update( next );
		} else {
			return super.update( next );
		}
	}

}
