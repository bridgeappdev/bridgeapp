import BaseMessage from './BaseMessage';

export default class FileMessage extends BaseMessage {

	constructor ( messageObject ){

		messageObject.type = "FILE";

		if(!messageObject.meta.hasOwnProperty('url')){
			throw new TypeError('Missing message property from messsage data');
		}
		if(!messageObject.meta.hasOwnProperty('name')){
			throw new TypeError('Missing name property from messsage data');
		}
		// if(!messageObject.meta.hasOwnProperty('filetype')){
		// 	throw new TypeError('Missing filetype property from messsage data');
		// }

		super( messageObject );

	}

	update( next ){
		console.warn( "Cannot update a File message. ", this );
		return void 0;
	}


}
