'use strict';

import BaseMessage from './BaseMessage';
import ChatMessage from './ChatMessage';

function _getUrlData( messageurl, next ){

	if( !messageurl ){
		return next( null );
	}

	$.getJSON(
		'http://api.embed.ly/1/oembed?' + $.param({
			url: messageurl,
			key:'dac524884eb249f7b958f553c8c12395',
			maxheight:300
		}),
		next
	).error(() => { next( null ); })
}

export default class URLMessage extends BaseMessage {

	constructor ( messageObject ){

		messageObject.type = "URL";

		//TODO: use json schema
		if(!messageObject.meta.hasOwnProperty('message')){
			throw new TypeError('Missing message property from messsage data');
		}
		if(!messageObject.meta.hasOwnProperty('url')){
			throw new TypeError('Missing url property from messsage data');
		}

		super( messageObject );

	}

	send( bridgeId, next ){

		super.send( bridgeId, ( err ) => {
			if( typeof next === 'function' ){
				if( err ) {
					return next( err );
				} else {
					next( null );
				}
			}

			const NOUPDATEDATE = true;
			_getUrlData( this._meta.url, ( data ) => {
				if( data ){

					this._meta.urlData = data;
					super.update( NOUPDATEDATE );

				} else {

					this._type = 'CHAT';
					delete this._meta.url;
					return super.update( NOUPDATEDATE );

				}
			});
		});
	}

	update( next ){

		super.update( ( err ) => {
			if( typeof next === 'function' ){
				if( err ) {
					return next( err );
				} else {
					next( null );
				}
			}

			const NOUPDATEDATE = true;
			_getUrlData( this._meta.url, ( data ) => {
				if( data ){

					this._meta.urlData = data;
					return super.update( NOUPDATEDATE );

				} else {

					this._type = 'CHAT';
					delete this._meta.url;
					return super.update( NOUPDATEDATE );

				}
			});
		});
	}

}
