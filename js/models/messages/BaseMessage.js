'use strict';

import Messages from './Messages';
import Replies from '../replies/Replies';

import ReactEmoji from 'react-emoji';
import ReactDOMServer from 'react-dom/server';

/**
	{
		"id" : ,
		"type" : ,
		"uId" : ,
		"bridgeId" : ,
		"username : ",
		"avatar" : ,
		"meta" : {
			<Depends on the type property>
		},
		"createdAt" : ,
		"ref" :
	}

**/
function _parseEmoji( text ) {
	const parsedMessage = ReactEmoji.emojify(text);
	let result = '';
	parsedMessage.forEach(function(e){
		if(typeof e === 'object'){
			result += ReactDOMServer.renderToString(e);
		}else{
			result += e;
		}
	});
	return result;
};

export default class BaseMessage {

	constructor ( messageData ){

		if( messageData.hasOwnProperty('uId') ){
			this._uid = messageData.uId;
		} else {
			throw new TypeError( 'Missing user id for Message' );
		}
		if( messageData.hasOwnProperty('username') ){
			this._username = messageData.username;
		} else {
			throw new TypeError( 'Missing username for Message' );
		}
		if( messageData.hasOwnProperty('avatar') ){
			this._avatar = messageData.avatar;
		} else {
			throw new TypeError( 'Missing avatar for Message' );
		}
		if( messageData.hasOwnProperty('meta') ){
			this._meta = messageData.meta;
		} else {
			throw new TypeError( 'Missing meta object for Message' );
		}
		if(!messageData.meta.hasOwnProperty('messageHTML')){
			this._meta.messageHTML = this._getMessageHTML(
				messageData.meta.message
			);
		}
		if( messageData.hasOwnProperty('replyMeta') ){
			this._replyMeta = messageData.replyMeta;
		}
		if( messageData.hasOwnProperty('type') ){
			this._type = messageData.type;
		} else {
			throw new TypeError( 'Missing type argument for Message' );
		}
		if( messageData.hasOwnProperty('id') ){
			this._id = messageData.id;
		}
		if( messageData.hasOwnProperty('bridgeId') ){
			this._bridgeId = messageData.bridgeId;
		}
		if( messageData.hasOwnProperty('ref') ){
			this._ref = messageData.ref;
		}
		if( messageData.hasOwnProperty('createdAt') ){
			this._createdAt = messageData.createdAt;
		}
		if( messageData.hasOwnProperty('updatedAt') ){
			this._updatedAt = messageData.updatedAt;
		}
		if( messageData.hasOwnProperty('isReply') ){
			this._isReply = true;
		}
		if( this._isReply ){
			if( messageData.hasOwnProperty('replyMessageId') ){
				this._replyMessageId = messageData.replyMessageId;
			} else {
				//throw new TypeError( 'Message set as reply. Missing Reply Message Id' );
			}
		}

	}

	get id(){
		return this._id;
	}
	get uId(){
		return this._uid;
	}
	get bridgeId(){
		return this._bridgeId;
	}
	get ref(){
		return this._ref;
	}
	set type( type ){
		this._type = type;
	}
	get type() {
		return this._type;
	}

	get createdAt(){
		return this._createdAt;
	}
	get edited(){
		return !! this._updatedAt;
	}

	get username(){
		return this._username;
	}

	get avatar(){
		return this._avatar;
	}

	set meta( metaData ){
		this._meta = metaData;
	}
	get meta() {
		return this._meta;
	}
	set message( metaObject ){
		if( !metaObject.hasOwnProperty('message') ){
			throw new TypeError('Missing message property from messsage data');
		}
		if(!metaObject.meta.hasOwnProperty('messageHTML')){
			this._meta.messageHTML = this._getMessageHTML(
				messageData.meta.message
			);
		}
	}
	get replyMeta() {
		return this._replyMeta;
	}

	set isReply( value ){
		this._isReply = value;
	}
	get isReply(){
		return this._isReply;
	}
	set replyMessageId( messageId ){
		this._replyMessageId = messageId;
	}

	toObject(){

		const returnObj = {};

			if( this._id ){
				returnObj.id = this._id;
			}

			if( this._uid ){
				returnObj.uId = this._uid;
			}

			if( this._bridgeId ){
				returnObj.bridgeId = this._bridgeId;
			}
			if( this._username ){
				returnObj.username = this._username;
			}

			if( this._avatar ){
				returnObj.avatar = this._avatar;
			}

			if( this._type ){
				returnObj.type = this._type;
			}

			if( this._meta ){
				returnObj.meta = this._meta;
			}

			if( this._updatedAt ){
				returnObj.updatedAt = this._updatedAt;
			}
			if( this._createdAt ){
				returnObj.createdAt = this._createdAt;
			}
			if( this._isReply ){
				returnObj.isReply = true;
			}
			if( this._replyMessageId ){
				returnObj.replyMessageId = this._replyMessageId;
			}

			return returnObj;

	}

	_getMessageHTML ( textToParse ){

		if( !textToParse ){
			return '';
		}

		textToParse = _parseEmoji( textToParse );

		const linkifyHtml = require('linkifyjs/html');
		const options = {
			defaultProtocol: 'http',

			format: function (value, type) {
				if (type === 'url' && value.length > 50) {
					value = value.slice(0, 50) + '…';
				}
				return value;
			},

			linkClass: 'message-link'

		};

		return linkifyHtml( textToParse, options )

	} //_getMessageHTML

	send ( bridgeId, next ){

		if( !bridgeId || typeof bridge === 'function'){
			if( this._ref ){
				return this.update( next )
			} else {
				throw new TypeError('A bridge id is required');
			}
		}

		if(this._isReply){
			return this.sendAsReply( bridgeId, next )
		}

		const saveRef = Messages.getRef().child(bridgeId);

		const messageData = this.toObject();
		messageData.createdAt = Firebase.ServerValue.TIMESTAMP;

		this._ref = saveRef.push( messageData, ( err ) => {
			if( err ) {
				console.log('Error sending message');
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	sendAsReply( bridgeId, next ){

		if( !bridgeId || typeof bridge === 'function'){
			throw new TypeError('A bridge id is required');
		}

		const saveRef =
			Replies.getRef()
				.child( bridgeId +'/' + this._replyMessageId );

		const messageData = this.toObject();
		messageData.createdAt = Firebase.ServerValue.TIMESTAMP;

		this._ref = saveRef.push( messageData, ( err ) => {
			if( err ) {
				console.log('Error sending message');
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	update( next ){
		if( !this._ref ){ return void 0 }
		if( !( next === true ) ){
			this._updatedAt = Firebase.ServerValue.TIMESTAMP;
		}
		this._ref.update( this.toObject(), ( err ) => {
			if( err ) {
				console.log('Error updating message ==>> ', err);
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	updateReplyCount(){
		this._updateReplyCount( this._ref, 'add');
	}

	_updateReplyCount( messageRef, addOrRemove ){

		messageRef.child( 'replyMeta' ).transaction( ( metaData ) => {
			let count = 1;
			if( metaData ){
				switch ( addOrRemove ) {
					case 'minus':
						count = metaData.count - 1
						break;
					default:
						count = metaData.count + 1
				}
			}
			return {
				lastReplyDate : Firebase.ServerValue.TIMESTAMP,
				count : count
			}
		}, function(error, committed, snapshot) {
			if (error) {
				console.log( error );
				//return next(error);
			}
			console.log(snapshot.val());
			//return next(null, snapshot.val());
		});
	}

	removeListeners( next ){
		this._ref.off();
	}

	remove( next ){
		const _self = this;

		this._ref.remove( ( err ) => {
			if( !err ){
				if( _self._isReply ){
					const bridgeId = _self._ref.parent().parent().key();
					const messageRef = Messages.getRef().child(
						bridgeId + '/' + _self._replyMessageId
					);
					_self._updateReplyCount( messageRef, 'minus' );
				} else if( _self.replyMeta ) {

					const threadPath = _self._ref.parent().key() + '/' + _self._id;
					return Replies.deleteThread( threadPath, next );
				}
			}
			next( err )
		});
	}

}
