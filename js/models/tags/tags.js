'use strict';

import FBase from '../firebase/fbase';

const _tagsRef = FBase.buildRef('tags');

export default class Tags {

	static getRef(){
		return _tagsRef;
	}

	static incrementTagCount( tagName, next ){
		const incRef = _tagsRef.child(tagName + '/count');
		incRef.transaction( ( currentCount ) => {
			return currentCount+1;
		}, function(error, committed, snapshot) {
			if (error) {
				return next(error);
			}
			return next(null, snapshot.val());
		});
	}

	static decrementTagCount( tagName, next ){

		const incRef = _tagsRef.child(tagName + '/count');

		incRef.transaction( ( currentCount ) => {
			return currentCount-1;
		}, function(error, committed, snapshot) {
			if (error) {
				return next(error);
			}
			return next(null, snapshot.val());
		});
	}

}
