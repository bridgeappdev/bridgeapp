'use strict';

import BaseEvent from './BaseEvent';

import FBase from '../firebase/fbase';
import meetupActions from '../../actions/MeetupActions';

export default class MUEvent extends BaseEvent{
	constructor( eventData ){

		super( eventData );

		const ref = FBase.buildRef( 'mubridgeindex/' + eventData.id );
		this._checkForBridge( ref );
	}

	_checkForBridge( ref ){
		ref.once( 'value', ( snapshot )=>{
			if( snapshot.exists() ){
				this._hasBridge = snapshot.val();
				meetupActions.updateEventsList();
			}
		});
	}

}
