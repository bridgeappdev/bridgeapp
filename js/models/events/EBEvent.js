'use strict';

import BaseEvent from './BaseEvent';

import FBase from '../firebase/fbase';
import eventBriteActions from '../../actions/EventBriteActions';

export default class EBEvent extends BaseEvent{
	constructor( eventData ){

		super( eventData );

		const ref = FBase.buildRef( 'ebbridgeindex/' + eventData.id );
		this._checkForBridge( ref );
	}

	_checkForBridge( ref ){
		ref.on( 'value', ( snapshot )=>{
			if( snapshot.exists() ){
				this._hasBridge = snapshot.val();
				eventBriteActions.updateEventsList();
			}
		});
	}

}
