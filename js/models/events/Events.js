'use strict';

const EventBrite = require('../../../config').EventBrite;
const rootUrl = EventBrite.rootUrl;

import EBEvent from './EBEvent';

import eventBriteActions from '../../actions/EventBriteActions';

import _ from 'lodash';

function _parseEvents( eventJson ){
	return eventJson.map( ( event )=>{
		event.eventSrc = 'eventbrite';
		return new EBEvent( event );
	} );
}

export default class Events {

static getUserEvents( authid, next ) {

	if ( authid !== undefined && authid.length !== 0 ) {
		const url = "users/me/owned_events/?expand=organizer,venue";
		this._get(url, authid)
			.then(function(json) {
				if (!_.isEmpty(json.error_description)) {
					return next(json.error_description);
				} else {
					eventBriteActions.getUserEvents( _parseEvents( json.events ) );
					return next(null);
				}
			});
		}
	}

	static _get( url, auth ) {
		//TODO: The fetch api is new and not supported in IE or Safari
		//This should be http request or we need a fallback strategy.
		return fetch( rootUrl + url, {
			headers: {
				'Authorization': 'Bearer ' + auth,
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			}
		} )
		.then( function( response ) {
			return response.json();
		})
		.catch( function( err ) {
			return {
				"error_description": err.message + "Resource:" + rootUrl + url
			};
		});
	}

}
