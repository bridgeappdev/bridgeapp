'use strict';

const Meetup = require('../../../config').Meetup;
const rootUrl = Meetup.rootUrl;
const token = Meetup.token;

import meetupActions from '../../actions/MeetupActions';
import MUEvent from './MUEvent';

import _ from 'lodash';

function _parseEvents( eventJson ){
	return eventJson.map( ( event )=>{
		event.eventSrc = 'meetup';
		return new MUEvent( event );
	} );
}

export default class Meetups {
	static getMeData( accessToken, profile, next ){

		if ( accessToken && accessToken.length !== 0 ) {
			let url="/2/member/self/?access_token=" + accessToken;
			this._get( url, accessToken )
			.then(function(json) {
				if (!_.isEmpty(json.error_description)) {
					return next(json.error_description);
				} else {
					profile.setMeetupMe( json, ( err ) =>{
						next(err);
					});
				}
			});
		}
	}

	static getUserGroups( accessToken, muId, next ){

		if ( accessToken !== undefined && accessToken.length !== 0 ) {
			const url = '/2/groups?organizer_id=' + muId + '&access_token=' + accessToken;
			this._get( url )
			.then( function( json ) {
				if ( !_.isEmpty( json.error_description ) ) {
					return next( json.error_description );
				} else {
					meetupActions.getUserGroups( json.results );
					return next( null, json.results );
				}
			});
		}
	}

	static getUserEvents( accessToken, muId, next ) {

		if ( accessToken !== undefined && accessToken.length !== 0 ) {
			this.getUserGroups( accessToken, muId, (err, results) => {
				if( !err && results.length !== 0 ){
					const url = '/2/events?member_id=self&access_token=' + accessToken;
					//let url = '/2/events?group_id=' + results[0].id + '&access_token=' + accessToken;
					this._get( url )
					.then( function( json ) {
						if ( !_.isEmpty(json.error_description) ) {
							return next( json.error_description );
						} else {
							meetupActions.getUserEvents(  _parseEvents( json.results ) );
							return next( null );
						}
					});
				}else{
					return err
					? next( err )
					: next( "No groups found that you organise" );
				}
			});
		}
	}

	static _get( url ) {
		//TODO: The fetch api is new and not supported in IE or Safari
		//This should be http request or we need a fallback strategy.
		return fetch( rootUrl + url, {'credentials': 'include'} )
		.then( function( response ) {
			return response.json();
		})
		.catch(function( err ) {
			return {
				"error_description": err.message + " Resource:" + rootUrl
			};
		});
	}

}
