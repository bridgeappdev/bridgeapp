'use strict';

export default class Event {
	constructor( eventData ){
		this._data = eventData;
	}

	get data(){
		return this._data;
	}
	get id(){
		return this._data.id;
	}
	get hasBridge(){
		if( this._hasBridge ){
			return this._hasBridge;
		}else{
			return false;
		}
	}
}
