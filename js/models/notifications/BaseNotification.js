'use strict';

import Firebase from 'firebase';

import Notifications from './Notifications';

module.exports = class BaseNotification {

	constructor ( type, meta ){
		this._type = type;
		this._meta = meta;
	}

	set id( notificationId ) {
		this._id = notificationId;
	}
	get id(){
		return this._id;
	}

	set ref( notificationRef ) {
		this._ref = notificationRef;
	}
	get ref(){
		return this._ref;
	}

	get type() {

		return this._type;

	}

	get meta() {

		return this._meta;

	}

	set createdAt(date){

		this._createdAt = date;

	}
	get createdAt(){

		return this._createdAt;

	}

	set read(value){

		this._read = !!value;

	}
	get read(){

		return this._read;

	}

	set seen(value){

		this._seen = !!value;

	}
	get seen(){

		return this._seen;

	}

	toObject(){

		return {

			type: this._type,

			meta: this._meta

		};

	}

	send ( recipientId, next ){

		const sendRef = Notifications.getRef().child(recipientId);

		const notifcationData = this.toObject();
		notifcationData.read = false;
		notifcationData.createdAt = Firebase.ServerValue.TIMESTAMP;

		sendRef.push(notifcationData, function ( FBError ){
			if(FBError){
				console.log("Error sending notification ==> ", FBError);
			}

			if(typeof next === 'function'){
				return next(FBError);
			}

		});

	}

	setSeen ( partialObject, next ){

		const fbRef = new Firebase(this._ref);
		fbRef.update( partialObject, ( err ) => {
			if( err ) {
				console.log('Error sending notification ==>> ', err);
			}
			if( typeof next === 'function' ){
				return next( err );
			}
		})
	}

	static parseFBObject(notificationInstance, fbObject){

		notificationInstance.id = fbObject.id;
		notificationInstance.ref = fbObject.ref;
		notificationInstance.read = fbObject.read;
		notificationInstance.createdAt = fbObject.createdAt;
		notificationInstance.seen = fbObject.seen;

	}

}
