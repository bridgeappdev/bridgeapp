'use strict';

import FBase from '../firebase/fbase';

import notificationsActions from '../../actions/NotificationActions';

import FollowNotification from './FollowNotification'


const _notificationRef = FBase.buildRef('notifications');

const _listenerCallback = function( dataSnapshot ){

	const notifcationObj = dataSnapshot.val();

	notifcationObj.id = dataSnapshot.key();
	notifcationObj.ref = dataSnapshot.ref().toString();

	let notificationInstance;
	switch (true) {

		case (notifcationObj.type === 'FOLLOW'):

			const notificationInstance =
				FollowNotification.buildFromFBObject(
					notifcationObj
				);

			break;

		default:

	}

	return notificationsActions.newNotification(
		notificationInstance
	);

}

const _listenerCancelCallback = function( error ){
	if( error ) {
		console.log("Error from notifications listener ==> ", error);
	}
}

export default class Notifications {

	static getRef(){
		return _notificationRef;
	}

	static addListener( uId, next ){

		const myNotificationsRef = _notificationRef.child(uId);
		myNotificationsRef.orderByChild( 'read' )
			.startAt(false).endAt(false)
				.on( 'child_added', _listenerCallback, _listenerCancelCallback );
	}

	static removeNotificationListener( uId ){
		_notificationRef.child( uId ).off();
	}
}
