import BaseNotification from './BaseNotification';
import React from 'react';

import Avatar from 'material-ui/Avatar';
import MenuItem from 'material-ui/MenuItem';

export default class FollowNotification extends BaseNotification {

	constructor ( detail ){

		if(
			!detail.hasOwnProperty('whoId') ||
			!detail.hasOwnProperty('who') ||
			!detail.hasOwnProperty('mutual')
		){
			throw new Error( 'Missing or wrong parameters for FollowNotification' );
		}

		super( "FOLLOW", detail );

	}

	render(){
		const classList = this._seen
			? 'notification follow'
			: 'notification follow notseen';
		return (
			<MenuItem
				primaryText={this._meta.who + ' followed you!'}
				leftIcon={
					<Avatar
						src={this._meta.avatar}
					/>
				}
				key={this.id}
			/>
		);
		/*return (
			<div className={classList} >
				<Avatar
				className='notification-avatar'
					src={this._meta.avatar}
				/>
			{this._meta.who + ' followed you!'}
			</div>
		);*/
	}

	setSeen( ){

		if(this.seen){
			return void 0;
		}

		const updateObj = {

			seen: true,

			read: true

		};

		super.setSeen( updateObj, ( err ) => {
				console.log('FollowNotification::setSeen ==> Didn\'t work', err);
			}
		);
		this.seen = true;
		this.read = true;
	}

	static buildFromFBObject( fbObject ){

		const notification = new FollowNotification(
			fbObject.meta
		);

		super.parseFBObject(notification, fbObject);

		return notification;

	}

}
